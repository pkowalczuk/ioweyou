from django.core.context_processors import csrf
from django.shortcuts import render
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from announcement.models import Announcement
from entry.models import Entry
from entry.forms import EntryFilterForm, AddDebtForm, PayDebtForm
from user.models import UserProfile
from entry.modals import AddDebtModal, PayDebtModal
from django.db.models import Count


def index(request):

    if request.user.is_authenticated():

        user = request.user
        
        # Modals
        
        add_debt_form = AddDebtForm()
        add_debt_form.set_contractors_field(user=user)
        add_debt_modal = AddDebtModal(request=request, form=add_debt_form)

        pay_debt_form = PayDebtForm()
        pay_debt_form.set_contractors_field(user=user)
        pay_debt_modal = PayDebtModal(request=request, form=pay_debt_form)
        
        # Entrys
        
        friend_id = request.GET.get('contractors', None)
        if friend_id == 'None' or friend_id == '0': friend_id = None
        status_id = request.GET.get('status', None)
        friends = UserProfile.objects.get_friends(user=user)
        date_to = request.GET.get('date_to', None)
        date_from = request.GET.get('date_from', None)
                
        ratio = Entry.objects.ratio(user=user, friend_id=friend_id)
        announcement = Announcement.objects.get_confirmed(user=user).order_by('-created_at').one_or_none()
        entries = get_entries(request, friend_id, status_id, date_from, date_to)

        # Filter Form
        
        filter = EntryFilterForm()
        filter.set_contractors_field(user=user)
        filter.fields["contractors"].initial = friend_id
        filter.fields["status"].initial = status_id
        filter.fields["date_from"].initial = date_from
        filter.fields["date_to"].initial = date_to
        filter.fields["contractors"].choices = [[0, 'All Friends']] + [[u.id, u.get_fullname()] for u in friends]
        
        return render(request, "dashboard/index.html", {
            'entries': entries,
            'filter': filter,
            'ratio': ratio,
            'announcement': announcement,
            'add_debt_modal': add_debt_modal,
            'pay_debt_modal': pay_debt_modal,
        })
    else:
        context = RequestContext(request, {})
        context.update(csrf(request))
        return render(request, "guest.html")

def entries(request):
    user = request.user
    friend_id = request.GET.get('contractors', None)
    if friend_id == 'None' or friend_id == '0': friend_id = None
    status_id = request.GET.get('status', None)
    friends = UserProfile.objects.get_friends(user=user)
    date_to = request.GET.get('date_to', None)
    date_from = request.GET.get('date_from', None)

    entries = get_entries(request, friend_id, status_id, date_from, date_to)
    
    return render(request, "dashboard/partials/tbody.html", {
            'entries': entries,
    })


def get_entries(request, friend_id=None, status_id=None, date_from=None, date_to=None):
    user = request.user
    entries_list = Entry.objects.get_existing()
    entries_list = entries_list.annotate(nb_comments=Count('comments'))
    entries_list = entries_list.filter_by_friend(user=user, friend_id=friend_id)
    entries_list = entries_list.filter_by_status(status=status_id)
    entries_list = entries_list.filter_by_daterange(start=date_from, end=date_to).order_by('-created_at')
    
    paginator = Paginator(entries_list, 8)
            
    try:
        entries = paginator.page(request.GET.get('page', 1))
    except PageNotAnInteger:
        entries = paginator.page(1)
    except EmptyPage:
        entries = paginator.page(paginator.num_pages)
            
    return entries

