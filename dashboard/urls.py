from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='home'),
    url(r'^dashboard/$', views.index, name='dashboard'),
    url(r'^dashboard/entries$', views.entries, name='dashboard_entries'),
)

