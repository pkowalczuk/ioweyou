import os
import sys
import django.core.handlers.wsgi
path = '/var/www/ioweyou'
if path not in sys.path:
    sys.path.append(path)

os.environ['PYTHON_EGG_CACHE'] = '/var/www/iowyou/.python-eggs'
os.environ['DJANGO_SETTINGS_MODULE'] = 'ioweyou.settings'
application = django.core.handlers.wsgi.WSGIHandler()
