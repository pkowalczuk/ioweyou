from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import RedirectView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'', include('social_auth.urls')),
    url(r'', include('entry.urls')),
    url(r'', include('announcement.urls')),
    url(r'', include('dashboard.urls')),
    url(r'', include('user.urls')),
    url(r'^auth/$', RedirectView.as_view(url='/login/facebook')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logout/', 'user.views.logout_view'),
    #others
    url(r'^my_admin/jsi18n', 'django.views.i18n.javascript_catalog'),
    url(r'^ext/', include('django_select2.urls')),
)

urlpatterns += staticfiles_urlpatterns()

