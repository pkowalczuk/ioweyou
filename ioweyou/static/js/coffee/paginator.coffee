class Paginator

  constructor: (@$node, @url, @$target, @$filter) ->
    @bindEvent()

  updatePageButton: (pageNumber, nextPageNumber, maxPageNumber)->
    if !@itsLastPage()
      @$node.attr 'data-page', nextPageNumber
      @$node.attr 'data-next-page', parseInt(nextPageNumber)+1
      @$node.find('span').html(nextPageNumber+'/'+maxPageNumber)
    if @itsLastPage()
      @$node.attr 'data-page', maxPageNumber
      @$node.attr 'data-next-page', maxPageNumber
      @$node.attr 'disabled', 'disabled'
      @$node.find('span').html(maxPageNumber+'/'+maxPageNumber)

  getNextPageNumber: ()->
    @$node.attr 'data-next-page'

  getCurrentPage: ()->
    @$node.attr 'data-page'

  getMaxPage: ()->
    @$node.attr 'data-max-page'

  itsLastPage: ()->
    if @getMaxPage() == @getCurrentPage()
      return true
    return false

  bindEvent: () ->
    @$node.bind 'click', (event)=>
      event.preventDefault()

      if !@itsLastPage()
        pageNumber = @getCurrentPage()
        nextPageNumber = @getNextPageNumber()
        maxPageNumber = @getMaxPage()
        data = @$filter.serialize() + "&page=" + nextPageNumber
        $.get @url, data, (data) =>
          @updatePageButton(pageNumber, nextPageNumber, maxPageNumber)
          @$target.append data
