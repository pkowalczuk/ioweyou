clickButton = (selector, callback) ->
	$(selector).on 'click', ->
		button = $(this)
		$.get button.attr('a-href'), {}, callback(response)

replaceRow = (response) ->
	button.closest('tr').replaceWith(response)

clickActivateButton = ->
	clickButton('.btn-accept-entry', replaceRow)


$(document).ready ->
	clickActivateButton()
