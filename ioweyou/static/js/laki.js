$(document).ready(function() {

  function filterEntries() {
    var fields = $('div.entry-filter :input').serializeArray();
    var result = '?';
    $.each(fields, function(i, field){
      result += field.name + '=' + field.value + '&' ;
    });
    window.location.href = result;
  }
 
  $('div.entry-filter :input').change(filterEntries);
 
	$('body').on('click', '#announcement_confirm_process', function() {
      var button;
      button = $(this);
      $.get(button.attr('a-href'), {}, function(response){
      	if ($.isEmptyObject(response)) {
      		return button.closest('.row-announcement').remove();
      	}
      	else {
      		return button.closest('.row-announcement').replaceWith(response);
      	}
      });
    });
});

$(function() {
    $( "#id_date_from" ).datepicker({
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#id_date_to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#id_date_to" ).datepicker({
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#id_date_from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });