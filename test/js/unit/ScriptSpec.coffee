describe 'Script', ->

	onSuccess =   null
	onComplete =  null
	onFailure =   null
	request =	 null


	beforeEach ->
		# clearAjaxRequests()
		
		onSuccess = jasmine.createSpy('onSuccess')
		onComplete = jasmine.createSpy('onComplete')
		onFailure = jasmine.createSpy('onFailure')

		jasmine.Ajax.useMock()

		clickActivateButton()

		$.get '/', {}, ->
		request = mostRecentAjaxRequest()
		dump(onFailure)

	afterEach ->
		# clearAjaxRequests()

	describe 'with a successful response', ->

		beforeEach ->
			successResponse =
				status: 200
				responseText: "OK"
		
			request(successResponse)

		it 'should call the success callback', ->
			expect(onSuccess).toHaveBeenCalledWith('OK')
		# it 'should call the complete callback', ->
		# 	expect(test.onComplete).toHaveBeenCalled()

	# describe 'with an unsuccessful response', ->

	# 	beforeEach ->
	# 		failureResponse =
	# 			status: 500
	# 			responseText: "FAIL"

	# 		test.request(failureResponse)

	# 	it 'should call the failure callback', ->
	# 		expect(test.onFailure).toHaveBeenCalledWith('FAIL')
	# 	it 'should call the complete callback', ->
	# 		expect(test.onComplete).toHaveBeenCalled()