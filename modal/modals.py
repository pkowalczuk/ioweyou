from django.template.loader import render_to_string
from django.template import RequestContext


class Modal(object):

    identificator = ''
    template = 'modals/modal.html'
    request = None

    def __init__(self, request, **kwargs):

        if 'identificator' in kwargs:
            self.identificator=kwargs.get('identificator')
        if 'template' in kwargs:
            self.template=kwargs.get('template')

        self.request = request
        self.header = ModalHeader(modal=self)
        self.body = ModalBody(modal=self)
        self.footer = ModalFooter(modal=self)

    def render(self):
        return render_to_string(self.template, {'modal': self})


class ModalPartial(object):

    modal = None

    def __init__(self, modal, **kwargs):

        self.modal = modal

        if 'template' in kwargs:
            self.template=kwargs.get('template')


class ModalHeader(ModalPartial):

    template='modals/header.html'

    def render(self):
        return render_to_string(self.template, {'header': self}, context_instance=RequestContext(self.modal.request))


class ModalBody(ModalPartial):

    template='modals/body.html'
    content = ''

    def render(self):
        return render_to_string(self.template, {'body': self}, context_instance=RequestContext(self.modal.request))


class ModalFooter(ModalPartial):

    template='modals/footer.html'

    def render(self):
        return render_to_string(self.template, {'footer': self}, context_instance=RequestContext(self.modal.request))


class FormModal(Modal):

    identificator = 'modal-form'
    route = ''
    form = None
    instance = None

    def __init__(self, request, **kwargs):
        super(FormModal, self).__init__(request=request, **kwargs)

        if 'form' in kwargs:
            self.form = kwargs.get('form')
        if 'instance' in kwargs:
            self.instance = kwargs.get('instance')
        if 'route' in kwargs:
            self.route = kwargs.get('route')


class FormModalBody(ModalBody):
    pass