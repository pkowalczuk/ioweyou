

def process_modal_form(request):
def create_process(request, form_class=EntryForm, template='entry/entry_add.html'):
    user = request.user
    form = form_class(request.POST)
    form.set_user(user=user)

    if form.is_valid():
        form.save(user=user)
        messages.add_message(request, messages.SUCCESS, 'Entry was added correctly.')
        return HttpResponseRedirect("/")
    else:
        messages.add_message(request, messages.ERROR, 'Form is not valid.')
        return render(request, template, {'form': form})