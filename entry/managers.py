from django.db import models
from django.db.models import Q, Sum
from datetime import datetime, time

class EntryManager(models.Manager):

    def get_query_set(self):

        return EntryQuerySet(self.model)

    def get_existing(self):

        return self.get_query_set().select_related('lender', 'debtor', 'comments').filter(~Q(status=3))

    def ratio(self, user, friend_id=None):
        if friend_id:
            query_income = Q(lender_id=user.id, debtor_id=friend_id)
            query_outcome = Q(debtor_id=user.id, lender_id=friend_id)
        else:
            query_income = Q(lender_id=user.id)
            query_outcome = Q(debtor_id=user.id)

        income = self.get_query_set().filter(query_income & Q(status=1)).aggregate(Sum('value'))
        outcome = self.get_query_set().filter(query_outcome & Q(status=1)).aggregate(Sum('value'))

        return float(income.get('value__sum') or 0) - float(outcome.get('value__sum') or 0)


class EntryQuerySet(models.query.QuerySet):

    def filter_by_status(self, status=None):

        if status:
            return self.filter(status=status)
        else:
            return self

    def filter_by_friend(self, user, friend_id=None):

        if friend_id:
            return self.filter(Q(lender_id=user.id, debtor_id=friend_id) | Q(lender_id=friend_id, debtor_id=user.id))
        else:
            return self.filter(Q(lender_id=user.id) | Q(debtor_id=user.id))

    def filter_by_daterange(self, start, end):

        query_from = Q()
        query_to = Q()

        if start:
            date_from = datetime.strptime(start, "%m/%d/%Y")
            query_from = Q(created_at__gte=date_from)

        if end:
            date_to = datetime.strptime(end, "%m/%d/%Y")
            date_to = datetime.combine(date_to, time(23, 59, 59, 999999))
            query_to = Q(created_at__lt=date_to)

        return self.filter(query_from & query_to)
