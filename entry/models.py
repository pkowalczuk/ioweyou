from django.db import models
import ioweyou.settings as settings
from comment.models import Comment
from entry.managers import EntryManager


class Entry(models.Model):

    OPEN = 0
    ACCEPTED = 1
    REJECTED = 2
    DELETED = 3
    STATUS_CHOICES = (
        (OPEN, 'Open'),
        (ACCEPTED, 'Accepted'),
        (REJECTED, 'Rejected'),
        (DELETED, 'Deleted'),
    )

    objects = EntryManager()

    name = models.CharField(max_length=255)
    description = models.TextField('entry description', blank=True, null=True)
    value = models.DecimalField(max_digits=6, decimal_places=2)
    lender = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='lender')
    debtor = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='debtor')
    status = models.SmallIntegerField(default=0, choices=STATUS_CHOICES)
    accepted_at = models.DateTimeField(blank=True, null=True)
    rejected_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name

    def is_open(self):
        if self.status == self.OPEN:
            return True
        return False

    def is_accepted(self):
        if self.status == self.ACCEPTED:
            return True
        return False

    def is_rejected(self):
        if self.status == self.REJECTED:
            return True
        return False

    def is_deleted(self):
        if self.status == self.DELETED:
            return True
        return False

    def is_editable(self):
        if self.is_open() or self.is_rejected():
            return True
        return False

    def is_acceptable(self):
        if self.is_open() or self.is_rejected():
            return True
        return False

    def is_rejectable(self):
        if not (self.is_accepted() or self.is_rejected() or self.is_deleted()):
            return True
        return False

    def is_deletable(self):
        if not (self.is_accepted() or self.is_deleted()):
            return True
        return False

    def get_statusname(self):
        return  self.STATUS_CHOICES[self.status][1]


class EntryComment(Comment):
    entry = models.ForeignKey(Entry, null=False, related_name="comments")

    def __unicode__(self):
        return self.entry.name + '_' + str(self.created_at)


