from django.contrib.admin.widgets import FilteredSelectMultiple, AdminDateWidget
from django.forms.fields import DateField
from django.forms.extras.widgets import SelectDateWidget
from user.models import UserProfile
from entry.models import Entry, EntryComment
from datetime import datetime
from django import forms
from django.forms import ModelForm
from comment.forms import CommentForm
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from django.db.models import Q
from django_select2.widgets import Select2MultipleWidget, Select2Widget


class ContracotorsMultipleChoice(forms.ModelMultipleChoiceField):

    def __init__(self, user, *args, **kwargs):
        super(ContracotorsMultipleChoice, self).__init__(queryset=UserProfile.objects.all(), widget=Select2MultipleWidget(attrs={'class': 'input-block-level'}), *args, **kwargs)
        if user:
            self.queryset = UserProfile.objects.filter(
                Q(user__friendship_creator_set__friend=user, user__is_superuser=0) |
                Q(user__friend_set__creator=user, user__is_superuser=0)
            ).distinct()

class ContracotorsChoice(forms.ModelChoiceField):

    def __init__(self, user, *args, **kwargs):
        super(ContracotorsChoice, self).__init__(queryset=UserProfile.objects.all(), widget=Select2Widget(attrs={'class': 'input-block-level'}), *args, **kwargs)
        if user:
            self.queryset = UserProfile.objects.filter(
                Q(user__friendship_creator_set__friend=user, user__is_superuser=0) |
                Q(user__friend_set__creator=user, user__is_superuser=0)
            ).distinct()


class PayDebtForm(ModelForm):
    class Meta:
        model = Entry
        fields = (
            'name',
            'value',
            'description'
        )
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'class': 'input-block-level'}),
            'value': forms.TextInput(attrs={'placeholder': 'Value', 'class': 'input-block-level','id': 'appendedInput'}),
            'description': forms.Textarea(attrs={'rows': '5', 'placeholder': 'Description', 'class': 'input-block-level'}),
        }

    def set_contractors_field(self, user):
        self.fields.update({
            'contractor': ContracotorsChoice(user=user),
        })

    def save(self, user, commit=True):
        entry = super(PayDebtForm, self).save(commit=False)
        entry.debtor = self.cleaned_data.get("contractor").user
        entry.lender = user
        entry.accepted_at = datetime.now()
        if commit:
            entry.save()
        return entry


class EditDebtForm(ModelForm):
    class Meta:
        model = Entry
        fields = (
            'name',
            'value',
            'description'
        )
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'class': 'input-block-level'}),
            'value': forms.TextInput(attrs={'placeholder': 'Value', 'class': 'input-block-level','id': 'appendedInput'}),
            'description': forms.Textarea(attrs={'rows': '5', 'placeholder': 'Description', 'class': 'input-block-level'}),
        }


class AddDebtForm(ModelForm):
    class Meta:
        model = Entry
        fields = (
            'name',
            'value',
            'description',
        )
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'class': 'input-block-level'}),
            'value': forms.TextInput(attrs={'placeholder': 'Value', 'class': 'input-block-level'}),
            'description': forms.Textarea(attrs={'rows': '5', 'placeholder': 'Description', 'class': 'input-block-level'}),
        }

    include_me = forms.BooleanField(widget=forms.CheckboxInput, initial=False, required = False)

    def set_contractors_field(self, user):
        self.fields.update({
            'contractors': ContracotorsMultipleChoice(user=user),
        })

    class Media:
        css = {
            'all': ('/static/admin/css/forms.css', '/static/admin/css/global.css', '/static/admin/css/widgets.css'),
        }

    def save(self, user, commit=True):
        if self.cleaned_data['include_me'] :
            nb_contractors = len(self.cleaned_data['contractors']) + 1
        else:
            nb_contractors = len(self.cleaned_data['contractors'])

        for contractor in self.cleaned_data['contractors']:
            entry = Entry()
            entry.lender_id = user.id
            entry.debtor_id = contractor.id
            entry.value = self.cleaned_data['value'] / nb_contractors
            entry.description = self.cleaned_data['description']
            entry.name = self.cleaned_data['name']
            entry.status = Entry.OPEN
            entry.save()

            subject = 'You owe money to ' + user.profile.get_fullname()
            template = get_template('ioweyou/emails/add_entry.html')
            context = Context({'lender': user, 'entry': entry})
            content = template.render(context)
            mail = EmailMessage(subject, content, 'ioweyou.pl@gmail.com', to=[entry.debtor.email])
            mail.send()

        return nb_contractors


class EntryFilterForm(ModelForm):
    class Meta:
        model = Entry
        fields = ()

    choices = (
            ('', 'All'),
            (Entry.OPEN, 'Open'),
            (Entry.ACCEPTED, 'Accepted'),
            (Entry.REJECTED, 'Rejected'),
            (Entry.DELETED, 'Deleted')
        )

    status = forms.ChoiceField(choices=choices, widget=forms.Select(attrs={'id': 'id_status_select', 'class': 'span4'}), required=True,)
    date_from = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'span4'}))
    date_to = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'span4'}))

    def set_contractors_field(self, user):
        self.fields.update({
            'contractors': forms.ChoiceField(choices=[], widget=forms.Select(attrs={'id': 'id_contractors_select', 'class': 'span4'}), required = True,),
        })

class EntryCommentForm(CommentForm):
    class Meta(CommentForm.Meta):
        model = EntryComment
        widgets = {
            'content': forms.Textarea(attrs={'placeholder': 'Comment', 'class': 'span4'}),
        }

    def save(self, user, entry, commit=True):
        comment = super(EntryCommentForm, self).save(commit=False)
        comment.entry_id = entry.id
        comment.user_id = user.id
        if commit:
            comment.save()
        return comment