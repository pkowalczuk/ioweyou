from django.contrib import admin
import entry.models as models


class EntryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'description','status']}),
        ('Related users', {'fields': ['lender', 'debtor']}),
    ]
    list_display = ('id', 'name', 'lender', 'debtor', 'created_at', 'updated_at')


class EntryCommentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['entry', 'content']}),
    ]
    list_display = ('id', 'entry', 'content', 'created_at',)

admin.site.register(models.Entry, EntryAdmin)
admin.site.register(models.EntryComment, EntryCommentAdmin)