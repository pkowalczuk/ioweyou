from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from entry.models import Entry
from entry.forms import EntryCommentForm, EditDebtForm
from entry.models import EntryComment
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from datetime import datetime
from entry.modals import EditDebtModal
import json


def details_view(request, id, comment_form=None, edit_form=None):
    entry = Entry.objects.get(pk=id)
    user = request.user

    if (entry.debtor_id == user.id or entry.lender_id == user.id) and not entry.is_deleted():

        if not edit_form:
            edit_debt_form = EditDebtForm(instance=entry)
            edit_debt_modal = EditDebtModal(request=request, instance=entry, form=edit_debt_form)

        if not comment_form:
            comment_form = EntryCommentForm()

        comments = EntryComment.objects.all().filter(entry_id=id).order_by('-created_at')
        return render(request, 'entry/entry.html', {'entry': entry, 'edit_debt_modal': edit_debt_modal, 'comments': comments, 'comment_form': comment_form})

    return redirect('/dashboard/')


def accept_process(request, entry_id):
    user = request.user
    entry = Entry.objects.get(pk=entry_id)

    if entry.debtor_id == user.id and entry.is_acceptable():
        entry.accepted_at = datetime.now()
        entry.status = Entry.ACCEPTED
        entry.save()

        subject = 'Entry was accepted by ' + user.profile.get_fullname()
        template = get_template('ioweyou/emails/accept_entry.html')
        context = Context({'debtor': user, 'entry': entry})
        content = template.render(context)
        mail = EmailMessage(subject, content, 'ioweyou.pl@gmail.com', to=[entry.lender.email])
        mail.send()

        return HttpResponseRedirect('/entry/%i/' % entry.id)

    return redirect('/dashboard/')


def delete_process(request, entry_id):
    user = request.user
    entry = Entry.objects.get(pk=entry_id)

    if entry.lender_id == user.id and entry.is_deletable():
        entry.status = Entry.DELETED
        entry.save()

    return redirect('/dashboard/')


def reject_process(request, entry_id):
    user = request.user
    entry = Entry.objects.get(pk=entry_id)

    if entry.debtor_id == user.id and entry.is_rejectable():
        entry.rejected_at = datetime.now()
        entry.status = Entry.REJECTED
        entry.save()

        subject = 'Entry was rejected by ' + user.profile.get_fullname()
        template = get_template('ioweyou/emails/reject_entry.html')
        context = Context({'debtor': user, 'entry': entry})
        content = template.render(context)
        mail = EmailMessage(subject, content, 'ioweyou.pl@gmail.com', to=[entry.lender.email])
        mail.send()

        return HttpResponseRedirect('/entry/%i/' % entry.id)

    return redirect('/dashboard/')


def comment_add_process(request, entry_id):
    user = request.user
    entry = Entry.objects.get(pk=entry_id)
    form = EntryCommentForm(request.POST)

    if entry.lender_id == user.id or entry.debtor_id == user.id:

        if form.is_valid():
            form.save(user=user, entry=entry)
            messages.add_message(request, messages.SUCCESS, 'Comment was added correctly.')
            return HttpResponseRedirect('/entry/%i/' % entry.id)

        messages.add_message(request, messages.ERROR, 'Form is not valid.')
        return details_view(request, entry_id, comment_form=form)

    return redirect('/dashboard/')



def modal_form_process(request, form_class, modal_class):
    response_data = {};
    user = request.user

    form = form_class(request.POST)
    form.set_contractors_field(user=user)

    modal = modal_class(request=request, form=form)

    if form.is_valid():
        form.save(user=user)
        messages.add_message(request, messages.SUCCESS, modal.messages['success'])
        response_data = {
            'status': 'success',
            'message': modal.messages['success'],
            'data': ''
        }
    else:
        response_data = {
            'status': 'fail',
            'message': modal.messages['error'],
            'data': modal.body.render()
        }

    return HttpResponse(json.dumps(response_data), content_type="application/json")


def modal_form_edit_process(request, id, model_class, form_class, modal_class):
    response_data = {};
    model_object = model_class.objects.get(pk=id)
    form = form_class(request.POST, instance=model_object)
    modal = modal_class(request=request, form=form)

    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, modal.messages['success'])
        response_data = {
            'status': 'success',
            'message': modal.messages['success'],
            'data': ''
        }
    else:
        response_data = {
            'status': 'fail',
            'message': modal.messages['error'],
            'data': modal.body.render()
        }

    return HttpResponse(json.dumps(response_data), content_type="application/json")