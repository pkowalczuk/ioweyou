from modal.modals import FormModal, FormModalBody, ModalHeader


class AddDebtModal(FormModal):

	identificator = 'add-debt-modal-form'
	route = 'entry_add_debt_process'
	messages = {
		'success': 'Information about debt was added successfuly.',
		'error': 'Form is not valid.',
	}

	def __init__(self, request, **kwargs):
		super(AddDebtModal, self).__init__(request=request, **kwargs)

		self.header = AddDebtModalHeader(modal=self)
		self.body = AddDebtModalBody(modal=self)

		if 'form' in kwargs:
			self.body.form = kwargs.get('form')


class AddDebtModalHeader(ModalHeader):

	title='Someone owes you money?'


class AddDebtModalBody(FormModalBody):

	template='entry/modals/add_debt_form_body.html'


class PayDebtModal(FormModal):

	identificator = 'pay-debt-modal-form'
	route = 'entry_pay_debt_process'
	messages = {
		'success': 'Payed information was added successfuly.',
		'error': 'Form is not valid.',
	}

	def __init__(self, request, **kwargs):
		super(PayDebtModal, self).__init__(request=request, **kwargs)

		self.header = PayDebtModalHeader(modal=self)
		self.body = PayDebtModalBody(modal=self)

class PayDebtModalHeader(ModalHeader):

	title='Pay someone money?'


class PayDebtModalBody(FormModalBody):

	template='entry/modals/pay_debt_form_body.html'


class EditDebtModal(FormModal):

	identificator = 'edit-debt-modal-form'
	route = 'entry_edit_debt_process'
	messages = {
		'success': 'Debt details was edited successfuly.',
		'error': 'Form is not valid.',
	}

	def __init__(self, request, **kwargs):
		super(EditDebtModal, self).__init__(request=request, **kwargs)

		self.header = EditDebtModalHeader(modal=self)
		self.body = EditDebtModalBody(modal=self)


class EditDebtModalHeader(ModalHeader):

	title='Edit debt.'


class EditDebtModalBody(FormModalBody):

	template='entry/modals/edit_debt_form_body.html'