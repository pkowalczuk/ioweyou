from django.conf.urls import patterns, url
from entry.forms import AddDebtForm, PayDebtForm, EditDebtForm
from entry.modals import AddDebtModal, PayDebtModal, EditDebtModal
from entry.models import Entry

urlpatterns = patterns('',
    url(r'^entry/(?P<id>\d+)/$', 'entry.views.details_view', name='entry_details_view'),
    url(r'^entry/accept_process/(?P<entry_id>\d+)/$', 'entry.views.accept_process', name='entry_accept_process'),
    url(r'^entry/delete_process/(?P<entry_id>\d+)/$', 'entry.views.delete_process', name='entry_delete_process'),
    url(r'^entry/reject_process/(?P<entry_id>\d+)/$', 'entry.views.reject_process', name='entry_reject_process'),
    url(r'^entry/add_debt_process/$', 'entry.views.modal_form_process', {'form_class': AddDebtForm, 'modal_class': AddDebtModal}, name='entry_add_debt_process'),
    url(r'^entry/pay_debt_process/$', 'entry.views.modal_form_process', {'form_class': PayDebtForm, 'modal_class': PayDebtModal}, name='entry_pay_debt_process'),
    url(r'^entry/edit_debt_process/(?P<id>\d+)/$', 'entry.views.modal_form_edit_process', {'model_class': Entry, 'form_class': EditDebtForm, 'modal_class': EditDebtModal}, name='entry_edit_debt_process'),
    url(r'^entry/comment/add_process/(?P<entry_id>\d+)/$', 'entry.views.comment_add_process',
       name='entry_comment_add_process'),
)