# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 37.59.111.138 (MySQL 5.5.28-1~dotdeb.0-log)
# Database: debtor
# Generation Time: 2013-03-06 18:11:05 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_user
# ------------------------------------------------------------

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`)
VALUES
	(1,'wqwlucky','Łukasz','Staniszewski','wqwlucky@gmail.com','!',0,1,0,'2013-03-05 08:19:08','2013-02-19 22:59:19'),
	(2,'maciej.kuprowski','Maciej','Kuprowsky','maciek220v@interia.pl','!',0,1,0,'2013-03-04 10:40:45','2013-02-19 22:59:21'),
	(3,'agata.adamska','Agata','Adamska','agata_a@o2.pl','!',0,1,0,'2013-03-06 08:37:51','2013-02-19 22:59:23'),
	(4,'piotr.kowalczuk','Piotr','Kowalczuk','imp4ever@gmail.com','!',0,1,0,'2013-03-05 07:16:41','2013-02-19 23:03:06'),
	(5,'tomasz.lis.50','Tomasz','Lis','listomek@gmail.com','!',0,1,0,'2013-02-20 11:51:38','2013-02-20 11:51:36'),
	(6,'arnold.leszczuk.90','Arnold','Leszczuk','tthitt@gmail.com','!',0,1,0,'2013-02-20 12:22:37','2013-02-20 12:22:35'),
	(7,'kamil.biela','Kamil','Biela','kamil.biela@gmail.com','!',0,1,0,'2013-02-21 12:28:27','2013-02-21 12:28:26'),
	(8,'daniel.waligora.902','Daniel','Waligóra','danielwaligora@gmail.com','!',0,1,0,'2013-02-21 12:38:28','2013-02-21 12:38:21'),
	(9,'piotr.breda','Piotr','Breda','miodowar@gmail.com','!',0,1,0,'2013-02-24 14:07:17','2013-02-21 19:18:30'),
	(10,'radoslaw.szynkaruk','Radosław','Szynkaruk','radoslaw.szynkaruk@gmail.com','!',0,1,0,'2013-02-24 16:14:00','2013-02-24 00:47:05'),
	(11,'adrian.smolarczyk','Adrian','Smolarczyk','adriansmolarczyk@gmail.com','!',0,1,0,'2013-03-04 17:49:52','2013-03-04 17:49:50'),
	(12,'qlaczek','Marek','Wilczyński','qlaczek@gmail.com','!',0,1,0,'2013-03-06 11:26:34','2013-03-06 11:26:33');

/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entry_entry
# ------------------------------------------------------------

LOCK TABLES `entry_entry` WRITE;
/*!40000 ALTER TABLE `entry_entry` DISABLE KEYS */;

INSERT INTO `entry_entry` (`id`, `name`, `description`, `value`, `lender_id`, `debtor_id`, `accepted_at`, `created_at`, `updated_at`)
VALUES
	(3,'Różnica za pizze','niech cię gęś kopnie.',3.00,7,4,'2013-02-21 12:30:40','2013-02-21 12:30:40','2013-02-21 12:30:40'),
	(4,'Kasa za popitke u rudego','kupilem kole plus czipsy solone',10.00,3,4,'2013-02-21 12:34:41','2013-02-21 12:34:41','2013-02-21 12:34:41'),
	(5,'Hajs dziwko za żarcie!','Gdyby nie ja to byś zdechł z głodu także dawaj hajs dziwko!',10.00,8,4,'2013-02-21 12:40:39','2013-02-21 12:39:55','2013-02-21 12:40:39'),
	(6,'Parówki na hotdogi','2 bulki + parówki + musztarda',9.00,4,2,'2013-02-21 19:20:44','2013-02-21 19:16:56','2013-02-21 19:20:44'),
	(7,'2 harnasie','bez kaucji',4.78,4,9,'2013-02-21 21:49:39','2013-02-21 19:19:08','2013-02-21 21:49:39'),
	(9,'wóda w piątek','Ale jak to w piątek?',12.00,1,4,'2013-02-23 01:29:45','2013-02-22 23:46:53','2013-02-23 01:29:45'),
	(10,'Kadarka','Kadarka, chleb, makaron, przecier',19.73,9,4,'2013-02-23 11:50:13','2013-02-23 11:23:26','2013-02-23 11:50:13'),
	(11,'chleb tak bardzo ciemny','',3.50,2,9,'2013-02-23 11:54:11','2013-02-23 11:48:45','2013-02-23 11:54:11'),
	(12,'Alternative Szpringer','',13.50,9,2,'2013-02-23 14:20:57','2013-02-23 14:20:09','2013-02-23 14:20:57'),
	(13,'wódka i energetyki','wódka i energetyki w sobote',20.43,1,4,'2013-02-24 12:31:11','2013-02-23 16:23:30','2013-02-24 12:31:11'),
	(14,'nocna buła i zupa','Sobota, sklep 24h przy Bema',7.80,2,4,'2013-02-24 12:31:12','2013-02-24 10:20:39','2013-02-24 12:31:12'),
	(15,'Mirinda','',2.19,9,4,'2013-02-24 14:14:53','2013-02-24 14:08:02','2013-02-24 14:14:53'),
	(16,'7up','',2.19,9,1,'2013-02-24 20:43:55','2013-02-24 14:08:25','2013-02-24 20:43:55'),
	(17,'zakupy','żywiec, fanta i pizza ',16.18,1,4,'2013-02-25 14:36:19','2013-02-24 20:45:35','2013-02-25 14:36:19'),
	(18,'El. metalowe/alkohol/owoce','el. metalowe/browar/jabłka',13.00,2,9,'2013-03-01 10:15:30','2013-02-26 17:55:43','2013-03-01 10:15:30'),
	(20,'Alkohol + chleb','Chleb żytni & Harna$',5.70,2,9,'2013-03-01 10:17:31','2013-02-26 20:36:56','2013-03-01 10:17:31'),
	(21,'kiełbaski','Dawaj siano',3.01,4,8,'2013-03-04 09:05:21','2013-02-27 18:53:30','2013-03-04 09:05:21'),
	(22,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,1,'2013-03-04 12:01:41','2013-03-01 09:55:50','2013-03-04 12:01:41'),
	(23,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,2,'2013-03-01 11:21:41','2013-03-01 09:56:08','2013-03-01 11:21:41'),
	(24,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,3,'2013-03-02 22:20:59','2013-03-01 09:56:25','2013-03-02 22:20:59'),
	(25,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,9,'2013-03-01 10:15:35','2013-03-01 09:56:55','2013-03-01 10:15:35'),
	(26,'Stafy na mieszkanie','1/2 * Prześcieradło, miska, krzesło obrotowe,  pudełko.',171.96,4,3,'2013-03-02 22:20:57','2013-03-01 09:59:56','2013-03-02 22:20:57'),
	(27,'Kaucja','jw',380.00,4,1,'2013-03-04 12:01:45','2013-03-01 10:00:22','2013-03-04 12:01:45'),
	(28,'Kaucja','jw',380.00,4,2,'2013-03-03 11:53:54','2013-03-01 10:01:20','2013-03-03 11:53:54'),
	(29,'Kaucja','jw',380.00,4,3,'2013-03-02 22:20:56','2013-03-01 10:01:35','2013-03-02 22:20:56'),
	(30,'Ipad + ram','jw',1827.53,9,4,'2013-03-01 10:02:29','2013-03-01 10:02:29','2013-03-01 10:02:29'),
	(31,'Kaucja','jw',380.00,4,9,'2013-03-01 10:15:36','2013-03-01 10:02:54','2013-03-01 10:15:36'),
	(32,'Internet za luty','',22.00,9,4,'2013-03-01 14:11:29','2013-03-01 10:16:35','2013-03-01 14:11:29'),
	(33,'Internet za luty','',22.00,9,1,'2013-03-04 12:01:55','2013-03-01 10:17:02','2013-03-04 12:01:55'),
	(35,'LeGyros','Za gyros zaplacilismy 49.5. 3x 16zl + przewoz. Daleś mi 14zł.',2.50,4,2,'2013-03-02 22:01:18','2013-03-02 20:09:11','2013-03-02 22:01:18'),
	(36,'Zakupy z leclerka','Zakupy, głownie chemia.',44.49,4,1,'2013-03-04 12:02:30','2013-03-02 21:48:26','2013-03-04 12:02:30'),
	(37,'Zakupy z leclerka','Głównie chemia.',44.49,4,2,'2013-03-02 22:01:17','2013-03-02 21:48:45','2013-03-02 22:01:17'),
	(38,'Zakupy z leclerka','Głównie chemia.',44.49,4,3,'2013-03-02 22:20:55','2013-03-02 21:49:06','2013-03-02 22:20:55'),
	(39,'Zakupy z leclerka','Głównie chemia.',44.49,4,9,'2013-03-02 22:00:59','2013-03-02 21:49:26','2013-03-02 22:00:59'),
	(40,'Patyczki do uszu','',1.87,4,1,'2013-03-04 12:01:59','2013-03-02 21:51:33','2013-03-04 12:01:59'),
	(41,'Wieszaki','',12.76,4,9,'2013-03-02 22:01:07','2013-03-02 21:52:24','2013-03-02 22:01:07'),
	(42,'Wóda na parapetówę','3xkrupnik + popita',19.00,4,1,'2013-03-04 12:02:37','2013-03-02 22:18:41','2013-03-04 12:02:37'),
	(43,'Wóda na parapetówę','3 x krupnik + popita',19.00,4,2,'2013-03-03 11:53:51','2013-03-02 22:19:04','2013-03-03 11:53:51'),
	(44,'Wóda na parapetówę','3 x krupnik i popita.',19.00,4,9,'2013-03-05 05:53:53','2013-03-02 22:19:32','2013-03-05 05:53:53'),
	(45,'Poranne zakupy','bułki 1.20\r\nparówki 3.70\r\n1/3 jednorazowki 0.03',4.93,4,1,'2013-03-04 12:02:03','2013-03-03 11:44:31','2013-03-04 12:02:03'),
	(46,'Mydło w płynie','',1.41,4,1,'2013-03-04 12:02:20','2013-03-03 11:44:58','2013-03-04 12:02:20'),
	(47,'Mydło w płynie','',1.41,4,2,'2013-03-03 11:53:50','2013-03-03 11:45:09','2013-03-03 11:53:50'),
	(48,'Mydło w płynie','',1.41,4,3,'2013-03-04 12:13:06','2013-03-03 11:45:22','2013-03-04 12:13:06'),
	(49,'Mydło w płynie','',1.41,4,9,'2013-03-05 05:54:01','2013-03-03 11:45:32','2013-03-05 05:54:01'),
	(50,'Legyros','zestaw tortilia extra',13.50,4,7,'2013-03-04 11:40:01','2013-03-04 11:38:59','2013-03-04 11:40:01'),
	(51,'Sałatka Turecka','dziwne ze nie sałatka zydowska',8.50,4,8,NULL,'2013-03-04 11:39:28','2013-03-04 11:39:28'),
	(54,'Dzika noc','',9999.99,5,11,'2013-03-04 17:51:33','2013-03-04 17:51:33','2013-03-04 17:51:33'),
	(55,'Piwo (parapetowka) ','',2.39,9,4,'2013-03-05 07:16:57','2013-03-05 05:55:14','2013-03-05 07:16:57'),
	(56,'Piwo (parapetowka) ','',2.39,9,2,NULL,'2013-03-05 05:55:41','2013-03-05 05:55:41'),
	(57,'Zakupy Ikea','Deska + suszarki',15.99,1,2,NULL,'2013-03-05 08:10:05','2013-03-05 08:10:05'),
	(58,'Zakupy Ikea','Deska + suszarki',15.99,1,3,'2013-03-05 14:34:50','2013-03-05 08:10:05','2013-03-05 14:34:50'),
	(59,'Zakupy Ikea','Deska + suszarki',15.99,1,4,'2013-03-05 11:03:42','2013-03-05 08:10:05','2013-03-05 11:03:42'),
	(60,'Zakupy Ikea','Deska + suszarki',15.99,1,9,'2013-03-05 09:35:30','2013-03-05 08:10:05','2013-03-05 09:35:30'),
	(61,'bezkorkowa tuba','Żubry',4.80,1,2,NULL,'2013-03-05 08:10:43','2013-03-05 08:10:43'),
	(62,'bezkorkowa tuba','Żubry',4.80,1,9,'2013-03-05 08:50:57','2013-03-05 08:11:02','2013-03-05 08:50:57'),
	(63,'bezkorkowa tuba','Żubr',2.40,1,4,'2013-03-05 11:03:27','2013-03-05 08:11:21','2013-03-05 11:03:27'),
	(64,'bezkorkowa tuba','Żubr',2.40,1,3,'2013-03-05 14:34:49','2013-03-05 08:11:38','2013-03-05 14:34:49'),
	(65,'Przelew za kaucje','doszedł.',380.00,2,4,'2013-03-05 15:59:57','2013-03-05 15:59:57','2013-03-05 15:59:57'),
	(66,'przelew za kaucję','',380.00,3,4,'2013-03-05 16:13:43','2013-03-05 16:08:29','2013-03-05 16:13:43'),
	(67,'Dług z arkusza kalkulacyjnego','',540.73,9,1,'2013-03-05 19:28:53','2013-03-05 19:18:41','2013-03-05 19:28:53');

/*!40000 ALTER TABLE `entry_entry` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_friendship
# ------------------------------------------------------------

LOCK TABLES `user_friendship` WRITE;
/*!40000 ALTER TABLE `user_friendship` DISABLE KEYS */;

INSERT INTO `user_friendship` (`id`, `creator_id`, `friend_id`, `created_at`)
VALUES
	(1,3,1,'2013-02-19 22:59:25'),
	(2,4,3,'2013-02-19 23:03:07'),
	(3,4,2,'2013-02-19 23:03:07'),
	(4,4,1,'2013-02-19 23:03:07'),
	(5,5,3,'2013-02-20 11:51:37'),
	(6,5,4,'2013-02-20 11:51:37'),
	(7,5,2,'2013-02-20 11:51:37'),
	(8,5,1,'2013-02-20 11:51:37'),
	(9,6,3,'2013-02-20 12:22:36'),
	(10,6,4,'2013-02-20 12:22:36'),
	(11,6,2,'2013-02-20 12:22:36'),
	(12,6,1,'2013-02-20 12:22:36'),
	(13,6,5,'2013-02-20 12:22:37'),
	(14,7,4,'2013-02-21 12:28:27'),
	(15,8,7,'2013-02-21 12:38:27'),
	(16,8,4,'2013-02-21 12:38:27'),
	(17,9,3,'2013-02-21 19:18:31'),
	(18,9,4,'2013-02-21 19:18:31'),
	(19,9,2,'2013-02-21 19:18:31'),
	(20,9,1,'2013-02-21 19:18:31'),
	(21,9,6,'2013-02-21 19:18:31'),
	(22,9,5,'2013-02-21 19:18:31'),
	(23,10,3,'2013-02-24 00:47:06'),
	(24,10,4,'2013-02-24 00:47:07'),
	(25,10,2,'2013-02-24 00:47:07'),
	(26,10,1,'2013-02-24 00:47:07'),
	(27,10,6,'2013-02-24 00:47:07'),
	(28,10,9,'2013-02-24 00:47:07'),
	(29,10,5,'2013-02-24 00:47:07'),
	(30,1,2,'0000-00-00 00:00:00'),
	(31,11,3,'2013-03-04 17:49:51'),
	(32,11,4,'2013-03-04 17:49:51'),
	(33,11,10,'2013-03-04 17:49:52'),
	(34,11,5,'2013-03-04 17:49:52'),
	(35,12,7,'2013-03-06 11:26:34'),
	(36,12,4,'2013-03-06 11:26:34'),
	(37,12,8,'2013-03-06 11:26:34');

/*!40000 ALTER TABLE `user_friendship` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_userprofile
# ------------------------------------------------------------

LOCK TABLES `user_userprofile` WRITE;
/*!40000 ALTER TABLE `user_userprofile` DISABLE KEYS */;

INSERT INTO `user_userprofile` (`id`, `user_id`, `photo`, `bank_account`)
VALUES
	(1,1,'',''),
	(2,2,'',''),
	(3,3,'',''),
	(4,4,'',''),
	(5,5,'',''),
	(6,6,'',''),
	(7,7,'',''),
	(8,8,'',''),
	(9,9,'',''),
	(10,10,'',''),
	(11,11,'',''),
	(12,12,'','');

/*!40000 ALTER TABLE `user_userprofile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table social_auth_usersocialauth
# ------------------------------------------------------------

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;

INSERT INTO `social_auth_usersocialauth` (`id`, `user_id`, `provider`, `uid`, `extra_data`)
VALUES
	(1,1,'facebook','100000467382825','{\"access_token\": \"AAAGl12e8jmoBAHo1ZAJe5M6ETyfnrcOfOXCIt1DOZAceceACekqLkxcoV9C5E3US2R3xiQdTRyJDs2q1OSeBc5DljYByafVWw1asv8zgZDZD\", \"expires\": \"5121331\", \"id\": \"100000467382825\"}'),
	(2,2,'facebook','100000431944095','{\"access_token\": \"AAAGl12e8jmoBAHmqfVnto919hgKfS5pmBW9ZA0sQvVkgDaHmwhaF385QEEE3ZBIdoUdHqZBGgIFKxD0iCtUJjJV3TnGkU5nyciKnafmvwZDZD\", \"expires\": \"5183998\", \"id\": \"100000431944095\"}'),
	(3,3,'facebook','1488867719','{\"access_token\": \"AAAGl12e8jmoBAHyeCXaC2ZBKpucZBacFZCvQzm06QGzDEYcSZBG5M7dVavbaqKTZAqHzda02gcVMm680vbWFN4ZAWQpTfwffxXF6URSLyIkgZDZD\", \"expires\": \"5183996\", \"id\": \"1488867719\"}'),
	(4,4,'facebook','100000284981757','{\"access_token\": \"AAAGl12e8jmoBAF695UKjXzeQQyWLt44wl3eJCZBcaCFa6faexF24htlXaFKlfsvIEwDZAWkVRIweN3eZBzShZCTLlDk365KKbDTZACDU0mQZDZD\", \"expires\": \"5183999\", \"id\": \"100000284981757\"}'),
	(5,5,'facebook','100003040397216','{\"access_token\": \"AAAGl12e8jmoBAIVLhAiSyJhb0X9yK5uljZCbSiR2ZA7ZAzkRYGjMJfiy3jjkgZAtacZCBzgCqmdSloG8EgOLG7tirdfQcZAM7mKAFzV2CHiQZDZD\", \"expires\": \"5183995\", \"id\": \"100003040397216\"}'),
	(6,6,'facebook','100001635932210','{\"access_token\": \"AAAGl12e8jmoBAMeISxOmjAuCO7xgHC74xAJSq5oJrZB2L9z1zHWYpbB5T4ZCvkHPZBnPvR21Y9ZCt2GuPKp1cVmZAI54Wn5SrSso2INwgFgZDZD\", \"expires\": \"5183998\", \"id\": \"100001635932210\"}'),
	(7,7,'facebook','100000061884540','{\"access_token\": \"AAAGl12e8jmoBAJgjBZBjC2fCWjqHNZBcVDmW8Y1ZB1NLdvlgVpD6l9osEhfl3k9GF2BdfoOQGSY3lWSvdrjvXbO8peN1YPzlxANOShqZAgZDZD\", \"expires\": \"5183999\", \"id\": \"100000061884540\"}'),
	(8,8,'facebook','100001412702235','{\"access_token\": \"AAAGl12e8jmoBAIuvIfpriP9PQ0ZBNzzAwlsZCwm4EOJsKiyPn3DwG3IuQpaUTCkgntn1ZAo0pTUM3zkg7LlMZAkTf2FBLHsg30RShfT2fwZDZD\", \"expires\": \"5183999\", \"id\": \"100001412702235\"}'),
	(9,9,'facebook','100002510910651','{\"access_token\": \"AAAGl12e8jmoBAN7WlqTEm5aJjJMLnv8JeZCijxEPzBJFZCYRgMJHJA4ovTv0Jyn8HxQaaJQmhDIuwQIOd7t7wqVQZBHocdAArENfE84zwZDZD\", \"expires\": \"5183998\", \"id\": \"100002510910651\"}'),
	(10,10,'facebook','100002323554962','{\"access_token\": \"AAAGl12e8jmoBAIM48bWnlC2UGUPEljFzIlWgqkNoiK39IlK1sgKYYyZCEUJHdCxMVSZCfHp1fM2vMHl5Vpos4M1OaTTHUqnMi9bwYOnQZDZD\", \"expires\": \"5128379\", \"id\": \"100002323554962\"}'),
	(11,11,'facebook','1183028056','{\"access_token\": \"AAAGl12e8jmoBAPn2KDB3g8Us9PWZB3LLATa4N2zPAABiWJOdRuXerTkEWIspfLSVD0jDCWcNnXYeLpxydoEJ15VFgMYwCeyc2k75tpAZDZD\", \"expires\": \"5183999\", \"id\": \"1183028056\"}'),
	(12,12,'facebook','100000635717443','{\"access_token\": \"AAAGl12e8jmoBAF86sovQyMWj7n2cqo0wU5QokGdZC6aYesWM1VSxjluiVbHx3XVchPYIZC9ycpLQPefMyVR0e5shU10aDZBhsF5SHTGIAZDZD\", \"expires\": \"5183999\", \"id\": \"100000635717443\"}');

/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
