--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO ioweyou;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO ioweyou;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO ioweyou;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO ioweyou;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO ioweyou;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO ioweyou;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO ioweyou;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO ioweyou;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO ioweyou;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO ioweyou;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO ioweyou;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO ioweyou;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO ioweyou;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO ioweyou;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO ioweyou;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO ioweyou;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO ioweyou;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO ioweyou;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO ioweyou;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: entry_entry; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE entry_entry (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    value numeric(6,2) NOT NULL,
    lender_id integer,
    accepted_at timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    debtor_id integer,
    rejected_at timestamp with time zone,
    status smallint NOT NULL
);


ALTER TABLE public.entry_entry OWNER TO ioweyou;

--
-- Name: entry_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE entry_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entry_entry_id_seq OWNER TO ioweyou;

--
-- Name: entry_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE entry_entry_id_seq OWNED BY entry_entry.id;


--
-- Name: entry_entrycomment; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE entry_entrycomment (
    id integer NOT NULL,
    content text,
    user_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    entry_id integer NOT NULL
);


ALTER TABLE public.entry_entrycomment OWNER TO ioweyou;

--
-- Name: entry_entrycomment_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE entry_entrycomment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entry_entrycomment_id_seq OWNER TO ioweyou;

--
-- Name: entry_entrycomment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE entry_entrycomment_id_seq OWNED BY entry_entrycomment.id;


--
-- Name: social_auth_association; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE social_auth_association (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE public.social_auth_association OWNER TO ioweyou;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE social_auth_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_association_id_seq OWNER TO ioweyou;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE social_auth_association_id_seq OWNED BY social_auth_association.id;


--
-- Name: social_auth_nonce; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE social_auth_nonce (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(40) NOT NULL
);


ALTER TABLE public.social_auth_nonce OWNER TO ioweyou;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE social_auth_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_nonce_id_seq OWNER TO ioweyou;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE social_auth_nonce_id_seq OWNED BY social_auth_nonce.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE social_auth_usersocialauth (
    id integer NOT NULL,
    user_id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL
);


ALTER TABLE public.social_auth_usersocialauth OWNER TO ioweyou;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE social_auth_usersocialauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_usersocialauth_id_seq OWNER TO ioweyou;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE social_auth_usersocialauth_id_seq OWNED BY social_auth_usersocialauth.id;


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO ioweyou;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO ioweyou;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;


--
-- Name: user_friendship; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE user_friendship (
    id integer NOT NULL,
    creator_id integer NOT NULL,
    friend_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_friendship OWNER TO ioweyou;

--
-- Name: user_friendship_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE user_friendship_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_friendship_id_seq OWNER TO ioweyou;

--
-- Name: user_friendship_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE user_friendship_id_seq OWNED BY user_friendship.id;


--
-- Name: user_userclient; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE user_userclient (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.user_userclient OWNER TO ioweyou;

--
-- Name: user_userclient_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE user_userclient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_userclient_id_seq OWNER TO ioweyou;

--
-- Name: user_userclient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE user_userclient_id_seq OWNED BY user_userclient.id;


--
-- Name: user_userprofile; Type: TABLE; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE TABLE user_userprofile (
    id integer NOT NULL,
    user_id integer NOT NULL,
    photo character varying(50) NOT NULL,
    bank_account character varying(100) NOT NULL
);


ALTER TABLE public.user_userprofile OWNER TO ioweyou;

--
-- Name: user_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: ioweyou
--

CREATE SEQUENCE user_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_userprofile_id_seq OWNER TO ioweyou;

--
-- Name: user_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ioweyou
--

ALTER SEQUENCE user_userprofile_id_seq OWNED BY user_userprofile.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY entry_entry ALTER COLUMN id SET DEFAULT nextval('entry_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY entry_entrycomment ALTER COLUMN id SET DEFAULT nextval('entry_entrycomment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY social_auth_association ALTER COLUMN id SET DEFAULT nextval('social_auth_association_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('social_auth_nonce_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_friendship ALTER COLUMN id SET DEFAULT nextval('user_friendship_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_userclient ALTER COLUMN id SET DEFAULT nextval('user_userclient_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_userprofile ALTER COLUMN id SET DEFAULT nextval('user_userprofile_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add log entry	7	add_logentry
20	Can change log entry	7	change_logentry
21	Can delete log entry	7	delete_logentry
22	Can add migration history	8	add_migrationhistory
23	Can change migration history	8	change_migrationhistory
24	Can delete migration history	8	delete_migrationhistory
25	Can add user profile	9	add_userprofile
26	Can change user profile	9	change_userprofile
27	Can delete user profile	9	delete_userprofile
28	Can add friendship	10	add_friendship
29	Can change friendship	10	change_friendship
30	Can delete friendship	10	delete_friendship
31	Can add user client	11	add_userclient
32	Can change user client	11	change_userclient
33	Can delete user client	11	delete_userclient
34	Can add entry	12	add_entry
35	Can change entry	12	change_entry
36	Can delete entry	12	delete_entry
37	Can add user social auth	13	add_usersocialauth
38	Can change user social auth	13	change_usersocialauth
39	Can delete user social auth	13	delete_usersocialauth
40	Can add nonce	14	add_nonce
41	Can change nonce	14	change_nonce
42	Can delete nonce	14	delete_nonce
43	Can add association	15	add_association
44	Can change association	15	change_association
45	Can delete association	15	delete_association
46	Can add entry comment	16	add_entrycomment
47	Can change entry comment	16	change_entrycomment
48	Can delete entry comment	16	delete_entrycomment
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('auth_permission_id_seq', 48, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
5	!	2013-02-20 11:51:38+01	f	tomasz.lis.50	Tomasz	Lis	listomek@gmail.com	f	t	2013-02-20 11:51:36+01
6	!	2013-02-20 12:22:37+01	f	arnold.leszczuk.90	Arnold	Leszczuk	tthitt@gmail.com	f	t	2013-02-20 12:22:35+01
10	!	2013-02-24 16:14:00+01	f	radoslaw.szynkaruk	Radosław	Szynkaruk	radoslaw.szynkaruk@gmail.com	f	t	2013-02-24 00:47:05+01
11	!	2013-03-04 17:49:52+01	f	adrian.smolarczyk	Adrian	Smolarczyk	adriansmolarczyk@gmail.com	f	t	2013-03-04 17:49:50+01
12	!	2013-03-06 11:26:34+01	f	qlaczek	Marek	Wilczyński	qlaczek@gmail.com	f	t	2013-03-06 11:26:33+01
16	!	2013-03-24 18:12:48.864774+01	f	p.kowalczuk.priv	Bartosz	Bartłomiej	p.kowalczuk.priv@gmail.com	f	t	2013-03-24 18:12:48.697682+01
13	!	2013-03-15 22:42:45.05298+01	f	holowinski.marcin	Marcin	Hołowiński	dzidzixxx@gmail.com	f	t	2013-03-11 22:15:11+01
9	!	2013-03-18 15:40:13.736177+01	f	piotr.breda	Piotr	Breda	miodowar@gmail.com	f	t	2013-02-21 19:18:30+01
7	!	2013-03-19 13:41:27.854832+01	f	kamil.biela	Kamil	Biela	kamil.biela@gmail.com	f	t	2013-02-21 12:28:26+01
14	!	2013-03-20 13:27:56.982474+01	f	robert.szostakowski	Robert	Szostakowski	robskiofpolski@hotmail.com	f	t	2013-03-20 13:27:56.859982+01
1	!	2013-03-20 17:45:51.672252+01	f	wqwlucky	Łukasz	Staniszewski	wqwlucky@gmail.com	f	t	2013-02-19 22:59:19+01
4	!	2013-03-21 15:55:42.284392+01	f	piotr.kowalczuk	Piotr	Kowalczuk	imp4ever@gmail.com	f	t	2013-02-19 23:03:06+01
2	!	2013-03-22 10:45:43.175526+01	f	maciej.kuprowski	Maciej	Kuprowsky	maciek220v@interia.pl	f	t	2013-02-19 22:59:21+01
8	!	2013-03-22 13:03:13.158627+01	f	daniel.waligora.902	Daniel	Waligóra	danielwaligora@gmail.com	f	t	2013-02-21 12:38:21+01
15	!	2013-03-23 14:14:13.680518+01	f	joanna.bednarska.50	Joanna	Bednarska	ashlee.e@interia.pl	f	t	2013-03-23 14:14:13.632463+01
3	!	2013-03-23 18:42:54.567345+01	f	agata.adamska	Agata	Adamska	agata_a@o2.pl	f	t	2013-02-19 22:59:23+01
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('auth_user_id_seq', 16, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	log entry	admin	logentry
8	migration history	south	migrationhistory
9	user profile	user	userprofile
10	friendship	user	friendship
11	user client	user	userclient
12	entry	entry	entry
13	user social auth	social_auth	usersocialauth
14	nonce	social_auth	nonce
15	association	social_auth	association
16	entry comment	entry	entrycomment
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('django_content_type_id_seq', 16, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
6ezrddh82nyyq7ht1ye5r0bgvos9mpx8	NjQ1YTJjYmZhNWRhMTBiZDJkMjk4MTRhNzI3YzU3NGRmMWVkOTM3MTqAAn1xAVUOZmFjZWJvb2tfc3RhdGVYIAAAADNOd1ZNclZIcFhkZFBqQnFoSmxPdUhYVFg0bmlESUl4cy4=	2013-04-05 22:39:22.165354+02
7e7tpyljw2svsp77xq8mg10kjqj9l588	MjhlYTcwZGMzMjI1YzZlYWYzM2QwNjlhNjNkOGFmNmU1MGE4MDY1ZTqAAn1xAVUOZmFjZWJvb2tfc3RhdGVYIAAAAG14dkxocUExdWVGckI3NlZFb1h1a1A2TWpuWktBY2M3cy4=	2013-03-29 02:30:45.525992+01
ncbqjkrfjknwfw95d5vvf9i28rq3njdz	YTcxYmI4OWVlMzhhNGVmYTQ3MTMwMDg3MjQwOTY0MTAxZGU5YmUxYTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLDVUOZmFjZWJvb2tfc3RhdGVYIAAAAFpZZWhhOTN6ZzJjMnpoc3BHNTlaNk0yb3hWaEpTUk1UVQlmcmllbmRfaWRxAlgBAAAANFUSX2F1dGhfdXNlcl9iYWNrZW5kVS1zb2NpYWxfYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRVHnNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZFgIAAAAZmFjZWJvb2tYDwAAAF9zZXNzaW9uX2V4cGlyeWNkYXRldGltZQpkYXRldGltZQpxA1UKB90FDhUqBQDmbWNkamFuZ28udXRpbHMudGltZXpvbmUKVVRDCnEEKVJxBYZScQZ1Lg==	2013-05-14 23:42:05.058989+02
b54c1523tcsku4mtb5ukiet8cqoq2lle	NTExZGVlYTQ2YTU5MGVjNWM2NzhjOTExNmM3MDQ0OTM4ZDE2ODljNjqAAn1xAShVDV9hdXRoX3VzZXJfaWRLAlUOZmFjZWJvb2tfc3RhdGVYIAAAAFVTZFlDVlBlc3Z4cmFvUjQ2djZ4T1EzdFZrTndjQzE5VRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUSFjAKACR8Y2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-19 00:48:10.00934+02
pl78jt8a4d400zt3ii7pvk63bps3zxlk	M2VhNDYyNGFjNTZjZTU2NjE3OWZjMmNlMDdkMmY4NzBlYzEwYTc0NTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLCVUOZmFjZWJvb2tfc3RhdGVYIAAAAHZRU1lmeHpVNnFQbUZBUWVCdjJLOTNIazBTM2k5TERCVRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUOBhQxAc7WY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-14 08:20:49.118486+02
t41ud5b3wtyu6zmev9zgqq3fgat9g09a	ODlhNWYxMmUwM2E3NzljNmU5Y2U4NjAyNzUyN2YxMGZlZjYyZGY2NjqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABlYkRqT3dqNkxEb1lnaDVCZThBOGY1b2dKbDNpdWhmTVgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBREALRsHdmpjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsBVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClgIAAAAZmFjZWJvb2txC3Uu	2013-05-17 02:45:27.489066+02
11os492b4wom418yghi3rcz4176wa2x0	ZjlmMzFhMzMzMTUyZTRiNDVmYjc3Y2UwZjFkZjg4YjU2YzQ1NzhmNzqAAn1xAShVDV9hdXRoX3VzZXJfaWRLCVUOZmFjZWJvb2tfc3RhdGVYIAAAAFNNMHJCMkhHZE9CYTNpY04ycVlYN3dtQlV6M0lNUTFHVRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QURDigLC01AY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-17 16:40:11.740672+02
5yg6wn8o7y9833yxa8mt4jxbedw5sbl4	ZTU4NThlZWJlNmE4MTNlYjM3NTNiNDIzNTExMTdkMDJiZTk0Y2VlYTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLAVUOZmFjZWJvb2tfc3RhdGVYIAAAAE9JOElqUkdTbDZBZWtMc28wMnoxc1NNSHJsMmFPTlB5VRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUPFDMTBL6kY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-15 22:51:19.310948+02
gkwhv3x9kjt2zcht7y0nxep898bi9snb	YzNlODA0MWM3ZDJhMTBlZjcxZTk2MDMxYTU1NDU1NDliZjMzM2VmYjqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAAAzaTVJVjlhZ1ZUTThoWjhJTGFXR2FwYkJBdWJhaHR6aVgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBRESBhoA3zNjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsDVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClgIAAAAZmFjZWJvb2txC3Uu	2013-05-17 20:06:26.057139+02
jauw0i6vvqk3p7l3yg02urvevirce3qh	NDc1ZTM5MDI3NTRlODJhMjc1OGQ5Y2U2ODkyMmI5ZTJhMjAwZWI2YjqAAn1xAShVDV9hdXRoX3VzZXJfaWRLB1UOZmFjZWJvb2tfc3RhdGVYIAAAAFpYOWM5ZzFsYmhuaFB0WXM1Z01mb2NSVVBwNjJGc3ZSVQlmcmllbmRfaWRYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZFUtc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRYCAAAAGZhY2Vib29rWA8AAABfc2Vzc2lvbl9leHBpcnljZGF0ZXRpbWUKZGF0ZXRpbWUKcQJVCgfdBBYMHBgNHqFjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxAylScQSGUnEFdS4=	2013-04-22 14:28:24.859809+02
7kgq5kxebmsgqs6xao5bqs321tllx10o	NzZlY2EyZDBjZWM5ZTA1N2JiMDlhNzI4NjViNGU2MDY1YWRiNGQxZDqAAn1xAShVDV9hdXRoX3VzZXJfaWRLA1UOZmFjZWJvb2tfc3RhdGVYIAAAAFNySFA1MW5KWkVZUU1Hb0ZMRnd2c2x4TjNORERJaUc0VRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUOCRoOCLuOY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-14 11:26:14.572302+02
0lfg9m30izqu9yhsyodpf3d5z6qf4sb0	NmNhYTNiNGQ0NGE1NTcyMmYxYzQ3MjgyMzRkNWRiM2VhODQ2MDIyMjqAAn1xAShVDV9hdXRoX3VzZXJfaWRLCFUOZmFjZWJvb2tfc3RhdGVYIAAAADJxdzRsUjJkRVpCZWZIRFJpVXgxWU9QM3BFMGZKN1FwVQlmcmllbmRfaWRxAlgBAAAANFUSX2F1dGhfdXNlcl9iYWNrZW5kVS1zb2NpYWxfYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRVHnNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZFgIAAAAZmFjZWJvb2tYDwAAAF9zZXNzaW9uX2V4cGlyeWNkYXRldGltZQpkYXRldGltZQpxA1UKB90FEgsAOw2UtGNkamFuZ28udXRpbHMudGltZXpvbmUKVVRDCnEEKVJxBYZScQZ1Lg==	2013-05-18 13:00:59.890036+02
5luzdgo7eqd7gbrwk9jwayoo6godxs7i	NDNiOGJjZGM1MjYxZDQ1N2QzNTExZDA4NWI0YTIwNTIwODk1YWYwYjqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABWZmgwWHZZMGgzR3JoZ0lhVGh6NjJLaEZWWTBLQ1JuVFgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBQ4VKgUFCKxjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsNVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClgIAAAAZmFjZWJvb2txC3Uu	2013-05-14 23:42:05.3299+02
e1syblkkmpzhdremvns5x3ulj8c06ro7	YTljOWJlOGRjZWZmY2YxMGZjYjIyODMxYzYzMDk5MGViZGFmMjEyNzqAAn1xAS4=	2013-04-03 16:14:53.148994+02
osd4khmc1qnzb803fzpfkztbdzheeasv	NTkyYjliY2I5OTQ4ODA2ODQwNmNkOTkxYzdmZjk4ZWNkZGYzNmFkOTqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABNbUpxMG1iWXJwdU01aHJISWhNbnR4dFFPbG1EUjlmNlgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBREALRcKWABjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsBVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClgIAAAAZmFjZWJvb2txC3Uu	2013-05-17 02:45:23.677888+02
jdspmd80xdrx7s5u0ns7lmcexcdjilkt	ZDQ0ZWU1ZDY5ZWNlZjM1ZTBlMmNhNjRiNTA3OGNjYWY1NGM3ZDhiYzqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABNV0pCYm1EVDNlSDZrVUdVZ3ZGZlg4dGtxUzNBMDBXeVgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBRQONygEbGJjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsEVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClgIAAAAZmFjZWJvb2txC3Uu	2013-05-20 16:55:40.28989+02
9cw8jlhxe16hbhitfnhkhxwp1w3cmjvy	MGI2Zjc3ZTA2NzJhNTZkYWViMjU3NTg5YzRhODI4Y2M5MDFhZjU5YzqAAn1xAShVDV9hdXRoX3VzZXJfaWRLA1UOZmFjZWJvb2tfc3RhdGVYIAAAAHJoa3JDclhnRUN0cW9PdWhRY1NiZklhbE8yNWZYV0FrVRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUPEC45BMKUY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-15 18:46:57.311956+02
9ojht9ey5gco69k7uz1r5mmamonjyrj8	NTFjZGJmOGU1Njc4ZDhmMzRiZmI2ZjNhODI0N2YyZjVjNTA3YTQ4YTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLAVUOZmFjZWJvb2tfc3RhdGVYIAAAAEJUaTZGNnIzUURGYVJJbnlLbXdwTzJ1WDZvSFlPdWZGVRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QURAC0XBMYrY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-17 02:45:23.312875+02
w01joaaa2vkfsx11k1a8o4y8b52kr1wq	MGUyYTkzZDRmMGJhODg2MzU0ZDg2MzA0OWQ0YjYwZTcxNjM5NDM3ZjqAAn1xAShVDV9hdXRoX3VzZXJfaWRLA1UOZmFjZWJvb2tfc3RhdGVYIAAAAGxydms4MDdvVzdVOVBoSlpJRFdpb29kWWdnamc0TDJzVRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUWESo1CLyYY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu	2013-05-22 19:42:53.572568+02
w3vfc2d1rv1vzahdade86kpfrcon42le	YjliZTI1NTkxM2E2Y2Q5MjBiZmFkN2Q3ZGYzOTI0ZmUwOTBlN2U2ZjqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAAByMG15SGFHS0ZiQ0o1anZ0c0dnd2tDRmJhM2xLdkFnSFgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBRcRDC0NTMljZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsQVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClUIZmFjZWJvb2txC3Uu	2013-05-23 19:12:45.871625+02
5hxcz3hsfjhchypc6etrqhtvma47zjia	ZjFkOGRkM2U0ZDJjOWE0ODMzYmU0MjkzMTM4ZDI5ZDkzMjdkNDA2ZTqAAn1xAVUOZmFjZWJvb2tfc3RhdGVYIAAAADVYTWNXZUdSQlNER3lxN2t1cnhjSGNTcHhaNEZwaTBNcy4=	2013-04-05 22:38:53.117223+02
cnajem82cc788xf484xo7r0amyrjeqei	NjEyZDEwZmIyNWI4ZTRjNDY1ZTcyNTMxMmZhMTYxNjdjZWNmZjliYTqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAAAwbWxibmlSMWZ6blFNT1J2UmZYMWNSV0ZhOGgza0kwV1gPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBRYNDgsKdtdjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsPVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClUIZmFjZWJvb2txC3Uu	2013-05-22 15:14:11.685783+02
dvl8wypvcgmw70bv2xmuwz24o18t2iyl	NDhjZjZkYmZlYjM3MjgwNTU0NjExZGNjNmU3OGM4MjY0NTA3NDY1YjqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABZbmlmMnNsZzN6dHVIaUFaODJ2dVl4cDA3RGpadjcxUVgPAAAAX3Nlc3Npb25fZXhwaXJ5cQJjZGF0ZXRpbWUKZGF0ZXRpbWUKcQNVCgfdBRUJGyYCz3RjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxBClScQWGUnEGVRJfYXV0aF91c2VyX2JhY2tlbmRxB1Utc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kcQhVDV9hdXRoX3VzZXJfaWRxCUsCVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxClgIAAAAZmFjZWJvb2txC3Uu	2013-05-21 11:27:38.18418+02
82qxerphm3qsuh717gvzybehfrazdgat	NDQ3YTYxNjY0ZTBiMWJkYzVmYzU4M2NhYjIzZDI5YzQyNjg5NWE0NTqAAn1xAShVDV9hdXRoX3VzZXJfaWRLCFUOZmFjZWJvb2tfc3RhdGVYIAAAAHlXOFF1ZHBLRms1MFVSQ3FtTU0ycklzc2txcjBReURlVQlmcmllbmRfaWRYAQAAADdVEl9hdXRoX3VzZXJfYmFja2VuZFUtc29jaWFsX2F1dGguYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tCYWNrZW5kVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRYCAAAAGZhY2Vib29rWA8AAABfc2Vzc2lvbl9leHBpcnljZGF0ZXRpbWUKZGF0ZXRpbWUKcQJVCgfdBRUMAwwCkkBjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxAylScQSGUnEFdS4=	2013-05-21 14:03:12.168512+02
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Data for Name: entry_entry; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY entry_entry (id, name, description, value, lender_id, accepted_at, created_at, updated_at, debtor_id, rejected_at, status) FROM stdin;
4	Kasa za popitke u rudego	kupilem kole plus czipsy solone	10.00	3	2013-02-21 12:34:41+01	2013-02-21 12:34:41+01	2013-02-21 12:34:41+01	4	\N	1
5	Hajs dziwko za żarcie!	Gdyby nie ja to byś zdechł z głodu także dawaj hajs dziwko!	10.00	8	2013-02-21 12:40:39+01	2013-02-21 12:39:55+01	2013-02-21 12:40:39+01	4	\N	1
6	Parówki na hotdogi	2 bulki + parówki + musztarda	9.00	4	2013-02-21 19:20:44+01	2013-02-21 19:16:56+01	2013-02-21 19:20:44+01	2	\N	1
7	2 harnasie	bez kaucji	4.78	4	2013-02-21 21:49:39+01	2013-02-21 19:19:08+01	2013-02-21 21:49:39+01	9	\N	1
9	wóda w piątek	Ale jak to w piątek?	12.00	1	2013-02-23 01:29:45+01	2013-02-22 23:46:53+01	2013-02-23 01:29:45+01	4	\N	1
10	Kadarka	Kadarka, chleb, makaron, przecier	19.73	9	2013-02-23 11:50:13+01	2013-02-23 11:23:26+01	2013-02-23 11:50:13+01	4	\N	1
11	chleb tak bardzo ciemny		3.50	2	2013-02-23 11:54:11+01	2013-02-23 11:48:45+01	2013-02-23 11:54:11+01	9	\N	1
12	Alternative Szpringer		13.50	9	2013-02-23 14:20:57+01	2013-02-23 14:20:09+01	2013-02-23 14:20:57+01	2	\N	1
13	wódka i energetyki	wódka i energetyki w sobote	20.43	1	2013-02-24 12:31:11+01	2013-02-23 16:23:30+01	2013-02-24 12:31:11+01	4	\N	1
14	nocna buła i zupa	Sobota, sklep 24h przy Bema	7.80	2	2013-02-24 12:31:12+01	2013-02-24 10:20:39+01	2013-02-24 12:31:12+01	4	\N	1
15	Mirinda		2.19	9	2013-02-24 14:14:53+01	2013-02-24 14:08:02+01	2013-02-24 14:14:53+01	4	\N	1
16	7up		2.19	9	2013-02-24 20:43:55+01	2013-02-24 14:08:25+01	2013-02-24 20:43:55+01	1	\N	1
17	zakupy	żywiec, fanta i pizza 	16.18	1	2013-02-25 14:36:19+01	2013-02-24 20:45:35+01	2013-02-25 14:36:19+01	4	\N	1
18	El. metalowe/alkohol/owoce	el. metalowe/browar/jabłka	13.00	2	2013-03-01 10:15:30+01	2013-02-26 17:55:43+01	2013-03-01 10:15:30+01	9	\N	1
20	Alkohol + chleb	Chleb żytni & Harna$	5.70	2	2013-03-01 10:17:31+01	2013-02-26 20:36:56+01	2013-03-01 10:17:31+01	9	\N	1
21	kiełbaski	Dawaj siano	3.01	4	2013-03-04 09:05:21+01	2013-02-27 18:53:30+01	2013-03-04 09:05:21+01	8	\N	1
22	Zakupy Ikea	2 szczotki i 4 szklanki	3.03	4	2013-03-04 12:01:41+01	2013-03-01 09:55:50+01	2013-03-04 12:01:41+01	1	\N	1
23	Zakupy Ikea	2 szczotki i 4 szklanki	3.03	4	2013-03-01 11:21:41+01	2013-03-01 09:56:08+01	2013-03-01 11:21:41+01	2	\N	1
24	Zakupy Ikea	2 szczotki i 4 szklanki	3.03	4	2013-03-02 22:20:59+01	2013-03-01 09:56:25+01	2013-03-02 22:20:59+01	3	\N	1
25	Zakupy Ikea	2 szczotki i 4 szklanki	3.03	4	2013-03-01 10:15:35+01	2013-03-01 09:56:55+01	2013-03-01 10:15:35+01	9	\N	1
27	Kaucja	jw	380.00	4	2013-03-04 12:01:45+01	2013-03-01 10:00:22+01	2013-03-04 12:01:45+01	1	\N	1
28	Kaucja	jw	380.00	4	2013-03-03 11:53:54+01	2013-03-01 10:01:20+01	2013-03-03 11:53:54+01	2	\N	1
29	Kaucja	jw	380.00	4	2013-03-02 22:20:56+01	2013-03-01 10:01:35+01	2013-03-02 22:20:56+01	3	\N	1
30	Ipad + ram	jw	1827.53	9	2013-03-01 10:02:29+01	2013-03-01 10:02:29+01	2013-03-01 10:02:29+01	4	\N	1
31	Kaucja	jw	380.00	4	2013-03-01 10:15:36+01	2013-03-01 10:02:54+01	2013-03-01 10:15:36+01	9	\N	1
32	Internet za luty		22.00	9	2013-03-01 14:11:29+01	2013-03-01 10:16:35+01	2013-03-01 14:11:29+01	4	\N	1
33	Internet za luty		22.00	9	2013-03-04 12:01:55+01	2013-03-01 10:17:02+01	2013-03-04 12:01:55+01	1	\N	1
35	LeGyros	Za gyros zaplacilismy 49.5. 3x 16zl + przewoz. Daleś mi 14zł.	2.50	4	2013-03-02 22:01:18+01	2013-03-02 20:09:11+01	2013-03-02 22:01:18+01	2	\N	1
36	Zakupy z leclerka	Zakupy, głownie chemia.	44.49	4	2013-03-04 12:02:30+01	2013-03-02 21:48:26+01	2013-03-04 12:02:30+01	1	\N	1
37	Zakupy z leclerka	Głównie chemia.	44.49	4	2013-03-02 22:01:17+01	2013-03-02 21:48:45+01	2013-03-02 22:01:17+01	2	\N	1
38	Zakupy z leclerka	Głównie chemia.	44.49	4	2013-03-02 22:20:55+01	2013-03-02 21:49:06+01	2013-03-02 22:20:55+01	3	\N	1
39	Zakupy z leclerka	Głównie chemia.	44.49	4	2013-03-02 22:00:59+01	2013-03-02 21:49:26+01	2013-03-02 22:00:59+01	9	\N	1
40	Patyczki do uszu		1.87	4	2013-03-04 12:01:59+01	2013-03-02 21:51:33+01	2013-03-04 12:01:59+01	1	\N	1
41	Wieszaki		12.76	4	2013-03-02 22:01:07+01	2013-03-02 21:52:24+01	2013-03-02 22:01:07+01	9	\N	1
42	Wóda na parapetówę	3xkrupnik + popita	19.00	4	2013-03-04 12:02:37+01	2013-03-02 22:18:41+01	2013-03-04 12:02:37+01	1	\N	1
43	Wóda na parapetówę	3 x krupnik + popita	19.00	4	2013-03-03 11:53:51+01	2013-03-02 22:19:04+01	2013-03-03 11:53:51+01	2	\N	1
44	Wóda na parapetówę	3 x krupnik i popita.	19.00	4	2013-03-05 05:53:53+01	2013-03-02 22:19:32+01	2013-03-05 05:53:53+01	9	\N	1
45	Poranne zakupy	bułki 1.20\\r\\nparówki 3.70\\r\\n1/3 jednorazowki 0.03	4.93	4	2013-03-04 12:02:03+01	2013-03-03 11:44:31+01	2013-03-04 12:02:03+01	1	\N	1
46	Mydło w płynie		1.41	4	2013-03-04 12:02:20+01	2013-03-03 11:44:58+01	2013-03-04 12:02:20+01	1	\N	1
47	Mydło w płynie		1.41	4	2013-03-03 11:53:50+01	2013-03-03 11:45:09+01	2013-03-03 11:53:50+01	2	\N	1
48	Mydło w płynie		1.41	4	2013-03-04 12:13:06+01	2013-03-03 11:45:22+01	2013-03-04 12:13:06+01	3	\N	1
49	Mydło w płynie		1.41	4	2013-03-05 05:54:01+01	2013-03-03 11:45:32+01	2013-03-05 05:54:01+01	9	\N	1
50	Legyros	zestaw tortilia extra	13.50	4	2013-03-04 11:40:01+01	2013-03-04 11:38:59+01	2013-03-04 11:40:01+01	7	\N	1
51	Sałatka Turecka	dziwne ze nie sałatka zydowska	8.50	4	2013-03-08 13:48:09+01	2013-03-04 11:39:28+01	2013-03-08 13:48:09+01	8	\N	1
54	Dzika noc		9999.99	5	2013-03-04 17:51:33+01	2013-03-04 17:51:33+01	2013-03-04 17:51:33+01	11	\N	1
55	Piwo (parapetowka) 		2.39	9	2013-03-05 07:16:57+01	2013-03-05 05:55:14+01	2013-03-05 07:16:57+01	4	\N	1
56	Piwo (parapetowka) 		2.39	9	2013-03-07 11:14:37+01	2013-03-05 05:55:41+01	2013-03-07 11:14:37+01	2	\N	1
57	Zakupy Ikea	Deska + suszarki	15.99	1	2013-03-07 11:15:32+01	2013-03-05 08:10:05+01	2013-03-07 11:15:32+01	2	\N	1
58	Zakupy Ikea	Deska + suszarki	15.99	1	2013-03-05 14:34:50+01	2013-03-05 08:10:05+01	2013-03-05 14:34:50+01	3	\N	1
59	Zakupy Ikea	Deska + suszarki	15.99	1	2013-03-05 11:03:42+01	2013-03-05 08:10:05+01	2013-03-05 11:03:42+01	4	\N	1
60	Zakupy Ikea	Deska + suszarki	15.99	1	2013-03-05 09:35:30+01	2013-03-05 08:10:05+01	2013-03-05 09:35:30+01	9	\N	1
61	bezkorkowa tuba	Żubry	4.80	1	2013-03-07 11:15:23+01	2013-03-05 08:10:43+01	2013-03-07 11:15:23+01	2	\N	1
62	bezkorkowa tuba	Żubry	4.80	1	2013-03-05 08:50:57+01	2013-03-05 08:11:02+01	2013-03-05 08:50:57+01	9	\N	1
63	bezkorkowa tuba	Żubr	2.40	1	2013-03-05 11:03:27+01	2013-03-05 08:11:21+01	2013-03-05 11:03:27+01	4	\N	1
64	bezkorkowa tuba	Żubr	2.40	1	2013-03-05 14:34:49+01	2013-03-05 08:11:38+01	2013-03-05 14:34:49+01	3	\N	1
65	Przelew za kaucje	doszedł.	380.00	2	2013-03-05 15:59:57+01	2013-03-05 15:59:57+01	2013-03-05 15:59:57+01	4	\N	1
66	przelew za kaucję		380.00	3	2013-03-05 16:13:43+01	2013-03-05 16:08:29+01	2013-03-05 16:13:43+01	4	\N	1
67	Dług z arkusza kalkulacyjnego		540.73	9	2013-03-05 19:28:53+01	2013-03-05 19:18:41+01	2013-03-05 19:28:53+01	1	\N	1
68	Piwo tuba	Piwo Lech	2.88	2	2013-03-07 11:41:10+01	2013-03-07 11:15:14+01	2013-03-07 11:41:10+01	1	\N	1
69	Śniadanie bułki	2xbułka duża	0.90	2	2013-03-07 11:41:11+01	2013-03-07 11:16:06+01	2013-03-07 11:41:11+01	1	\N	1
71	spłata ioweyou.pl via przelew	Money transfer	11.68	2	2013-03-11 13:55:39+01	2013-03-07 13:48:43+01	2013-03-11 13:55:39+01	1	\N	1
72	spłata ioweyou.pl via przelew	50pln via bank transfer	50.00	2	2013-03-11 16:01:06+01	2013-03-07 13:54:43+01	2013-03-11 16:01:06+01	4	\N	1
73	Stare długi	kaucja + zakupy + pierdoły - pierdoły + odsetki	617.99	4	2013-03-07 15:21:22+01	2013-03-07 15:08:30+01	2013-03-07 15:21:22+01	1	\N	1
74	zwrot za bezkorkowa tube i ikea		20.00	3	2013-03-09 12:53:02+01	2013-03-07 18:08:29+01	2013-03-09 12:53:02+01	1	\N	1
75	Piwo z parapetowki i chleb	Cleb oraz chleb orkiszowy 2.39 + 3.99	6.38	9	2013-03-08 13:21:26+01	2013-03-07 18:15:09+01	2013-03-08 13:21:26+01	3	\N	1
3	Różnica za pizze	niech cię gęś kopnie.	3.00	7	2013-02-21 12:30:40+01	2013-02-21 12:30:40+01	2013-02-21 12:30:40+01	4	\N	1
26	Stafy na mieszkanie	1/2 * Prześcieradło, miska, krzesło obrotowe,  pudełko.	171.96	4	2013-03-02 22:20:57+01	2013-03-01 09:59:56+01	2013-03-02 22:20:57+01	3	\N	1
70	Śniadanie czwartek	3xkajzerka+szynka chłopska	5.33	2	2013-03-07 11:41:11+01	2013-03-07 11:16:43+01	2013-03-07 11:41:11+01	1	\N	1
76	Piwo Tyskie	Czwartkowe piwo szt. 1	2.55	2	2013-03-08 12:30:20+01	2013-03-08 12:29:16+01	2013-03-08 12:30:20+01	4	\N	1
77	Frytki śniadanie		3.50	2	2013-03-09 12:52:49+01	2013-03-08 12:30:13+01	2013-03-09 12:52:49+01	1	\N	1
78	AlePizza	SZYBKO SIANO BOM SPŁUKANY!	15.00	4	2013-03-09 12:52:50+01	2013-03-08 17:13:30+01	2013-03-09 12:52:50+01	1	\N	1
79	AlePizza	SZYBKO SIANO BOM SPŁUKANY!	15.00	4	2013-03-11 22:23:30+01	2013-03-08 17:13:30+01	2013-03-11 22:23:30+01	2	\N	1
80	AlePizza	SZYBKO SIANO BOM SPŁUKANY!	15.00	4	2013-03-10 00:34:09+01	2013-03-08 17:13:30+01	2013-03-10 00:34:09+01	3	\N	1
82	Zwrot za kaucje	przelew razem z czynszem	380.00	1	2013-03-12 12:43:53+01	2013-03-11 13:56:16+01	2013-03-12 12:43:53+01	4	\N	1
81	zwrot		40.00	3	2013-03-11 16:00:53+01	2013-03-10 18:40:47+01	2013-03-11 16:00:53+01	4	\N	1
83	Internet za marzec	Opłata za okres od 25.02 do 24.03	17.50	9	2013-03-12 13:43:38+01	2013-03-11 17:25:52+01	2013-03-12 13:43:38+01	1	\N	1
84	Internet za marzec	Opłata za okres od 25.02 do 24.03	17.50	9	2013-03-12 17:21:49+01	2013-03-11 17:26:15+01	2013-03-12 17:21:49+01	2	\N	1
85	Internet za marzec	Opłata za okres od 25.02 do 24.03	17.50	9	2013-03-13 20:50:33+01	2013-03-11 17:26:32+01	2013-03-13 20:50:33+01	3	\N	1
86	Internet za marzec	Opłata za okres od 25.02 do 24.03	17.50	9	2013-03-11 21:57:06+01	2013-03-11 17:26:46+01	2013-03-11 21:57:06+01	4	\N	1
87	Źle naliczona opłata za internet		22.00	1	2013-03-11 17:27:20+01	2013-03-11 17:27:20+01	2013-03-11 17:27:20+01	9	\N	1
88	Źle naliczona opłata za internet		22.00	4	2013-03-11 17:27:44+01	2013-03-11 17:27:44+01	2013-03-11 17:27:44+01	9	\N	1
101	setka	flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)	19.50	4	2013-03-12 13:49:16+01	2013-03-12 01:06:30+01	2013-03-12 13:49:16+01	1	\N	1
102	setka	flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)	19.50	4	2013-03-12 17:21:48+01	2013-03-12 01:06:30+01	2013-03-12 17:21:48+01	2	\N	1
103	setka	flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)	19.50	4	2013-03-13 18:11:34+01	2013-03-12 01:06:30+01	2013-03-13 18:11:34+01	13	\N	1
104	przelew	1,51 doliczone do przelewu za gyros (tutaj nie uwzgledniony)	1.51	4	2013-03-12 11:51:17+01	2013-03-12 11:51:17+01	2013-03-12 11:51:17+01	8	\N	1
105	poprawka		3.02	8	2013-03-12 12:41:04+01	2013-03-12 11:53:23+01	2013-03-12 12:41:04+01	4	\N	1
106	Gyros drobiowy	!	10.50	4	2013-03-12 12:45:14+01	2013-03-12 12:41:44+01	2013-03-12 12:45:14+01	8	\N	1
107	Setka	Warka - 3\\r\\nSetka 20,5	23.50	1	2013-03-12 14:54:41+01	2013-03-12 13:45:47+01	2013-03-12 14:54:41+01	4	\N	1
108	Setka	Setka 20,5\\r\\nŻywiec 3.8\\r\\nŻywiec 3.5	27.80	1	2013-03-12 17:21:42+01	2013-03-12 13:47:19+01	2013-03-12 17:21:42+01	2	\N	1
109	Setka	Butelka wódy ;d	12.50	1	2013-03-13 18:11:33+01	2013-03-12 13:48:00+01	2013-03-13 18:11:33+01	13	\N	1
110	Piwo i mars	Warka 3.5\\r\\nMars 1.69	5.19	1	2013-03-13 20:50:32+01	2013-03-12 13:48:44+01	2013-03-13 20:50:32+01	3	\N	1
111	Setka	Setka	1.00	2	2013-03-12 17:24:20+01	2013-03-12 17:22:52+01	2013-03-12 17:24:20+01	1	\N	1
112	Setka	Setka	1.00	2	2013-03-13 23:46:54+01	2013-03-12 17:22:52+01	2013-03-13 23:46:54+01	4	\N	1
114	setka	setka bo nie ogarniam	3.00	2	2013-03-12 17:24:21+01	2013-03-12 17:23:53+01	2013-03-12 17:24:21+01	1	\N	1
113	Setka	Setka	1.00	2	2013-03-13 18:11:31+01	2013-03-12 17:22:52+01	2013-03-13 18:11:31+01	13	\N	1
115	setka	setka bo nie ogarniam	3.00	2	2013-03-13 23:46:48+01	2013-03-12 17:23:53+01	2013-03-13 23:46:48+01	4	\N	1
116	setka	setka bo nie ogarniam	3.00	2	2013-03-13 18:11:29+01	2013-03-12 17:23:53+01	2013-03-13 18:11:29+01	13	\N	1
117	Spłata częściowa		1000.00	4	2013-03-12 19:45:55+01	2013-03-12 19:45:55+01	2013-03-12 19:45:55+01	9	\N	1
118	setka		4.00	13	2013-03-13 22:20:21+01	2013-03-13 18:09:59+01	2013-03-13 22:20:21+01	1	\N	1
120	setka		4.00	13	2013-03-13 23:45:53+01	2013-03-13 18:10:58+01	2013-03-13 23:45:53+01	4	\N	1
121	stafy na obiad i chleb		10.00	3	2013-03-13 23:46:45+01	2013-03-13 20:52:44+01	2013-03-13 23:46:45+01	4	\N	1
125	test		0.50	4	\N	2013-03-15 20:14:57.931377+01	2013-03-18 01:28:38.275739+01	9	\N	3
138	Pizza		16.00	4	2013-03-18 01:26:42.60792+01	2013-03-18 01:23:28.482238+01	2013-03-18 01:26:42.610156+01	3	2013-03-18 01:26:06.44168+01	1
135	Napój we freshu		2.29	9	2013-03-18 01:27:20.15088+01	2013-03-17 18:29:54.160604+01	2013-03-18 01:27:20.152997+01	4	\N	1
134	piwo + baton		2.50	1	2013-03-18 01:27:28.873398+01	2013-03-16 21:52:44.842703+01	2013-03-18 01:27:28.87622+01	3	\N	1
132	Piwo		9.70	1	2013-03-18 01:27:43.992303+01	2013-03-16 21:51:58.966037+01	2013-03-18 01:27:43.994293+01	4	\N	1
127	Zeszłotygodniowa podróż do IKEA		5.00	9	2013-03-18 01:28:02.189275+01	2013-03-16 08:55:34.862659+01	2013-03-18 01:28:02.191407+01	4	\N	1
1	4 piersi z kurczaka i waDŻywa		17.00	3	2013-03-18 01:28:14.780038+01	2013-03-15 10:29:15.281155+01	2013-03-18 01:28:14.782152+01	4	\N	1
122	dentysta		100.00	1	2013-03-13 20:56:24+01	2013-03-13 20:56:24+01	2013-03-13 20:56:24+01	3	\N	1
137	Błąd w rachunku	Bo nie ogarniam	9.49	2	2013-03-18 14:03:11.166298+01	2013-03-17 19:05:21.071387+01	2013-03-18 14:03:11.168637+01	1	\N	1
124	test		0.50	4	\N	2013-03-15 20:14:57.924401+01	2013-03-18 01:29:05.801297+01	3	\N	3
130	Piwka	Harnasie	6.00	9	2013-03-18 01:29:09.247732+01	2013-03-16 21:46:35.919881+01	2013-03-18 01:29:09.24974+01	3	\N	1
2	test		0.50	4	\N	2013-03-15 10:38:14.632423+01	2013-03-18 01:29:13.177689+01	1	\N	3
126	Zeszłotygodniowa podróż do IKEA		5.00	9	2013-03-18 01:29:17.253499+01	2013-03-16 08:55:34.842515+01	2013-03-18 01:29:17.25556+01	3	\N	1
8	Curchuck	Dostałem piątaka w łapę	2.41	9	2013-03-18 01:29:24.975169+01	2013-03-15 19:48:48.800601+01	2013-03-18 01:29:24.977612+01	3	\N	1
131	Harnaś		2.00	9	2013-03-18 18:56:32.741184+01	2013-03-16 21:46:56.129239+01	2013-03-18 18:56:32.743634+01	2	\N	1
129	pizzastation	śniadanie mistrzów	22.00	2	2013-03-18 01:48:11.447218+01	2013-03-16 12:32:25.354915+01	2013-03-18 01:48:11.44927+01	1	\N	1
133	piwo + baton		2.50	1	2013-03-18 13:40:03.842695+01	2013-03-16 21:52:17.730737+01	2013-03-18 13:40:03.846231+01	3	2013-03-18 01:28:54.217624+01	1
123	przelew - dentysta i reszta		103.58	3	2013-03-18 14:02:27.553014+01	2013-03-13 20:58:04+01	2013-03-18 14:02:27.555393+01	1	\N	1
136	Niedziela, Dzień Pański	Pierogi 6.99\r\nZapiekanka 5.99 x2	9.48	2	2013-03-18 14:03:05.827896+01	2013-03-17 19:04:19.152695+01	2013-03-18 14:03:05.830237+01	1	\N	1
128	tuba	Zapoja i alko	23.00	2	2013-03-18 19:05:43.128748+01	2013-03-16 12:31:39.70296+01	2013-03-18 19:05:43.131033+01	9	\N	1
119	setka		4.00	13	2013-03-18 19:11:23.493391+01	2013-03-13 18:10:38+01	2013-03-18 19:11:23.495567+01	2	\N	1
140	Biurko	Biurko via bank transfer	25.00	2	\N	2013-03-18 19:09:45.454208+01	2013-03-18 19:11:27.402484+01	4	2013-03-18 19:11:27.400357+01	2
139	spłata ioweyou.pl via bank transfer	Spłata ioweyou.pl via bank transfer	49.95	2	2013-03-18 19:13:03.78647+01	2013-03-18 19:00:15.094104+01	2013-03-18 19:13:03.789081+01	4	\N	1
141	Biurko		25.00	4	2013-03-18 19:15:38.907093+01	2013-03-18 19:11:59.051759+01	2013-03-18 19:15:38.909316+01	2	\N	1
142	Biurko		25.00	4	2013-03-18 19:17:10.87297+01	2013-03-18 19:11:59.081067+01	2013-03-18 19:17:10.87514+01	3	\N	1
143	Zakupy w ikea	Te kiedy oddawaliśmy krzesło. 	54.50	4	2013-03-18 19:17:28.056278+01	2013-03-18 19:16:01.008962+01	2013-03-18 19:17:28.058433+01	3	\N	1
144	Biurko	biurko via przelew	25.00	2	2013-03-18 20:18:05.736927+01	2013-03-18 19:16:40.685624+01	2013-03-18 20:18:05.739161+01	4	\N	1
145	test	tests	50.00	8	\N	2013-03-19 12:04:16.141732+01	2013-03-19 12:06:41.260108+01	4	2013-03-19 12:05:49.52086+01	3
147	lasania		11.00	7	2013-03-19 15:04:38.479312+01	2013-03-19 15:03:27.202406+01	2013-03-19 15:04:38.4821+01	4	\N	1
146	pizzała		3.50	7	2013-03-19 21:06:17.83945+01	2013-03-19 15:02:03.373682+01	2013-03-19 21:06:17.843112+01	8	\N	1
148	Zakupy CAREFOUR	3x WADŻYW 3,59, MJUT 19,89, OLIWA 17,79	48.65	9	2013-03-22 17:36:33.190009+01	2013-03-19 19:58:21.483358+01	2013-03-22 17:36:33.192698+01	3	\N	1
150	perfectum template	https://wrapbootstrap.com/theme/perfectum-dashboard-admin-template-WB0PHMG9K	11.33	8	\N	2013-03-19 21:08:07.919303+01	2013-03-19 21:08:07.919383+01	7	\N	0
149	perfectum template	https://wrapbootstrap.com/theme/perfectum-dashboard-admin-template-WB0PHMG9K	11.33	8	2013-03-20 20:15:42.171518+01	2013-03-19 21:08:07.899788+01	2013-03-20 20:15:42.17452+01	4	\N	1
151	Duży gyros baranina		16.50	4	\N	2013-03-21 15:56:18.722012+01	2013-03-21 15:56:18.722092+01	7	\N	0
153	Wiarus 	dokładałem ci do żeberek.	6.00	4	\N	2013-03-22 15:13:56.272171+01	2013-03-22 15:13:56.272236+01	8	\N	0
154	Zupa z wiarusa		3.00	4	\N	2013-03-22 15:14:10.140983+01	2013-03-22 15:14:15.767291+01	7	\N	0
152	2kc tuba		3.00	2	2013-03-22 15:14:25.074425+01	2013-03-22 10:45:30.212663+01	2013-03-22 15:14:25.077408+01	4	\N	1
155	zakupy	Jablka i mleko	4.22	2	2013-03-23 08:02:25.775219+01	2013-03-22 21:43:06.540038+01	2013-03-23 08:02:25.778286+01	9	\N	1
156	Legyros przy woodzie	jw	16.50	4	\N	2013-03-23 13:21:14.383126+01	2013-03-23 13:21:14.383277+01	2	\N	0
157	proba		2.00	3	\N	2013-03-23 18:45:26.130204+01	2013-03-23 18:46:22.308753+01	4	\N	3
158	piwo + hotdog		14.00	1	\N	2013-03-24 11:13:47.272784+01	2013-03-24 11:13:47.272934+01	4	\N	0
\.


--
-- Name: entry_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('entry_entry_id_seq', 158, true);


--
-- Data for Name: entry_entrycomment; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY entry_entrycomment (id, content, user_id, created_at, updated_at, entry_id) FROM stdin;
1	sam płać yetti	3	2013-03-18 01:26:27.2896+01	2013-03-18 01:26:27.289646+01	138
2	DAMN YOU!	4	2013-03-18 01:27:39.995971+01	2013-03-18 01:27:39.996022+01	132
3	dwa razy powtórzyłeś to samo	3	2013-03-18 01:28:42.543927+01	2013-03-18 01:28:42.543982+01	133
4	bo miało być 5zł, ale nie odznaczyłem na dole "include me" :/	1	2013-03-18 01:46:06.137577+01	2013-03-18 01:46:06.137628+01	133
5	ok :)	3	2013-03-18 13:40:01.230356+01	2013-03-18 13:40:01.230413+01	133
6	czemu nie zaakceptowane?	3	2013-03-18 13:40:46.032539+01	2013-03-18 13:40:46.032594+01	123
7	dodałeś mi dług.	4	2013-03-18 19:11:33.759254+01	2013-03-18 19:11:33.759305+01	140
8	nie skminiłem że to spłata.	4	2013-03-18 19:13:24.48447+01	2013-03-18 19:13:24.484519+01	140
9	nie ogarniam co się stało.\r\nWysłałem 49.95 spłaty i drugi na 25 za biurko	2	2013-03-18 19:18:12.859748+01	2013-03-18 19:18:12.864036+01	144
10	to zaakceptowalem poprzednie odrzucilem. takze jest git.	4	2013-03-18 20:19:01.250023+01	2013-03-18 20:19:01.250083+01	144
11	bla bla dogadujemy sie	4	2013-03-19 12:06:00.245457+01	2013-03-19 12:06:00.245512+01	145
12	nie dzrzyj łacha KURWA!	4	2013-03-19 15:04:36.126934+01	2013-03-19 15:04:36.127005+01	147
13	proba	3	2013-03-23 18:46:06.995422+01	2013-03-23 18:46:06.995485+01	157
\.


--
-- Name: entry_entrycomment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('entry_entrycomment_id_seq', 13, true);


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY social_auth_association (id, server_url, handle, secret, issued, lifetime, assoc_type) FROM stdin;
\.


--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('social_auth_association_id_seq', 1, false);


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY social_auth_nonce (id, server_url, "timestamp", salt) FROM stdin;
\.


--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('social_auth_nonce_id_seq', 1, false);


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY social_auth_usersocialauth (id, user_id, provider, uid, extra_data) FROM stdin;
7	7	facebook	100000061884540	{"access_token": "AAAGl12e8jmoBAJgjBZBjC2fCWjqHNZBcVDmW8Y1ZB1NLdvlgVpD6l9osEhfl3k9GF2BdfoOQGSY3lWSvdrjvXbO8peN1YPzlxANOShqZAgZDZD", "expires": "2936817", "id": "100000061884540"}
14	14	facebook	100001081417899	{"access_token": "AAAGl12e8jmoBADI26elAvpZC3QPASPMUjWZAXL5GwTZC6i7kIFpEFDM1F9gMee9C2nuNn7VeeQR7WajT7aDUfTRBU3LJkrgeEokKj2K1AZDZD", "expires": "5183998", "id": "100001081417899"}
6	6	facebook	100001635932210	{}
1	1	facebook	100000467382825	{"access_token": "AAAGl12e8jmoBAEgvOGmw6SJ5H0NjYLZARRMAHEDzX1cOp1CNWPgfKehXS3g4n83NGcDeLgt0IxkQwrHB43JzbWKEz0YeC7N212Bz3vwZDZD", "expires": "4953572", "id": "100000467382825"}
4	4	facebook	100000284981757	{"access_token": "AAAGl12e8jmoBAF695UKjXzeQQyWLt44wl3eJCZBcaCFa6faexF24htlXaFKlfsvIEwDZAWkVRIweN3eZBzShZCTLlDk365KKbDTZACDU0mQZDZD", "expires": "5183998", "id": "100000284981757"}
10	10	facebook	100002323554962	{}
11	11	facebook	1183028056	{}
12	12	facebook	100000635717443	{}
2	2	facebook	100000431944095	{"access_token": "AAAGl12e8jmoBAHmqfVnto919hgKfS5pmBW9ZA0sQvVkgDaHmwhaF385QEEE3ZBIdoUdHqZBGgIFKxD0iCtUJjJV3TnGkU5nyciKnafmvwZDZD", "expires": "5182915", "id": "100000431944095"}
5	5	facebook	100003040397216	{"id": "100003040397216"}
15	15	facebook	100000815243708	{"access_token": "AAAGl12e8jmoBAOPHeiBDRMonCZAIRZA5W44eX9BDrZAiy60GEMTXbIpMnvFVRP19sjDkY9GXGjqFeKikJXUsOdo3ZBRZBWn9jByeffIm5NgZDZD", "expires": "5183998", "id": "100000815243708"}
3	3	facebook	1488867719	{"access_token": "AAAGl12e8jmoBAHyeCXaC2ZBKpucZBacFZCvQzm06QGzDEYcSZBG5M7dVavbaqKTZAqHzda02gcVMm680vbWFN4ZAWQpTfwffxXF6URSLyIkgZDZD", "expires": "5183999", "id": "1488867719"}
13	13	facebook	100005421398990	{"access_token": "AAAGl12e8jmoBAGbHMZAgRviRM7XExZAxaghGCgRL1jz5SZAnjyTXtmHHPv795RfJtBkYyi2jYQLr3DyzEnWILQdt1oRMEUKeLRjlk8o2wZDZD", "expires": "5183960", "id": "100005421398990"}
16	16	facebook	100005512869149	{"access_token": "AAAGl12e8jmoBAOniZBNiG9B6wZCytdvtbmZBJfPZAZCgfidYk1UZAFJ5bvEv2yqUG3rg7AGTRYpmMZCck1LqZAFvxtZAaxRn5qb1sC0ez1B2xPgZDZD", "expires": "5183997", "id": "100005512869149"}
9	9	facebook	100002510910651	{"access_token": "AAAGl12e8jmoBAN7WlqTEm5aJjJMLnv8JeZCijxEPzBJFZCYRgMJHJA4ovTv0Jyn8HxQaaJQmhDIuwQIOd7t7wqVQZBHocdAArENfE84zwZDZD", "expires": "5183998", "id": "100002510910651"}
8	8	facebook	100001412702235	{"access_token": "AAAGl12e8jmoBAIuvIfpriP9PQ0ZBNzzAwlsZCwm4EOJsKiyPn3DwG3IuQpaUTCkgntn1ZAo0pTUM3zkg7LlMZAkTf2FBLHsg30RShfT2fwZDZD", "expires": "5183999", "id": "100001412702235"}
\.


--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('social_auth_usersocialauth_id_seq', 16, true);


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
1	user	0001_initial	2013-03-15 00:26:02.923697+01
2	entry	0001_initial	2013-03-15 00:26:33.969168+01
3	entry	0002_auto__del_field_entry_status__del_field_entry_debtor__del_field_entry_	2013-03-15 00:38:03.582722+01
4	social_auth	0001_initial	2013-03-15 00:44:17.548652+01
5	entry	0003_auto__del_field_entry_deptor__add_field_entry_debtor__add_field_entry_	2013-03-15 01:17:29.077761+01
6	entry	0004_auto__del_field_entry_status	2013-03-15 02:59:41.55555+01
7	entry	0005_auto__add_entrycomment__add_field_entry_status	2013-03-18 01:10:27.461483+01
\.


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 7, true);


--
-- Data for Name: user_friendship; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY user_friendship (id, creator_id, friend_id, created_at) FROM stdin;
1	3	1	2013-02-19 22:59:25+01
2	4	3	2013-02-19 23:03:07+01
3	4	2	2013-02-19 23:03:07+01
4	4	1	2013-02-19 23:03:07+01
5	5	3	2013-02-20 11:51:37+01
6	5	4	2013-02-20 11:51:37+01
7	5	2	2013-02-20 11:51:37+01
8	5	1	2013-02-20 11:51:37+01
9	6	3	2013-02-20 12:22:36+01
10	6	4	2013-02-20 12:22:36+01
11	6	2	2013-02-20 12:22:36+01
12	6	1	2013-02-20 12:22:36+01
13	6	5	2013-02-20 12:22:37+01
14	7	4	2013-02-21 12:28:27+01
15	8	7	2013-02-21 12:38:27+01
16	8	4	2013-02-21 12:38:27+01
17	9	3	2013-02-21 19:18:31+01
18	9	4	2013-02-21 19:18:31+01
19	9	2	2013-02-21 19:18:31+01
20	9	1	2013-02-21 19:18:31+01
21	9	6	2013-02-21 19:18:31+01
22	9	5	2013-02-21 19:18:31+01
23	10	3	2013-02-24 00:47:06+01
24	10	4	2013-02-24 00:47:07+01
25	10	2	2013-02-24 00:47:07+01
26	10	1	2013-02-24 00:47:07+01
27	10	6	2013-02-24 00:47:07+01
28	10	9	2013-02-24 00:47:07+01
29	10	5	2013-02-24 00:47:07+01
30	1	2	2013-02-24 00:47:07+01
31	11	3	2013-03-04 17:49:51+01
32	11	4	2013-03-04 17:49:51+01
33	11	10	2013-03-04 17:49:52+01
34	11	5	2013-03-04 17:49:52+01
35	12	7	2013-03-06 11:26:34+01
36	12	4	2013-03-06 11:26:34+01
37	12	8	2013-03-06 11:26:34+01
38	13	3	2013-03-11 22:15:12+01
39	13	4	2013-03-11 22:15:12+01
40	13	2	2013-03-11 22:15:12+01
41	13	1	2013-03-11 22:15:12+01
42	13	6	2013-03-11 22:15:12+01
43	13	9	2013-03-11 22:15:12+01
44	13	5	2013-03-11 22:15:12+01
\.


--
-- Name: user_friendship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('user_friendship_id_seq', 44, true);


--
-- Data for Name: user_userclient; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY user_userclient (id, name, user_id) FROM stdin;
\.


--
-- Name: user_userclient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('user_userclient_id_seq', 1, false);


--
-- Data for Name: user_userprofile; Type: TABLE DATA; Schema: public; Owner: ioweyou
--

COPY user_userprofile (id, user_id, photo, bank_account) FROM stdin;
1	1		
2	2		
3	3		
4	4		
5	5		
6	6		
7	7		
8	8		
9	9		
10	10		
11	11		
12	12		
13	13		
14	14		
15	15		
16	16		
\.


--
-- Name: user_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ioweyou
--

SELECT pg_catalog.setval('user_userprofile_id_seq', 16, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: entry_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY entry_entry
    ADD CONSTRAINT entry_entry_pkey PRIMARY KEY (id);


--
-- Name: entry_entrycomment_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY entry_entrycomment
    ADD CONSTRAINT entry_entrycomment_pkey PRIMARY KEY (id);


--
-- Name: social_auth_association_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth_provider_2f763109e2c4a1fb_uniq; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_2f763109e2c4a1fb_uniq UNIQUE (provider, uid);


--
-- Name: south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: user_friendship_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY user_friendship
    ADD CONSTRAINT user_friendship_pkey PRIMARY KEY (id);


--
-- Name: user_userclient_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY user_userclient
    ADD CONSTRAINT user_userclient_pkey PRIMARY KEY (id);


--
-- Name: user_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY user_userprofile
    ADD CONSTRAINT user_userprofile_pkey PRIMARY KEY (id);


--
-- Name: user_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: ioweyou; Tablespace: 
--

ALTER TABLE ONLY user_userprofile
    ADD CONSTRAINT user_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_like; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_group_name_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_like; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX auth_user_username_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_like; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX django_session_session_key_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: entry_entry_debtor_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX entry_entry_debtor_id ON entry_entry USING btree (debtor_id);


--
-- Name: entry_entry_lender_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX entry_entry_lender_id ON entry_entry USING btree (lender_id);


--
-- Name: entry_entrycomment_entry_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX entry_entrycomment_entry_id ON entry_entrycomment USING btree (entry_id);


--
-- Name: entry_entrycomment_user_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX entry_entrycomment_user_id ON entry_entrycomment USING btree (user_id);


--
-- Name: social_auth_usersocialauth_user_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX social_auth_usersocialauth_user_id ON social_auth_usersocialauth USING btree (user_id);


--
-- Name: user_friendship_creator_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX user_friendship_creator_id ON user_friendship USING btree (creator_id);


--
-- Name: user_friendship_friend_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX user_friendship_friend_id ON user_friendship USING btree (friend_id);


--
-- Name: user_userclient_user_id; Type: INDEX; Schema: public; Owner: ioweyou; Tablespace: 
--

CREATE INDEX user_userclient_user_id ON user_userclient USING btree (user_id);


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: creator_id_refs_id_47751bbdeac1aaa7; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_friendship
    ADD CONSTRAINT creator_id_refs_id_47751bbdeac1aaa7 FOREIGN KEY (creator_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debtor_id_refs_id_585ec167def92386; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY entry_entry
    ADD CONSTRAINT debtor_id_refs_id_585ec167def92386 FOREIGN KEY (debtor_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: entry_id_refs_id_d406936b9d69bbd; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY entry_entrycomment
    ADD CONSTRAINT entry_id_refs_id_d406936b9d69bbd FOREIGN KEY (entry_id) REFERENCES entry_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: friend_id_refs_id_47751bbdeac1aaa7; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_friendship
    ADD CONSTRAINT friend_id_refs_id_47751bbdeac1aaa7 FOREIGN KEY (friend_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lender_id_refs_id_585ec167def92386; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY entry_entry
    ADD CONSTRAINT lender_id_refs_id_585ec167def92386 FOREIGN KEY (lender_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_23ae1fa1db3a7c80; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_userprofile
    ADD CONSTRAINT user_id_refs_id_23ae1fa1db3a7c80 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_3b2e9d30d7d102ae; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY entry_entrycomment
    ADD CONSTRAINT user_id_refs_id_3b2e9d30d7d102ae FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_40c41112; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4a00d015b2eb761; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY user_userclient
    ADD CONSTRAINT user_id_refs_id_4a00d015b2eb761 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4dc23c39; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_529c317860fa311b; Type: FK CONSTRAINT; Schema: public; Owner: ioweyou
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT user_id_refs_id_529c317860fa311b FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

