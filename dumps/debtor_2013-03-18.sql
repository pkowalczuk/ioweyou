# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.28)
# Database: debtor
# Generation Time: 2013-03-17 23:42:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_group
# ------------------------------------------------------------



# Dump of table auth_group_permissions
# ------------------------------------------------------------



# Dump of table auth_permission
# ------------------------------------------------------------

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`)
VALUES
	(1,'Can add permission',1,'add_permission'),
	(2,'Can change permission',1,'change_permission'),
	(3,'Can delete permission',1,'delete_permission'),
	(4,'Can add group',2,'add_group'),
	(5,'Can change group',2,'change_group'),
	(6,'Can delete group',2,'delete_group'),
	(7,'Can add user',3,'add_user'),
	(8,'Can change user',3,'change_user'),
	(9,'Can delete user',3,'delete_user'),
	(10,'Can add content type',4,'add_contenttype'),
	(11,'Can change content type',4,'change_contenttype'),
	(12,'Can delete content type',4,'delete_contenttype'),
	(13,'Can add session',5,'add_session'),
	(14,'Can change session',5,'change_session'),
	(15,'Can delete session',5,'delete_session'),
	(16,'Can add site',6,'add_site'),
	(17,'Can change site',6,'change_site'),
	(18,'Can delete site',6,'delete_site'),
	(19,'Can add log entry',7,'add_logentry'),
	(20,'Can change log entry',7,'change_logentry'),
	(21,'Can delete log entry',7,'delete_logentry'),
	(22,'Can add user profile',8,'add_userprofile'),
	(23,'Can change user profile',8,'change_userprofile'),
	(24,'Can delete user profile',8,'delete_userprofile'),
	(25,'Can add friendship',9,'add_friendship'),
	(26,'Can change friendship',9,'change_friendship'),
	(27,'Can delete friendship',9,'delete_friendship'),
	(28,'Can add user client',10,'add_userclient'),
	(29,'Can change user client',10,'change_userclient'),
	(30,'Can delete user client',10,'delete_userclient'),
	(31,'Can add entry',11,'add_entry'),
	(32,'Can change entry',11,'change_entry'),
	(33,'Can delete entry',11,'delete_entry'),
	(34,'Can add user social auth',12,'add_usersocialauth'),
	(35,'Can change user social auth',12,'change_usersocialauth'),
	(36,'Can delete user social auth',12,'delete_usersocialauth'),
	(37,'Can add nonce',13,'add_nonce'),
	(38,'Can change nonce',13,'change_nonce'),
	(39,'Can delete nonce',13,'delete_nonce'),
	(40,'Can add association',14,'add_association'),
	(41,'Can change association',14,'change_association'),
	(42,'Can delete association',14,'delete_association'),
	(43,'Can add migration history',15,'add_migrationhistory'),
	(44,'Can change migration history',15,'change_migrationhistory'),
	(45,'Can delete migration history',15,'delete_migrationhistory'),
	(46,'Can add entry comment',16,'add_entrycomment'),
	(47,'Can change entry comment',16,'change_entrycomment'),
	(48,'Can delete entry comment',16,'delete_entrycomment');

/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user
# ------------------------------------------------------------

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`)
VALUES
	(1,'wqwlucky','Łukasz','Staniszewski','wqwlucky@gmail.com','!',0,1,0,'2013-03-05 08:19:08','2013-02-19 22:59:19'),
	(2,'maciej.kuprowski','Maciej','Kuprowsky','maciek220v@interia.pl','!',0,1,0,'2013-03-04 10:40:45','2013-02-19 22:59:21'),
	(3,'agata.adamska','Agata','Adamska','agata_a@o2.pl','!',0,1,0,'2013-03-06 08:37:51','2013-02-19 22:59:23'),
	(4,'piotr.kowalczuk','Piotr','Kowalczuk','imp4ever@gmail.com','!',0,1,0,'2013-03-11 20:42:27','2013-02-19 23:03:06'),
	(5,'tomasz.lis.50','Tomasz','Lis','listomek@gmail.com','!',0,1,0,'2013-02-20 11:51:38','2013-02-20 11:51:36'),
	(6,'arnold.leszczuk.90','Arnold','Leszczuk','tthitt@gmail.com','!',0,1,0,'2013-02-20 12:22:37','2013-02-20 12:22:35'),
	(7,'kamil.biela','Kamil','Biela','kamil.biela@gmail.com','!',0,1,0,'2013-02-21 12:28:27','2013-02-21 12:28:26'),
	(8,'daniel.waligora.902','Daniel','Waligóra','danielwaligora@gmail.com','!',0,1,0,'2013-02-21 12:38:28','2013-02-21 12:38:21'),
	(9,'piotr.breda','Piotr','Breda','miodowar@gmail.com','!',0,1,0,'2013-02-24 14:07:17','2013-02-21 19:18:30'),
	(10,'radoslaw.szynkaruk','Radosław','Szynkaruk','radoslaw.szynkaruk@gmail.com','!',0,1,0,'2013-02-24 16:14:00','2013-02-24 00:47:05'),
	(11,'adrian.smolarczyk','Adrian','Smolarczyk','adriansmolarczyk@gmail.com','!',0,1,0,'2013-03-04 17:49:52','2013-03-04 17:49:50'),
	(12,'qlaczek','Marek','Wilczyński','qlaczek@gmail.com','!',0,1,0,'2013-03-06 11:26:34','2013-03-06 11:26:33');

/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user_groups
# ------------------------------------------------------------



# Dump of table auth_user_user_permissions
# ------------------------------------------------------------



# Dump of table django_admin_log
# ------------------------------------------------------------



# Dump of table django_content_type
# ------------------------------------------------------------

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`)
VALUES
	(1,'permission','auth','permission'),
	(2,'group','auth','group'),
	(3,'user','auth','user'),
	(4,'content type','contenttypes','contenttype'),
	(5,'session','sessions','session'),
	(6,'site','sites','site'),
	(7,'log entry','admin','logentry'),
	(8,'user profile','user','userprofile'),
	(9,'friendship','user','friendship'),
	(10,'user client','user','userclient'),
	(11,'entry','entry','entry'),
	(12,'user social auth','social_auth','usersocialauth'),
	(13,'nonce','social_auth','nonce'),
	(14,'association','social_auth','association'),
	(15,'migration history','south','migrationhistory'),
	(16,'entry comment','entry','entrycomment');

/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_session
# ------------------------------------------------------------

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`)
VALUES
	('g19v3acioknzh1v01nmv8wgqfbaf5tkm','MzRmMDM5ZTkzNjViZjk5OTA0NDVlNzY3OTNhOTkwNjIxNTEyNDcyNTqAAn1xAVUOZmFjZWJvb2tfc3RhdGVYIAAAAGNGbndHa05acDRTbnRXMjMyV3dQbUkzYmRERnVmQ0Z0cy4=','2013-03-27 23:45:18'),
	('xr43wbsd2fqo1elp7ymjel0r4yxaxw0a','ZDRlM2FiNzI4OGYxMzAzNWIzNzk2NDVkNmVmY2QwZDk1YjBlZTBjYTqAAn1xAShVDV9hdXRoX3VzZXJfaWSKAQRVDmZhY2Vib29rX3N0YXRlWCAAAABhOTNGWGU4Nk52TzNJdDUyMXpQVkdCa214aVV0MmF1NFgKAAAAdGVzdGNvb2tpZVgGAAAAd29ya2VkVRJfYXV0aF91c2VyX2JhY2tlbmRVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZFUec29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kWAgAAABmYWNlYm9va1gPAAAAX3Nlc3Npb25fZXhwaXJ5Y2RhdGV0aW1lCmRhdGV0aW1lCnECVQoH3QUKFCoaB5/SY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQMpUnEEhlJxBXUu','2013-05-10 20:42:26');

/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_site
# ------------------------------------------------------------

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;

INSERT INTO `django_site` (`id`, `domain`, `name`)
VALUES
	(1,'example.com','example.com');

/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entry_entry
# ------------------------------------------------------------

LOCK TABLES `entry_entry` WRITE;
/*!40000 ALTER TABLE `entry_entry` DISABLE KEYS */;

INSERT INTO `entry_entry` (`id`, `name`, `description`, `value`, `lender_id`, `debtor_id`, `accepted_at`, `created_at`, `updated_at`, `rejected_at`, `status`)
VALUES
	(3,'Różnica za pizze','niech cię gęś kopnie.','3.00',7,4,'2013-02-21 12:30:40','2013-02-21 12:30:40','2013-02-21 12:30:40',NULL,1),
	(4,'Kasa za popitke u rudego','kupilem kole plus czipsy solone','10.00',3,4,'2013-02-21 12:34:41','2013-02-21 12:34:41','2013-02-21 12:34:41',NULL,1),
	(5,'Hajs dziwko za żarcie!','Gdyby nie ja to byś zdechł z głodu także dawaj hajs dziwko!','10.00',8,4,'2013-02-21 12:40:39','2013-02-21 12:39:55','2013-02-21 12:40:39',NULL,1),
	(6,'Parówki na hotdogi','2 bulki + parówki + musztarda','9.00',4,2,'2013-02-21 19:20:44','2013-02-21 19:16:56','2013-02-21 19:20:44',NULL,1),
	(7,'2 harnasie','bez kaucji','4.78',4,9,'2013-02-21 21:49:39','2013-02-21 19:19:08','2013-02-21 21:49:39',NULL,1),
	(9,'wóda w piątek','Ale jak to w piątek?','12.00',1,4,'2013-02-23 01:29:45','2013-02-22 23:46:53','2013-02-23 01:29:45',NULL,1),
	(10,'Kadarka','Kadarka, chleb, makaron, przecier','19.73',9,4,'2013-02-23 11:50:13','2013-02-23 11:23:26','2013-02-23 11:50:13',NULL,1),
	(11,'chleb tak bardzo ciemny','','3.50',2,9,'2013-02-23 11:54:11','2013-02-23 11:48:45','2013-02-23 11:54:11',NULL,1),
	(12,'Alternative Szpringer','','13.50',9,2,'2013-02-23 14:20:57','2013-02-23 14:20:09','2013-02-23 14:20:57',NULL,1),
	(13,'wódka i energetyki','wódka i energetyki w sobote','20.43',1,4,'2013-02-24 12:31:11','2013-02-23 16:23:30','2013-02-24 12:31:11',NULL,1),
	(14,'nocna buła i zupa','Sobota, sklep 24h przy Bema','7.80',2,4,'2013-02-24 12:31:12','2013-02-24 10:20:39','2013-02-24 12:31:12',NULL,1),
	(15,'Mirinda','','2.19',9,4,'2013-02-24 14:14:53','2013-02-24 14:08:02','2013-02-24 14:14:53',NULL,1),
	(16,'7up','','2.19',9,1,'2013-02-24 20:43:55','2013-02-24 14:08:25','2013-02-24 20:43:55',NULL,1),
	(17,'zakupy','żywiec, fanta i pizza ','16.18',1,4,'2013-02-25 14:36:19','2013-02-24 20:45:35','2013-02-25 14:36:19',NULL,1),
	(18,'El. metalowe/alkohol/owoce','el. metalowe/browar/jabłka','13.00',2,9,'2013-03-01 10:15:30','2013-02-26 17:55:43','2013-03-01 10:15:30',NULL,1),
	(20,'Alkohol + chleb','Chleb żytni & Harna$','5.70',2,9,'2013-03-01 10:17:31','2013-02-26 20:36:56','2013-03-01 10:17:31',NULL,1),
	(21,'kiełbaski','Dawaj siano','3.01',4,8,'2013-03-04 09:05:21','2013-02-27 18:53:30','2013-03-04 09:05:21',NULL,1),
	(22,'Zakupy Ikea','2 szczotki i 4 szklanki','3.03',4,1,'2013-03-04 12:01:41','2013-03-01 09:55:50','2013-03-04 12:01:41',NULL,1),
	(23,'Zakupy Ikea','2 szczotki i 4 szklanki','3.03',4,2,'2013-03-01 11:21:41','2013-03-01 09:56:08','2013-03-01 11:21:41',NULL,1),
	(24,'Zakupy Ikea','2 szczotki i 4 szklanki','3.03',4,3,'2013-03-02 22:20:59','2013-03-01 09:56:25','2013-03-02 22:20:59',NULL,1),
	(25,'Zakupy Ikea','2 szczotki i 4 szklanki','3.03',4,9,'2013-03-01 10:15:35','2013-03-01 09:56:55','2013-03-01 10:15:35',NULL,1),
	(26,'Stafy na mieszkanie','1/2 * Prześcieradło, miska, krzesło obrotowe,  pudełko.','171.96',4,3,'2013-03-02 22:20:57','2013-03-01 09:59:56','2013-03-02 22:20:57',NULL,1),
	(27,'Kaucja','jw','380.00',4,1,'2013-03-04 12:01:45','2013-03-01 10:00:22','2013-03-04 12:01:45',NULL,1),
	(28,'Kaucja','jw','380.00',4,2,'2013-03-03 11:53:54','2013-03-01 10:01:20','2013-03-03 11:53:54',NULL,1),
	(29,'Kaucja','jw','380.00',4,3,'2013-03-02 22:20:56','2013-03-01 10:01:35','2013-03-02 22:20:56',NULL,1),
	(30,'Ipad + ram','jw','1827.53',9,4,'2013-03-01 10:02:29','2013-03-01 10:02:29','2013-03-01 10:02:29',NULL,1),
	(31,'Kaucja','jw','380.00',4,9,'2013-03-01 10:15:36','2013-03-01 10:02:54','2013-03-01 10:15:36',NULL,1),
	(32,'Internet za luty','','22.00',9,4,'2013-03-01 14:11:29','2013-03-01 10:16:35','2013-03-01 14:11:29',NULL,1),
	(33,'Internet za luty','','22.00',9,1,'2013-03-04 12:01:55','2013-03-01 10:17:02','2013-03-04 12:01:55',NULL,1),
	(35,'LeGyros','Za gyros zaplacilismy 49.5. 3x 16zl + przewoz. Daleś mi 14zł.','2.50',4,2,'2013-03-02 22:01:18','2013-03-02 20:09:11','2013-03-02 22:01:18',NULL,1),
	(36,'Zakupy z leclerka','Zakupy, głownie chemia.','44.49',4,1,'2013-03-04 12:02:30','2013-03-02 21:48:26','2013-03-04 12:02:30',NULL,1),
	(37,'Zakupy z leclerka','Głównie chemia.','44.49',4,2,'2013-03-02 22:01:17','2013-03-02 21:48:45','2013-03-02 22:01:17',NULL,1),
	(38,'Zakupy z leclerka','Głównie chemia.','44.49',4,3,'2013-03-02 22:20:55','2013-03-02 21:49:06','2013-03-02 22:20:55',NULL,1),
	(39,'Zakupy z leclerka','Głównie chemia.','44.49',4,9,'2013-03-02 22:00:59','2013-03-02 21:49:26','2013-03-02 22:00:59',NULL,1),
	(40,'Patyczki do uszu','','1.87',4,1,'2013-03-04 12:01:59','2013-03-02 21:51:33','2013-03-04 12:01:59',NULL,1),
	(41,'Wieszaki','','12.76',4,9,'2013-03-02 22:01:07','2013-03-02 21:52:24','2013-03-02 22:01:07',NULL,1),
	(42,'Wóda na parapetówę','3xkrupnik + popita','19.00',4,1,'2013-03-04 12:02:37','2013-03-02 22:18:41','2013-03-04 12:02:37',NULL,1),
	(43,'Wóda na parapetówę','3 x krupnik + popita','19.00',4,2,'2013-03-03 11:53:51','2013-03-02 22:19:04','2013-03-03 11:53:51',NULL,1),
	(44,'Wóda na parapetówę','3 x krupnik i popita.','19.00',4,9,'2013-03-05 05:53:53','2013-03-02 22:19:32','2013-03-05 05:53:53',NULL,1),
	(45,'Poranne zakupy','bułki 1.20\nparówki 3.70\n1/3 jednorazowki 0.03','4.93',4,1,'2013-03-04 12:02:03','2013-03-03 11:44:31','2013-03-04 12:02:03',NULL,1),
	(46,'Mydło w płynie','','1.41',4,1,'2013-03-04 12:02:20','2013-03-03 11:44:58','2013-03-04 12:02:20',NULL,1),
	(47,'Mydło w płynie','','1.41',4,2,'2013-03-03 11:53:50','2013-03-03 11:45:09','2013-03-03 11:53:50',NULL,1),
	(48,'Mydło w płynie','','1.41',4,3,'2013-03-04 12:13:06','2013-03-03 11:45:22','2013-03-04 12:13:06',NULL,1),
	(49,'Mydło w płynie','','1.41',4,9,'2013-03-05 05:54:01','2013-03-03 11:45:32','2013-03-05 05:54:01',NULL,1),
	(50,'Legyros','zestaw tortilia extra','13.50',4,7,'2013-03-04 11:40:01','2013-03-04 11:38:59','2013-03-04 11:40:01',NULL,1),
	(51,'Sałatka Turecka','dziwne ze nie sałatka zydowska','8.50',4,8,'2013-03-08 13:48:09','2013-03-04 11:39:28','2013-03-08 13:48:09',NULL,1),
	(54,'Dzika noc','','9999.99',5,11,'2013-03-04 17:51:33','2013-03-04 17:51:33','2013-03-04 17:51:33',NULL,1),
	(55,'Piwo (parapetowka) ','','2.39',9,4,'2013-03-05 07:16:57','2013-03-05 05:55:14','2013-03-05 07:16:57',NULL,1),
	(56,'Piwo (parapetowka) ','','2.39',9,2,'2013-03-07 11:14:37','2013-03-05 05:55:41','2013-03-07 11:14:37',NULL,1),
	(57,'Zakupy Ikea','Deska + suszarki','15.99',1,2,'2013-03-07 11:15:32','2013-03-05 08:10:05','2013-03-07 11:15:32',NULL,1),
	(58,'Zakupy Ikea','Deska + suszarki','15.99',1,3,'2013-03-05 14:34:50','2013-03-05 08:10:05','2013-03-05 14:34:50',NULL,1),
	(59,'Zakupy Ikea','Deska + suszarki','15.99',1,4,'2013-03-05 11:03:42','2013-03-05 08:10:05','2013-03-05 11:03:42',NULL,1),
	(60,'Zakupy Ikea','Deska + suszarki','15.99',1,9,'2013-03-05 09:35:30','2013-03-05 08:10:05','2013-03-05 09:35:30',NULL,1),
	(61,'bezkorkowa tuba','Żubry','4.80',1,2,'2013-03-07 11:15:23','2013-03-05 08:10:43','2013-03-07 11:15:23',NULL,1),
	(62,'bezkorkowa tuba','Żubry','4.80',1,9,'2013-03-05 08:50:57','2013-03-05 08:11:02','2013-03-05 08:50:57',NULL,1),
	(63,'bezkorkowa tuba','Żubr','2.40',1,4,'2013-03-05 11:03:27','2013-03-05 08:11:21','2013-03-05 11:03:27',NULL,1),
	(64,'bezkorkowa tuba','Żubr','2.40',1,3,'2013-03-05 14:34:49','2013-03-05 08:11:38','2013-03-05 14:34:49',NULL,1),
	(65,'Przelew za kaucje','doszedł.','380.00',2,4,'2013-03-05 15:59:57','2013-03-05 15:59:57','2013-03-05 15:59:57',NULL,1),
	(66,'przelew za kaucję','','380.00',3,4,'2013-03-05 16:13:43','2013-03-05 16:08:29','2013-03-05 16:13:43',NULL,1),
	(67,'Dług z arkusza kalkulacyjnego','','540.73',9,1,'2013-03-05 19:28:53','2013-03-05 19:18:41','2013-03-05 19:28:53',NULL,1),
	(68,'Piwo tuba','Piwo Lech','2.88',2,1,'2013-03-07 11:41:10','2013-03-07 11:15:14','2013-03-07 11:41:10',NULL,1),
	(69,'Śniadanie bułki','2xbułka duża','0.90',2,1,'2013-03-07 11:41:11','2013-03-07 11:16:06','2013-03-07 11:41:11',NULL,1),
	(70,'Śniadanie czwartek','3xkajzerka+szynka chłopska','5.33',2,1,'2013-03-07 11:41:11','2013-03-07 11:16:43','2013-03-07 11:41:11',NULL,1),
	(71,'spłata ioweyou.pl via przelew','Money transfer','11.68',2,1,'2013-03-11 13:55:39','2013-03-07 13:48:43','2013-03-11 13:55:39',NULL,1),
	(72,'spłata ioweyou.pl via przelew','50pln via bank transfer','50.00',2,4,'2013-03-11 16:01:06','2013-03-07 13:54:43','2013-03-11 16:01:06',NULL,1),
	(73,'Stare długi','kaucja + zakupy + pierdoły - pierdoły + odsetki','617.99',4,1,'2013-03-07 15:21:22','2013-03-07 15:08:30','2013-03-07 15:21:22',NULL,1),
	(74,'zwrot za bezkorkowa tube i ikea','','20.00',3,1,'2013-03-09 12:53:02','2013-03-07 18:08:29','2013-03-09 12:53:02',NULL,1),
	(75,'Piwo z parapetowki i chleb','Cleb oraz chleb orkiszowy 2.39 + 3.99','6.38',9,3,'2013-03-08 13:21:26','2013-03-07 18:15:09','2013-03-08 13:21:26',NULL,1),
	(76,'Piwo Tyskie','Czwartkowe piwo szt. 1','2.55',2,4,'2013-03-08 12:30:20','2013-03-08 12:29:16','2013-03-08 12:30:20',NULL,1),
	(77,'Frytki śniadanie','','3.50',2,1,'2013-03-09 12:52:49','2013-03-08 12:30:13','2013-03-09 12:52:49',NULL,1),
	(78,'AlePizza','SZYBKO SIANO BOM SPŁUKANY!','15.00',4,1,'2013-03-09 12:52:50','2013-03-08 17:13:30','2013-03-09 12:52:50',NULL,1),
	(79,'AlePizza','SZYBKO SIANO BOM SPŁUKANY!','15.00',4,2,'2013-03-11 22:23:30','2013-03-08 17:13:30','2013-03-11 22:23:30',NULL,1),
	(80,'AlePizza','SZYBKO SIANO BOM SPŁUKANY!','15.00',4,3,'2013-03-10 00:34:09','2013-03-08 17:13:30','2013-03-10 00:34:09',NULL,1),
	(81,'zwrot','','40.00',3,4,'2013-03-11 16:00:53','2013-03-10 18:40:47','2013-03-11 16:00:53',NULL,1),
	(82,'Zwrot za kaucje','przelew razem z czynszem','380.00',1,4,'2013-03-12 12:43:53','2013-03-11 13:56:16','2013-03-12 12:43:53',NULL,1),
	(83,'Internet za marzec','Opłata za okres od 25.02 do 24.03','17.50',9,1,'2013-03-12 13:43:38','2013-03-11 17:25:52','2013-03-12 13:43:38',NULL,1),
	(84,'Internet za marzec','Opłata za okres od 25.02 do 24.03','17.50',9,2,'2013-03-12 17:21:49','2013-03-11 17:26:15','2013-03-12 17:21:49',NULL,1),
	(85,'Internet za marzec','Opłata za okres od 25.02 do 24.03','17.50',9,3,'2013-03-13 20:50:33','2013-03-11 17:26:32','2013-03-13 20:50:33',NULL,1),
	(86,'Internet za marzec','Opłata za okres od 25.02 do 24.03','17.50',9,4,'2013-03-11 21:57:06','2013-03-11 17:26:46','2013-03-11 21:57:06',NULL,1),
	(87,'Źle naliczona opłata za internet','','22.00',1,9,'2013-03-11 17:27:20','2013-03-11 17:27:20','2013-03-11 17:27:20',NULL,1),
	(88,'Źle naliczona opłata za internet','','22.00',4,9,'2013-03-11 17:27:44','2013-03-11 17:27:44','2013-03-11 17:27:44',NULL,1),
	(101,'setka','flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)','19.50',4,1,'2013-03-12 13:49:16','2013-03-12 01:06:30','2013-03-12 13:49:16',NULL,3),
	(102,'setka','flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)','19.50',4,2,'2013-03-12 17:21:48','2013-03-12 01:06:30','2013-03-12 17:21:48',NULL,0),
	(103,'setka','flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)','19.50',4,13,'2013-03-13 18:11:34','2013-03-12 01:06:30','2013-03-13 18:11:34',NULL,2),
	(104,'Nie bylejaki przelew','agagasgas gas gah','223.00',4,8,'2013-03-12 07:51:17','2013-03-12 07:51:17','2013-03-17 23:05:50',NULL,0),
	(105,'poprawka','','3.02',8,4,'2013-03-12 12:41:04','2013-03-12 11:53:23','2013-03-12 12:41:04',NULL,1),
	(106,'Gyros drobiowy','!','12.12',4,8,'2013-03-12 06:45:14','2013-03-12 06:41:44','2013-03-17 14:21:37',NULL,0),
	(107,'Setka','Warka - 3\nSetka 20,5','23.50',1,4,'2013-03-12 14:54:41','2013-03-12 13:45:47','2013-03-12 14:54:41',NULL,1),
	(108,'Setka','Setka 20,5\nŻywiec 3.8\nŻywiec 3.5','27.80',1,2,'2013-03-12 17:21:42','2013-03-12 13:47:19','2013-03-12 17:21:42',NULL,1),
	(109,'Setka','Butelka wódy ;d','12.50',1,13,'2013-03-13 18:11:33','2013-03-12 13:48:00','2013-03-13 18:11:33',NULL,1),
	(110,'Piwo i mars','Warka 3.5\nMars 1.69','5.19',1,3,'2013-03-13 20:50:32','2013-03-12 13:48:44','2013-03-13 20:50:32',NULL,1),
	(111,'Setka','Setka','1.00',2,1,'2013-03-12 17:24:20','2013-03-12 17:22:52','2013-03-12 17:24:20',NULL,1),
	(112,'Setka','Setka','1.00',2,4,'2013-03-13 23:46:54','2013-03-12 17:22:52','2013-03-13 23:46:54',NULL,1),
	(113,'Setka','Setka','1.00',2,13,'2013-03-13 18:11:31','2013-03-12 17:22:52','2013-03-13 18:11:31',NULL,1),
	(114,'setka','setka bo nie ogarniam','3.00',2,1,'2013-03-12 17:24:21','2013-03-12 17:23:53','2013-03-12 17:24:21',NULL,1),
	(115,'setka','setka bo nie ogarniam','3.00',2,4,'2013-03-13 22:46:48','2013-03-12 16:23:53','2013-03-17 23:05:22','2013-03-17 23:05:22',2),
	(116,'setka','setka bo nie ogarniam','3.00',2,13,'2013-03-13 18:11:29','2013-03-12 17:23:53','2013-03-13 18:11:29',NULL,1),
	(118,'setka','','4.00',13,1,'2013-03-13 22:20:21','2013-03-13 18:09:59','2013-03-13 22:20:21',NULL,1),
	(119,'setka','','4.00',13,2,NULL,'2013-03-13 18:10:38','2013-03-13 18:10:38',NULL,0),
	(120,'setka','','4.00',13,4,'2013-03-17 23:01:32','2013-03-13 17:10:58','2013-03-17 23:01:32',NULL,1),
	(121,'stafy na obiad i chleb','','10.00',3,4,'2013-03-17 22:44:32','2013-03-13 09:52:44','2013-03-17 22:44:32','2013-03-17 13:12:38',1),
	(122,'dentysta','','100.00',1,3,'2013-03-13 20:56:24','2013-03-13 20:56:24','2013-03-13 20:56:24',NULL,1),
	(123,'przelew - dentysta i reszta','','103.58',3,1,NULL,'2013-03-13 20:58:04','2013-03-13 20:58:04',NULL,0),
	(124,'asdfgasdg','fasdfas','1374.67',4,3,NULL,'2013-03-17 23:09:47','2013-03-17 23:09:47',NULL,0),
	(125,'asdfgasdg','fasdfas','1374.67',4,7,NULL,'2013-03-17 23:09:47','2013-03-17 23:09:47',NULL,0);

/*!40000 ALTER TABLE `entry_entry` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entry_entrycomment
# ------------------------------------------------------------

LOCK TABLES `entry_entrycomment` WRITE;
/*!40000 ALTER TABLE `entry_entrycomment` DISABLE KEYS */;

INSERT INTO `entry_entrycomment` (`id`, `content`, `user_id`, `created_at`, `updated_at`, `entry_id`)
VALUES
	(1,'asdasdas',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),
	(2,'NULL12312312',4,'2013-03-08 13:21:26','0000-00-00 00:00:00',121),
	(3,'346346346',NULL,'2013-03-17 22:41:41','2013-03-17 22:41:41',121),
	(4,'qwqwtqwt',NULL,'2013-03-17 22:41:45','2013-03-17 22:41:45',121),
	(5,'15125',4,'2013-03-17 22:42:39','2013-03-17 22:42:39',121),
	(6,'4314261236',4,'2013-03-17 22:42:42','2013-03-17 22:42:42',121),
	(7,'346346',4,'2013-03-17 22:44:27','2013-03-17 22:44:27',121),
	(8,'235235',4,'2013-03-17 23:05:15','2013-03-17 23:05:15',120),
	(9,'2151 5ads hg',4,'2013-03-17 23:21:12','2013-03-17 23:21:12',115);

/*!40000 ALTER TABLE `entry_entrycomment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table social_auth_association
# ------------------------------------------------------------



# Dump of table social_auth_nonce
# ------------------------------------------------------------



# Dump of table social_auth_usersocialauth
# ------------------------------------------------------------

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;

INSERT INTO `social_auth_usersocialauth` (`id`, `user_id`, `provider`, `uid`, `extra_data`)
VALUES
	(1,1,'facebook','100000467382825','{\"access_token\": \"AAAGl12e8jmoBAHo1ZAJe5M6ETyfnrcOfOXCIt1DOZAceceACekqLkxcoV9C5E3US2R3xiQdTRyJDs2q1OSeBc5DljYByafVWw1asv8zgZDZD\", \"expires\": \"5121331\", \"id\": \"100000467382825\"}'),
	(2,2,'facebook','100000431944095','{\"access_token\": \"AAAGl12e8jmoBAHmqfVnto919hgKfS5pmBW9ZA0sQvVkgDaHmwhaF385QEEE3ZBIdoUdHqZBGgIFKxD0iCtUJjJV3TnGkU5nyciKnafmvwZDZD\", \"expires\": \"5183998\", \"id\": \"100000431944095\"}'),
	(3,3,'facebook','1488867719','{\"access_token\": \"AAAGl12e8jmoBAHyeCXaC2ZBKpucZBacFZCvQzm06QGzDEYcSZBG5M7dVavbaqKTZAqHzda02gcVMm680vbWFN4ZAWQpTfwffxXF6URSLyIkgZDZD\", \"expires\": \"5183996\", \"id\": \"1488867719\"}'),
	(4,4,'facebook','100000284981757','{\"access_token\": \"AAABi4iRjtC8BAPLZCdtFablqSxX6nZBbJ4Qd7DggvPa7MrKkDIdboADEtU4sMTS79kpZBDqlEcuWcxKYFDkNZCHlohd6BzZCmCk6LRFf4cQZDZD\", \"expires\": \"5183999\", \"id\": \"100000284981757\"}'),
	(5,5,'facebook','100003040397216','{\"access_token\": \"AAAGl12e8jmoBAIVLhAiSyJhb0X9yK5uljZCbSiR2ZA7ZAzkRYGjMJfiy3jjkgZAtacZCBzgCqmdSloG8EgOLG7tirdfQcZAM7mKAFzV2CHiQZDZD\", \"expires\": \"5183995\", \"id\": \"100003040397216\"}'),
	(6,6,'facebook','100001635932210','{\"access_token\": \"AAAGl12e8jmoBAMeISxOmjAuCO7xgHC74xAJSq5oJrZB2L9z1zHWYpbB5T4ZCvkHPZBnPvR21Y9ZCt2GuPKp1cVmZAI54Wn5SrSso2INwgFgZDZD\", \"expires\": \"5183998\", \"id\": \"100001635932210\"}'),
	(7,7,'facebook','100000061884540','{\"access_token\": \"AAAGl12e8jmoBAJgjBZBjC2fCWjqHNZBcVDmW8Y1ZB1NLdvlgVpD6l9osEhfl3k9GF2BdfoOQGSY3lWSvdrjvXbO8peN1YPzlxANOShqZAgZDZD\", \"expires\": \"5183999\", \"id\": \"100000061884540\"}'),
	(8,8,'facebook','100001412702235','{\"access_token\": \"AAAGl12e8jmoBAIuvIfpriP9PQ0ZBNzzAwlsZCwm4EOJsKiyPn3DwG3IuQpaUTCkgntn1ZAo0pTUM3zkg7LlMZAkTf2FBLHsg30RShfT2fwZDZD\", \"expires\": \"5183999\", \"id\": \"100001412702235\"}'),
	(9,9,'facebook','100002510910651','{\"access_token\": \"AAAGl12e8jmoBAN7WlqTEm5aJjJMLnv8JeZCijxEPzBJFZCYRgMJHJA4ovTv0Jyn8HxQaaJQmhDIuwQIOd7t7wqVQZBHocdAArENfE84zwZDZD\", \"expires\": \"5183998\", \"id\": \"100002510910651\"}'),
	(10,10,'facebook','100002323554962','{\"access_token\": \"AAAGl12e8jmoBAIM48bWnlC2UGUPEljFzIlWgqkNoiK39IlK1sgKYYyZCEUJHdCxMVSZCfHp1fM2vMHl5Vpos4M1OaTTHUqnMi9bwYOnQZDZD\", \"expires\": \"5128379\", \"id\": \"100002323554962\"}'),
	(11,11,'facebook','1183028056','{\"access_token\": \"AAAGl12e8jmoBAPn2KDB3g8Us9PWZB3LLATa4N2zPAABiWJOdRuXerTkEWIspfLSVD0jDCWcNnXYeLpxydoEJ15VFgMYwCeyc2k75tpAZDZD\", \"expires\": \"5183999\", \"id\": \"1183028056\"}'),
	(12,12,'facebook','100000635717443','{\"access_token\": \"AAAGl12e8jmoBAF86sovQyMWj7n2cqo0wU5QokGdZC6aYesWM1VSxjluiVbHx3XVchPYIZC9ycpLQPefMyVR0e5shU10aDZBhsF5SHTGIAZDZD\", \"expires\": \"5183999\", \"id\": \"100000635717443\"}');

/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table south_migrationhistory
# ------------------------------------------------------------

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;

INSERT INTO `south_migrationhistory` (`id`, `app_name`, `migration`, `applied`)
VALUES
	(1,'entry','0001_initial','2013-03-14 11:30:49');

/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_friendship
# ------------------------------------------------------------

LOCK TABLES `user_friendship` WRITE;
/*!40000 ALTER TABLE `user_friendship` DISABLE KEYS */;

INSERT INTO `user_friendship` (`id`, `creator_id`, `friend_id`, `created_at`)
VALUES
	(1,3,1,'2013-02-19 22:59:25'),
	(2,4,3,'2013-02-19 23:03:07'),
	(3,4,2,'2013-02-19 23:03:07'),
	(4,4,1,'2013-02-19 23:03:07'),
	(5,5,3,'2013-02-20 11:51:37'),
	(6,5,4,'2013-02-20 11:51:37'),
	(7,5,2,'2013-02-20 11:51:37'),
	(8,5,1,'2013-02-20 11:51:37'),
	(9,6,3,'2013-02-20 12:22:36'),
	(10,6,4,'2013-02-20 12:22:36'),
	(11,6,2,'2013-02-20 12:22:36'),
	(12,6,1,'2013-02-20 12:22:36'),
	(13,6,5,'2013-02-20 12:22:37'),
	(14,7,4,'2013-02-21 12:28:27'),
	(15,8,7,'2013-02-21 12:38:27'),
	(16,8,4,'2013-02-21 12:38:27'),
	(17,9,3,'2013-02-21 19:18:31'),
	(18,9,4,'2013-02-21 19:18:31'),
	(19,9,2,'2013-02-21 19:18:31'),
	(20,9,1,'2013-02-21 19:18:31'),
	(21,9,6,'2013-02-21 19:18:31'),
	(22,9,5,'2013-02-21 19:18:31'),
	(23,10,3,'2013-02-24 00:47:06'),
	(24,10,4,'2013-02-24 00:47:07'),
	(25,10,2,'2013-02-24 00:47:07'),
	(26,10,1,'2013-02-24 00:47:07'),
	(27,10,6,'2013-02-24 00:47:07'),
	(28,10,9,'2013-02-24 00:47:07'),
	(29,10,5,'2013-02-24 00:47:07'),
	(30,1,2,'0000-00-00 00:00:00'),
	(31,11,3,'2013-03-04 17:49:51'),
	(32,11,4,'2013-03-04 17:49:51'),
	(33,11,10,'2013-03-04 17:49:52'),
	(34,11,5,'2013-03-04 17:49:52'),
	(35,12,7,'2013-03-06 11:26:34'),
	(36,12,4,'2013-03-06 11:26:34'),
	(37,12,8,'2013-03-06 11:26:34');

/*!40000 ALTER TABLE `user_friendship` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_userclient
# ------------------------------------------------------------



# Dump of table user_userprofile
# ------------------------------------------------------------

LOCK TABLES `user_userprofile` WRITE;
/*!40000 ALTER TABLE `user_userprofile` DISABLE KEYS */;

INSERT INTO `user_userprofile` (`id`, `user_id`, `photo`, `bank_account`)
VALUES
	(1,1,'',''),
	(2,2,'',''),
	(3,3,'',''),
	(4,4,'',''),
	(5,5,'',''),
	(6,6,'',''),
	(7,7,'',''),
	(8,8,'',''),
	(9,9,'',''),
	(10,10,'',''),
	(11,11,'',''),
	(12,12,'','');

/*!40000 ALTER TABLE `user_userprofile` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
