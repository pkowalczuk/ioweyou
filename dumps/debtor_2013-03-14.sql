# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 37.59.111.138 (MySQL 5.5.28-1~dotdeb.0-log)
# Database: debtor
# Generation Time: 2013-03-14 20:24:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_group
# ------------------------------------------------------------



# Dump of table auth_group_permissions
# ------------------------------------------------------------



# Dump of table auth_permission
# ------------------------------------------------------------

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`)
VALUES
	(1,'Can add permission',1,'add_permission'),
	(2,'Can change permission',1,'change_permission'),
	(3,'Can delete permission',1,'delete_permission'),
	(4,'Can add group',2,'add_group'),
	(5,'Can change group',2,'change_group'),
	(6,'Can delete group',2,'delete_group'),
	(7,'Can add user',3,'add_user'),
	(8,'Can change user',3,'change_user'),
	(9,'Can delete user',3,'delete_user'),
	(10,'Can add content type',4,'add_contenttype'),
	(11,'Can change content type',4,'change_contenttype'),
	(12,'Can delete content type',4,'delete_contenttype'),
	(13,'Can add session',5,'add_session'),
	(14,'Can change session',5,'change_session'),
	(15,'Can delete session',5,'delete_session'),
	(16,'Can add site',6,'add_site'),
	(17,'Can change site',6,'change_site'),
	(18,'Can delete site',6,'delete_site'),
	(19,'Can add log entry',7,'add_logentry'),
	(20,'Can change log entry',7,'change_logentry'),
	(21,'Can delete log entry',7,'delete_logentry'),
	(22,'Can add user profile',8,'add_userprofile'),
	(23,'Can change user profile',8,'change_userprofile'),
	(24,'Can delete user profile',8,'delete_userprofile'),
	(25,'Can add friendship',9,'add_friendship'),
	(26,'Can change friendship',9,'change_friendship'),
	(27,'Can delete friendship',9,'delete_friendship'),
	(28,'Can add user client',10,'add_userclient'),
	(29,'Can change user client',10,'change_userclient'),
	(30,'Can delete user client',10,'delete_userclient'),
	(31,'Can add entry',11,'add_entry'),
	(32,'Can change entry',11,'change_entry'),
	(33,'Can delete entry',11,'delete_entry'),
	(34,'Can add user social auth',12,'add_usersocialauth'),
	(35,'Can change user social auth',12,'change_usersocialauth'),
	(36,'Can delete user social auth',12,'delete_usersocialauth'),
	(37,'Can add nonce',13,'add_nonce'),
	(38,'Can change nonce',13,'change_nonce'),
	(39,'Can delete nonce',13,'delete_nonce'),
	(40,'Can add association',14,'add_association'),
	(41,'Can change association',14,'change_association'),
	(42,'Can delete association',14,'delete_association'),
	(43,'Can add migration history',15,'add_migrationhistory'),
	(44,'Can change migration history',15,'change_migrationhistory'),
	(45,'Can delete migration history',15,'delete_migrationhistory');

/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user
# ------------------------------------------------------------

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`)
VALUES
	(1,'wqwlucky','Łukasz','Staniszewski','wqwlucky@gmail.com','!',0,1,0,'2013-03-05 08:19:08','2013-02-19 22:59:19'),
	(2,'maciej.kuprowski','Maciej','Kuprowsky','maciek220v@interia.pl','!',0,1,0,'2013-03-11 22:26:31','2013-02-19 22:59:21'),
	(3,'agata.adamska','Agata','Adamska','agata_a@o2.pl','!',0,1,0,'2013-03-10 00:33:53','2013-02-19 22:59:23'),
	(4,'piotr.kowalczuk','Piotr','Kowalczuk','imp4ever@gmail.com','!',0,1,0,'2013-03-14 11:53:52','2013-02-19 23:03:06'),
	(5,'tomasz.lis.50','Tomasz','Lis','listomek@gmail.com','!',0,1,0,'2013-02-20 11:51:38','2013-02-20 11:51:36'),
	(6,'arnold.leszczuk.90','Arnold','Leszczuk','tthitt@gmail.com','!',0,1,0,'2013-02-20 12:22:37','2013-02-20 12:22:35'),
	(7,'kamil.biela','Kamil','Biela','kamil.biela@gmail.com','!',0,1,0,'2013-02-21 12:28:27','2013-02-21 12:28:26'),
	(8,'daniel.waligora.902','Daniel','Waligóra','danielwaligora@gmail.com','!',0,1,0,'2013-03-12 11:48:19','2013-02-21 12:38:21'),
	(9,'piotr.breda','Piotr','Breda','miodowar@gmail.com','!',0,1,0,'2013-03-12 19:45:20','2013-02-21 19:18:30'),
	(10,'radoslaw.szynkaruk','Radosław','Szynkaruk','radoslaw.szynkaruk@gmail.com','!',0,1,0,'2013-02-24 16:14:00','2013-02-24 00:47:05'),
	(11,'adrian.smolarczyk','Adrian','Smolarczyk','adriansmolarczyk@gmail.com','!',0,1,0,'2013-03-04 17:49:52','2013-03-04 17:49:50'),
	(12,'qlaczek','Marek','Wilczyński','qlaczek@gmail.com','!',0,1,0,'2013-03-06 11:26:34','2013-03-06 11:26:33'),
	(13,'holowinski.marcin','Marcin','Hołowiński','dzidzixxx@gmail.com','!',0,1,0,'2013-03-13 18:06:42','2013-03-11 22:15:11');

/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user_groups
# ------------------------------------------------------------



# Dump of table auth_user_user_permissions
# ------------------------------------------------------------



# Dump of table deptor_entry
# ------------------------------------------------------------

LOCK TABLES `deptor_entry` WRITE;
/*!40000 ALTER TABLE `deptor_entry` DISABLE KEYS */;

INSERT INTO `deptor_entry` (`id`, `name`, `description`, `value`, `lender_id`, `deptor_id`, `accepted_at`, `created_at`, `updated_at`)
VALUES
	(3,'Różnica za pizze','niech cię gęś kopnie.',3.00,7,4,'2013-02-21 12:30:40','2013-02-21 12:30:40','2013-02-21 12:30:40'),
	(4,'Kasa za popitke u rudego','kupilem kole plus czipsy solone',10.00,3,4,'2013-02-21 12:34:41','2013-02-21 12:34:41','2013-02-21 12:34:41'),
	(5,'Hajs dziwko za żarcie!','Gdyby nie ja to byś zdechł z głodu także dawaj hajs dziwko!',10.00,8,4,'2013-02-21 12:40:39','2013-02-21 12:39:55','2013-02-21 12:40:39'),
	(6,'Parówki na hotdogi','2 bulki + parówki + musztarda',9.00,4,2,'2013-02-21 19:20:44','2013-02-21 19:16:56','2013-02-21 19:20:44'),
	(7,'2 harnasie','bez kaucji',4.78,4,9,'2013-02-21 21:49:39','2013-02-21 19:19:08','2013-02-21 21:49:39'),
	(9,'wóda w piątek','Ale jak to w piątek?',12.00,1,4,'2013-02-23 01:29:45','2013-02-22 23:46:53','2013-02-23 01:29:45'),
	(10,'Kadarka','Kadarka, chleb, makaron, przecier',19.73,9,4,'2013-02-23 11:50:13','2013-02-23 11:23:26','2013-02-23 11:50:13'),
	(11,'chleb tak bardzo ciemny','',3.50,2,9,'2013-02-23 11:54:11','2013-02-23 11:48:45','2013-02-23 11:54:11'),
	(12,'Alternative Szpringer','',13.50,9,2,'2013-02-23 14:20:57','2013-02-23 14:20:09','2013-02-23 14:20:57'),
	(13,'wódka i energetyki','wódka i energetyki w sobote',20.43,1,4,'2013-02-24 12:31:11','2013-02-23 16:23:30','2013-02-24 12:31:11'),
	(14,'nocna buła i zupa','Sobota, sklep 24h przy Bema',7.80,2,4,'2013-02-24 12:31:12','2013-02-24 10:20:39','2013-02-24 12:31:12'),
	(15,'Mirinda','',2.19,9,4,'2013-02-24 14:14:53','2013-02-24 14:08:02','2013-02-24 14:14:53'),
	(16,'7up','',2.19,9,1,'2013-02-24 20:43:55','2013-02-24 14:08:25','2013-02-24 20:43:55'),
	(17,'zakupy','żywiec, fanta i pizza ',16.18,1,4,'2013-02-25 14:36:19','2013-02-24 20:45:35','2013-02-25 14:36:19'),
	(18,'El. metalowe/alkohol/owoce','el. metalowe/browar/jabłka',13.00,2,9,'2013-03-01 10:15:30','2013-02-26 17:55:43','2013-03-01 10:15:30'),
	(20,'Alkohol + chleb','Chleb żytni & Harna$',5.70,2,9,'2013-03-01 10:17:31','2013-02-26 20:36:56','2013-03-01 10:17:31'),
	(21,'kiełbaski','Dawaj siano',3.01,4,8,'2013-03-04 09:05:21','2013-02-27 18:53:30','2013-03-04 09:05:21'),
	(22,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,1,'2013-03-04 12:01:41','2013-03-01 09:55:50','2013-03-04 12:01:41'),
	(23,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,2,'2013-03-01 11:21:41','2013-03-01 09:56:08','2013-03-01 11:21:41'),
	(24,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,3,'2013-03-02 22:20:59','2013-03-01 09:56:25','2013-03-02 22:20:59'),
	(25,'Zakupy Ikea','2 szczotki i 4 szklanki',3.03,4,9,'2013-03-01 10:15:35','2013-03-01 09:56:55','2013-03-01 10:15:35'),
	(26,'Stafy na mieszkanie','1/2 * Prześcieradło, miska, krzesło obrotowe,  pudełko.',171.96,4,3,'2013-03-02 22:20:57','2013-03-01 09:59:56','2013-03-02 22:20:57'),
	(27,'Kaucja','jw',380.00,4,1,'2013-03-04 12:01:45','2013-03-01 10:00:22','2013-03-04 12:01:45'),
	(28,'Kaucja','jw',380.00,4,2,'2013-03-03 11:53:54','2013-03-01 10:01:20','2013-03-03 11:53:54'),
	(29,'Kaucja','jw',380.00,4,3,'2013-03-02 22:20:56','2013-03-01 10:01:35','2013-03-02 22:20:56'),
	(30,'Ipad + ram','jw',1827.53,9,4,'2013-03-01 10:02:29','2013-03-01 10:02:29','2013-03-01 10:02:29'),
	(31,'Kaucja','jw',380.00,4,9,'2013-03-01 10:15:36','2013-03-01 10:02:54','2013-03-01 10:15:36'),
	(32,'Internet za luty','',22.00,9,4,'2013-03-01 14:11:29','2013-03-01 10:16:35','2013-03-01 14:11:29'),
	(33,'Internet za luty','',22.00,9,1,'2013-03-04 12:01:55','2013-03-01 10:17:02','2013-03-04 12:01:55'),
	(35,'LeGyros','Za gyros zaplacilismy 49.5. 3x 16zl + przewoz. Daleś mi 14zł.',2.50,4,2,'2013-03-02 22:01:18','2013-03-02 20:09:11','2013-03-02 22:01:18'),
	(36,'Zakupy z leclerka','Zakupy, głownie chemia.',44.49,4,1,'2013-03-04 12:02:30','2013-03-02 21:48:26','2013-03-04 12:02:30'),
	(37,'Zakupy z leclerka','Głównie chemia.',44.49,4,2,'2013-03-02 22:01:17','2013-03-02 21:48:45','2013-03-02 22:01:17'),
	(38,'Zakupy z leclerka','Głównie chemia.',44.49,4,3,'2013-03-02 22:20:55','2013-03-02 21:49:06','2013-03-02 22:20:55'),
	(39,'Zakupy z leclerka','Głównie chemia.',44.49,4,9,'2013-03-02 22:00:59','2013-03-02 21:49:26','2013-03-02 22:00:59'),
	(40,'Patyczki do uszu','',1.87,4,1,'2013-03-04 12:01:59','2013-03-02 21:51:33','2013-03-04 12:01:59'),
	(41,'Wieszaki','',12.76,4,9,'2013-03-02 22:01:07','2013-03-02 21:52:24','2013-03-02 22:01:07'),
	(42,'Wóda na parapetówę','3xkrupnik + popita',19.00,4,1,'2013-03-04 12:02:37','2013-03-02 22:18:41','2013-03-04 12:02:37'),
	(43,'Wóda na parapetówę','3 x krupnik + popita',19.00,4,2,'2013-03-03 11:53:51','2013-03-02 22:19:04','2013-03-03 11:53:51'),
	(44,'Wóda na parapetówę','3 x krupnik i popita.',19.00,4,9,'2013-03-05 05:53:53','2013-03-02 22:19:32','2013-03-05 05:53:53'),
	(45,'Poranne zakupy','bułki 1.20\r\nparówki 3.70\r\n1/3 jednorazowki 0.03',4.93,4,1,'2013-03-04 12:02:03','2013-03-03 11:44:31','2013-03-04 12:02:03'),
	(46,'Mydło w płynie','',1.41,4,1,'2013-03-04 12:02:20','2013-03-03 11:44:58','2013-03-04 12:02:20'),
	(47,'Mydło w płynie','',1.41,4,2,'2013-03-03 11:53:50','2013-03-03 11:45:09','2013-03-03 11:53:50'),
	(48,'Mydło w płynie','',1.41,4,3,'2013-03-04 12:13:06','2013-03-03 11:45:22','2013-03-04 12:13:06'),
	(49,'Mydło w płynie','',1.41,4,9,'2013-03-05 05:54:01','2013-03-03 11:45:32','2013-03-05 05:54:01'),
	(50,'Legyros','zestaw tortilia extra',13.50,4,7,'2013-03-04 11:40:01','2013-03-04 11:38:59','2013-03-04 11:40:01'),
	(51,'Sałatka Turecka','dziwne ze nie sałatka zydowska',8.50,4,8,'2013-03-08 13:48:09','2013-03-04 11:39:28','2013-03-08 13:48:09'),
	(54,'Dzika noc','',9999.99,5,11,'2013-03-04 17:51:33','2013-03-04 17:51:33','2013-03-04 17:51:33'),
	(55,'Piwo (parapetowka) ','',2.39,9,4,'2013-03-05 07:16:57','2013-03-05 05:55:14','2013-03-05 07:16:57'),
	(56,'Piwo (parapetowka) ','',2.39,9,2,'2013-03-07 11:14:37','2013-03-05 05:55:41','2013-03-07 11:14:37'),
	(57,'Zakupy Ikea','Deska + suszarki',15.99,1,2,'2013-03-07 11:15:32','2013-03-05 08:10:05','2013-03-07 11:15:32'),
	(58,'Zakupy Ikea','Deska + suszarki',15.99,1,3,'2013-03-05 14:34:50','2013-03-05 08:10:05','2013-03-05 14:34:50'),
	(59,'Zakupy Ikea','Deska + suszarki',15.99,1,4,'2013-03-05 11:03:42','2013-03-05 08:10:05','2013-03-05 11:03:42'),
	(60,'Zakupy Ikea','Deska + suszarki',15.99,1,9,'2013-03-05 09:35:30','2013-03-05 08:10:05','2013-03-05 09:35:30'),
	(61,'bezkorkowa tuba','Żubry',4.80,1,2,'2013-03-07 11:15:23','2013-03-05 08:10:43','2013-03-07 11:15:23'),
	(62,'bezkorkowa tuba','Żubry',4.80,1,9,'2013-03-05 08:50:57','2013-03-05 08:11:02','2013-03-05 08:50:57'),
	(63,'bezkorkowa tuba','Żubr',2.40,1,4,'2013-03-05 11:03:27','2013-03-05 08:11:21','2013-03-05 11:03:27'),
	(64,'bezkorkowa tuba','Żubr',2.40,1,3,'2013-03-05 14:34:49','2013-03-05 08:11:38','2013-03-05 14:34:49'),
	(65,'Przelew za kaucje','doszedł.',380.00,2,4,'2013-03-05 15:59:57','2013-03-05 15:59:57','2013-03-05 15:59:57'),
	(66,'przelew za kaucję','',380.00,3,4,'2013-03-05 16:13:43','2013-03-05 16:08:29','2013-03-05 16:13:43'),
	(67,'Dług z arkusza kalkulacyjnego','',540.73,9,1,'2013-03-05 19:28:53','2013-03-05 19:18:41','2013-03-05 19:28:53'),
	(68,'Piwo tuba','Piwo Lech',2.88,2,1,'2013-03-07 11:41:10','2013-03-07 11:15:14','2013-03-07 11:41:10'),
	(69,'Śniadanie bułki','2xbułka duża',0.90,2,1,'2013-03-07 11:41:11','2013-03-07 11:16:06','2013-03-07 11:41:11'),
	(70,'Śniadanie czwartek','3xkajzerka+szynka chłopska',5.33,2,1,'2013-03-07 11:41:11','2013-03-07 11:16:43','2013-03-07 11:41:11'),
	(71,'spłata ioweyou.pl via przelew','Money transfer',11.68,2,1,'2013-03-11 13:55:39','2013-03-07 13:48:43','2013-03-11 13:55:39'),
	(72,'spłata ioweyou.pl via przelew','50pln via bank transfer',50.00,2,4,'2013-03-11 16:01:06','2013-03-07 13:54:43','2013-03-11 16:01:06'),
	(73,'Stare długi','kaucja + zakupy + pierdoły - pierdoły + odsetki',617.99,4,1,'2013-03-07 15:21:22','2013-03-07 15:08:30','2013-03-07 15:21:22'),
	(74,'zwrot za bezkorkowa tube i ikea','',20.00,3,1,'2013-03-09 12:53:02','2013-03-07 18:08:29','2013-03-09 12:53:02'),
	(75,'Piwo z parapetowki i chleb','Cleb oraz chleb orkiszowy 2.39 + 3.99',6.38,9,3,'2013-03-08 13:21:26','2013-03-07 18:15:09','2013-03-08 13:21:26'),
	(76,'Piwo Tyskie','Czwartkowe piwo szt. 1',2.55,2,4,'2013-03-08 12:30:20','2013-03-08 12:29:16','2013-03-08 12:30:20'),
	(77,'Frytki śniadanie','',3.50,2,1,'2013-03-09 12:52:49','2013-03-08 12:30:13','2013-03-09 12:52:49'),
	(78,'AlePizza','SZYBKO SIANO BOM SPŁUKANY!',15.00,4,1,'2013-03-09 12:52:50','2013-03-08 17:13:30','2013-03-09 12:52:50'),
	(79,'AlePizza','SZYBKO SIANO BOM SPŁUKANY!',15.00,4,2,'2013-03-11 22:23:30','2013-03-08 17:13:30','2013-03-11 22:23:30'),
	(80,'AlePizza','SZYBKO SIANO BOM SPŁUKANY!',15.00,4,3,'2013-03-10 00:34:09','2013-03-08 17:13:30','2013-03-10 00:34:09'),
	(81,'zwrot','',40.00,3,4,'2013-03-11 16:00:53','2013-03-10 18:40:47','2013-03-11 16:00:53'),
	(82,'Zwrot za kaucje','przelew razem z czynszem',380.00,1,4,'2013-03-12 12:43:53','2013-03-11 13:56:16','2013-03-12 12:43:53'),
	(83,'Internet za marzec','Opłata za okres od 25.02 do 24.03',17.50,9,1,'2013-03-12 13:43:38','2013-03-11 17:25:52','2013-03-12 13:43:38'),
	(84,'Internet za marzec','Opłata za okres od 25.02 do 24.03',17.50,9,2,'2013-03-12 17:21:49','2013-03-11 17:26:15','2013-03-12 17:21:49'),
	(85,'Internet za marzec','Opłata za okres od 25.02 do 24.03',17.50,9,3,'2013-03-13 20:50:33','2013-03-11 17:26:32','2013-03-13 20:50:33'),
	(86,'Internet za marzec','Opłata za okres od 25.02 do 24.03',17.50,9,4,'2013-03-11 21:57:06','2013-03-11 17:26:46','2013-03-11 21:57:06'),
	(87,'Źle naliczona opłata za internet','',22.00,1,9,'2013-03-11 17:27:20','2013-03-11 17:27:20','2013-03-11 17:27:20'),
	(88,'Źle naliczona opłata za internet','',22.00,4,9,'2013-03-11 17:27:44','2013-03-11 17:27:44','2013-03-11 17:27:44'),
	(101,'setka','flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)',19.50,4,1,'2013-03-12 13:49:16','2013-03-12 01:06:30','2013-03-12 13:49:16'),
	(102,'setka','flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)',19.50,4,2,'2013-03-12 17:21:48','2013-03-12 01:06:30','2013-03-12 17:21:48'),
	(103,'setka','flaszka + kolejka 4x 0,40ml (16zł ) + 4x popita  (12zł)',19.50,4,13,'2013-03-13 18:11:34','2013-03-12 01:06:30','2013-03-13 18:11:34'),
	(104,'przelew','1,51 doliczone do przelewu za gyros (tutaj nie uwzgledniony)',1.51,4,8,'2013-03-12 11:51:17','2013-03-12 11:51:17','2013-03-12 11:51:17'),
	(105,'poprawka','',3.02,8,4,'2013-03-12 12:41:04','2013-03-12 11:53:23','2013-03-12 12:41:04'),
	(106,'Gyros drobiowy','!',10.50,4,8,'2013-03-12 12:45:14','2013-03-12 12:41:44','2013-03-12 12:45:14'),
	(107,'Setka','Warka - 3\r\nSetka 20,5',23.50,1,4,'2013-03-12 14:54:41','2013-03-12 13:45:47','2013-03-12 14:54:41'),
	(108,'Setka','Setka 20,5\r\nŻywiec 3.8\r\nŻywiec 3.5',27.80,1,2,'2013-03-12 17:21:42','2013-03-12 13:47:19','2013-03-12 17:21:42'),
	(109,'Setka','Butelka wódy ;d',12.50,1,13,'2013-03-13 18:11:33','2013-03-12 13:48:00','2013-03-13 18:11:33'),
	(110,'Piwo i mars','Warka 3.5\r\nMars 1.69',5.19,1,3,'2013-03-13 20:50:32','2013-03-12 13:48:44','2013-03-13 20:50:32'),
	(111,'Setka','Setka',1.00,2,1,'2013-03-12 17:24:20','2013-03-12 17:22:52','2013-03-12 17:24:20'),
	(112,'Setka','Setka',1.00,2,4,'2013-03-13 23:46:54','2013-03-12 17:22:52','2013-03-13 23:46:54'),
	(113,'Setka','Setka',1.00,2,13,'2013-03-13 18:11:31','2013-03-12 17:22:52','2013-03-13 18:11:31'),
	(114,'setka','setka bo nie ogarniam',3.00,2,1,'2013-03-12 17:24:21','2013-03-12 17:23:53','2013-03-12 17:24:21'),
	(115,'setka','setka bo nie ogarniam',3.00,2,4,'2013-03-13 23:46:48','2013-03-12 17:23:53','2013-03-13 23:46:48'),
	(116,'setka','setka bo nie ogarniam',3.00,2,13,'2013-03-13 18:11:29','2013-03-12 17:23:53','2013-03-13 18:11:29'),
	(117,'Spłata częściowa','',1000.00,4,9,'2013-03-12 19:45:55','2013-03-12 19:45:55','2013-03-12 19:45:55'),
	(118,'setka','',4.00,13,1,'2013-03-13 22:20:21','2013-03-13 18:09:59','2013-03-13 22:20:21'),
	(119,'setka','',4.00,13,2,NULL,'2013-03-13 18:10:38','2013-03-13 18:10:38'),
	(120,'setka','',4.00,13,4,'2013-03-13 23:45:53','2013-03-13 18:10:58','2013-03-13 23:45:53'),
	(121,'stafy na obiad i chleb','',10.00,3,4,'2013-03-13 23:46:45','2013-03-13 20:52:44','2013-03-13 23:46:45'),
	(122,'dentysta','',100.00,1,3,'2013-03-13 20:56:24','2013-03-13 20:56:24','2013-03-13 20:56:24'),
	(123,'przelew - dentysta i reszta','',103.58,3,1,NULL,'2013-03-13 20:58:04','2013-03-13 20:58:04');

/*!40000 ALTER TABLE `deptor_entry` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deptor_friendship
# ------------------------------------------------------------

LOCK TABLES `deptor_friendship` WRITE;
/*!40000 ALTER TABLE `deptor_friendship` DISABLE KEYS */;

INSERT INTO `deptor_friendship` (`id`, `creator_id`, `friend_id`, `created_at`)
VALUES
	(1,3,1,'2013-02-19 22:59:25'),
	(2,4,3,'2013-02-19 23:03:07'),
	(3,4,2,'2013-02-19 23:03:07'),
	(4,4,1,'2013-02-19 23:03:07'),
	(5,5,3,'2013-02-20 11:51:37'),
	(6,5,4,'2013-02-20 11:51:37'),
	(7,5,2,'2013-02-20 11:51:37'),
	(8,5,1,'2013-02-20 11:51:37'),
	(9,6,3,'2013-02-20 12:22:36'),
	(10,6,4,'2013-02-20 12:22:36'),
	(11,6,2,'2013-02-20 12:22:36'),
	(12,6,1,'2013-02-20 12:22:36'),
	(13,6,5,'2013-02-20 12:22:37'),
	(14,7,4,'2013-02-21 12:28:27'),
	(15,8,7,'2013-02-21 12:38:27'),
	(16,8,4,'2013-02-21 12:38:27'),
	(17,9,3,'2013-02-21 19:18:31'),
	(18,9,4,'2013-02-21 19:18:31'),
	(19,9,2,'2013-02-21 19:18:31'),
	(20,9,1,'2013-02-21 19:18:31'),
	(21,9,6,'2013-02-21 19:18:31'),
	(22,9,5,'2013-02-21 19:18:31'),
	(23,10,3,'2013-02-24 00:47:06'),
	(24,10,4,'2013-02-24 00:47:07'),
	(25,10,2,'2013-02-24 00:47:07'),
	(26,10,1,'2013-02-24 00:47:07'),
	(27,10,6,'2013-02-24 00:47:07'),
	(28,10,9,'2013-02-24 00:47:07'),
	(29,10,5,'2013-02-24 00:47:07'),
	(30,1,2,'0000-00-00 00:00:00'),
	(31,11,3,'2013-03-04 17:49:51'),
	(32,11,4,'2013-03-04 17:49:51'),
	(33,11,10,'2013-03-04 17:49:52'),
	(34,11,5,'2013-03-04 17:49:52'),
	(35,12,7,'2013-03-06 11:26:34'),
	(36,12,4,'2013-03-06 11:26:34'),
	(37,12,8,'2013-03-06 11:26:34'),
	(38,13,3,'2013-03-11 22:15:12'),
	(39,13,4,'2013-03-11 22:15:12'),
	(40,13,2,'2013-03-11 22:15:12'),
	(41,13,1,'2013-03-11 22:15:12'),
	(42,13,6,'2013-03-11 22:15:12'),
	(43,13,9,'2013-03-11 22:15:12'),
	(44,13,5,'2013-03-11 22:15:12');

/*!40000 ALTER TABLE `deptor_friendship` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deptor_userclient
# ------------------------------------------------------------



# Dump of table deptor_userprofile
# ------------------------------------------------------------

LOCK TABLES `deptor_userprofile` WRITE;
/*!40000 ALTER TABLE `deptor_userprofile` DISABLE KEYS */;

INSERT INTO `deptor_userprofile` (`id`, `user_id`, `photo`, `bank_account`)
VALUES
	(1,1,'',''),
	(2,2,'',''),
	(3,3,'',''),
	(4,4,'',''),
	(5,5,'',''),
	(6,6,'',''),
	(7,7,'',''),
	(8,8,'',''),
	(9,9,'',''),
	(10,10,'',''),
	(11,11,'',''),
	(12,12,'',''),
	(13,13,'','');

/*!40000 ALTER TABLE `deptor_userprofile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_admin_log
# ------------------------------------------------------------



# Dump of table django_content_type
# ------------------------------------------------------------

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`)
VALUES
	(1,'permission','auth','permission'),
	(2,'group','auth','group'),
	(3,'user','auth','user'),
	(4,'content type','contenttypes','contenttype'),
	(5,'session','sessions','session'),
	(6,'site','sites','site'),
	(7,'log entry','admin','logentry'),
	(8,'user profile','deptor','userprofile'),
	(9,'friendship','deptor','friendship'),
	(10,'user client','deptor','userclient'),
	(11,'entry','deptor','entry'),
	(12,'user social auth','social_auth','usersocialauth'),
	(13,'nonce','social_auth','nonce'),
	(14,'association','social_auth','association'),
	(15,'migration history','south','migrationhistory');

/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_session
# ------------------------------------------------------------

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`)
VALUES
	('09bbbcb99876c12759dfe1dd8a5b1ab1','ZGMxM2FlMjVlNGJmZWU5M2Q1ZDVlM2QxYzc5OWQyODNiOWY2NzEwYjqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIGJEd2hkZERpZUNYYXZTNlhEZ1ZnVFp6SjBEdnZHRlhncQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUFCxohDwN7Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQxVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMVQhmYWNlYm9va3ENdS4=\n','2013-05-05 11:26:33'),
	('13d3106d65d82ded99d0d9af46c43ba7','ZmI2YWRlMmEwYzkyMjg5NTUxZTcyMmEzNzhkMjE5MTZmOTE4MTY2YjqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIGRpUkxjeU1YS2dkUzhORE5QZ1V5M2h4YUpaaENBNEpIcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUKFg8ICOz/Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQ1VHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-10 22:15:08'),
	('15072b4c6217a8d77ee4d259d1b5404c','ZjlhODkzOTBkZWFlMGEyNGI3NzhjODllMDc2ZGEyMTE5MDgxMDQxMzqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBCVUOZmFjZWJvb2tfc3RhdGVxA1UgVnVuT1YwOHVsaG5VcDQ1TmxSNjA3Q0s4REp0\nTEJUbHlxBFUJZnJpZW5kX2lkcQVYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QQZDgcPAydVY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-04-25 14:07:15'),
	('1629ad72385f6f991f06517a452bf557','YTljOWJlOGRjZWZmY2YxMGZjYjIyODMxYzYzMDk5MGViZGFmMjEyNzqAAn1xAS4=\n','2013-03-06 11:51:51'),
	('17cd1c028821239f70eec6f265b576a2','YTljOWJlOGRjZWZmY2YxMGZjYjIyODMxYzYzMDk5MGViZGFmMjEyNzqAAn1xAS4=\n','2013-03-19 11:50:42'),
	('19abc5a94ad84dcf34d57efe4227f13c','OTg1MTMyYTNmNmJiZDM0ODEwY2IzZmY0YmJkMWQxMzllNjllMTdjYzqAAn1xAVUOZmFjZWJvb2tf\nc3RhdGVxAlUgejhxbWNrMXhTRlRIcW5jRUhLcFRPNGMyNmFoTzdKS1lxA3Mu\n','2013-03-06 06:41:09'),
	('1b015ba3eeb4ce62f046d8b4ed3c4c92','YzViODk0ZGIzOWFmYTg1NzVmMDFmZTkwODEyNjIxMDE1MWY0NDRlODqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIHNoeE9LM1JySmdqVHBNTHZyMXpwRkh1UFhGcXM5VTJCcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQWEjAsC6YoY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQJVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-22 18:48:44'),
	('1df9300dab98d5670287fe2edc74aaa0','NDI5Yzk3OTI0MjBmYWQ0NmM0NGRlNWMzMmEyODhlZDQ5Nzg4ZjFjNTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIE1QOVdySm54VzVHaFQyZnZYeTFydFRaelN6NnlzS0ZkcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUKFTgxC1taY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQRVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-10 21:56:49'),
	('1ffc55a575d667ccbe6bcf7fd12007d1','NGQzNTlmZWY4ZTE0MTA3YzhjMzZhZWQ0MTEzNDI4ZjAzNDY0ODg3NTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIEVoQ1JBVnlkOXNETnJ6d2xMa3ZxeTM5cEdVck0yNFVWcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUMEgYoDuaLY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQ1VHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-12 18:06:40'),
	('207c8ef912b95c3d08c0eed3756dfd96','ZWU1NzEzYjYxYTE0Yzg5NTcyZmIxNTZlMTdmODcwODk2N2YyY2EyMzqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBA1UOZmFjZWJvb2tfc3RhdGVxA1UgQjNRaGN4OFhOY01wVExPZDFiQ2E0WTQ4cngx\nWGZ6NlJxBFUJZnJpZW5kX2lkcQVYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QUGEDYvDntbY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-05-06 16:54:47'),
	('326fb815ba4df7257b2881620aaa267a','YWQ5Y2FhN2E4Mzg4MWU1ZGQwMjMxOTcyOGFkMzg5NGFjZjc1NTNjZjqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIEdaVHN0N21NaGVmeklGVzNhTVB6YVNUdTExZ1VuVzh1cQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUDDjYoCzBfY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQFVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-03 14:54:40'),
	('34619001b851318c123c6e9b9a2ff6d3','ZTEwYzMxNzNhMDNmMGU2NWY5OTlmNGE3YTM3NWM0MWNiYjQ2ZDU5NDqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIHUyaDZkVU84N3lPWU5vMTBvOThKVDhncFdkSzFaU2lZcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUGCw4OBZJxY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQJVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-06 11:14:14'),
	('3651c7f0198ead54057d65669d5730de','OTViOTRjNjhlMTY2NjI1NGQ2YjdiMjdkYTczY2M0MzU5Y2UyYmNmYjqAAn1xAVUOZmFjZWJvb2tf\nc3RhdGVxAlUgd2U4eXRIS0l1U2tiYXhGNFpJV3UxVFFKNmp2cXlyZ2FxA3Mu\n','2013-03-17 11:55:58'),
	('36dfabd750193b392f5328d14afa1798','MDE2MmVkNDZiYjNlNDczY2JjNjMwYTYwNzIxZjJjYTFkMmQ2Y2U1MTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIENGOWY4Wm9CNjJ4Rkt0aXd4bHRrSHpya1hvSFRrQ0RIcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QULCzARDwogY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQhVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-11 11:48:17'),
	('38df0e9d630c9445da3b4a5d07178daa','MzhiOWVjMGVkMGQxZTVhMWM0YmRmMWVjNjc4MTJhZDkwZjM3YTI0ZTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIFJQMGFlNm02U1RmZUVGM080NmVRdzZ4dzJiQTRXUGlXcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQVDBYjC60tY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQZVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMVQhmYWNlYm9va3ENdS4=\n','2013-04-21 12:22:35'),
	('4d70fc7001a51d76c144894690bc888c','ZTgxODc4MTc1OTc2YWM0MmY5NTMzMDcxZjA5NzBkMDViMzg2OGQ4OTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIEowTGgyTGtUMVpqV1kzc1pkanY0RWFEaGxYR1ZNdWRNcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUGEDYqBBc9Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQNVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-06 16:54:42'),
	('4e8025f8cc619270a19da43e37dcc1e0','MTYzMDY5NzNmYjJkYWQwYmMxM2Y3ZGZkNmQxZTgyYTJkOWFlMWEyZDqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIFQ4RkZpRVVEeVljTmV2TVlJTGxTSklyUXViekFEYWtCcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQZADAiBUi1Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQRVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-25 00:48:34'),
	('512afd659359cf03087191a7543b33f5','NzYyN2JjMDU3NzQ2Y2NjN2Y1YmNiMTc0Zjg2YTNlZjI5YTFkZTZhZTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIHlKUEh4d28ybmdYdm1LS2RRWERJRU5vcTB0RjVkQno4cQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUKFhoeDcueY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQJVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-10 22:26:30'),
	('56ccf5b11624a64030f6df65da22db40','YTljOWJlOGRjZWZmY2YxMGZjYjIyODMxYzYzMDk5MGViZGFmMjEyNzqAAn1xAS4=\n','2013-03-05 23:30:19'),
	('5750081ec6f894d26114e1caa564bee6','ZWQ1YmNkMjM5MDYyOTkxMDljYTAxNzU3Nzg4MDAyNGNkOTVjZmI5MTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIFc5QjI2bXZWTnJtbTg1TFNEUU83Mm0xQTJ5b2k4ZEhscQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQYCy8VCUfSY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQJVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-24 11:47:21'),
	('5951e0c6b8b5f241b7167b252ec0f40f','Nzg3OTcyYTU4MDRiNTk0NzM5MDI0ZjY4OWM0ZDY5YTM1MDFmYzIwNTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIDdaOXJSeHhPUGZzYkZxSVM3YzYwRnJsZGptN0pqUjFycQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQUEx4wA4I4Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQNVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMVQhmYWNlYm9va3ENdS4=\n','2013-04-20 19:30:48'),
	('5dbc70ae04c4cf41e5d6880eff6c3ea9','YTMwMmE1NzFjMzNjN2ZkM2FmMWRjYWI2YTc4NTdhNWI1ZWFmNjk2YzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIHI0WDVTOFFROXNmYlR6VnhHWmI1RU5DejFMTXRrem94cQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUDETEzDlQ+Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQtVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMVQhmYWNlYm9va3ENdS4=\n','2013-05-03 17:49:51'),
	('72f571ee5bf44e1e8392738ec741db76','MjI4NTc3ZDAyMzY2NThjNDk2MzA3N2ViNDMyMzAzNDExNGRjODI2MzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIFE3Z1RSZFp0bmk1dDR1Q1VjTFRiZzBvRGxGdWJaSWZUcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUKFg8KAGXDY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQ1VHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMVQhmYWNlYm9va3ENdS4=\n','2013-05-10 22:15:10'),
	('74ce8a51589fae24d758dad3bc448569','MjBlZDg5NDgzMmVhZmQxNGM4Y2QwNDRhYzBlMTRjNDE0N2EwZjkyNzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIGdKTEdDaXJkZkExMXZ1dEVnTXl0Uk9pM0xWR1l2T0ZFcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUFCCUvAxLOY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQNVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-05 08:37:47'),
	('83ea8cf2d53ccf15b5712d2e8b30c195','ZDM1MmUxNjVhYWRkNTI1ZmE2NTM4NDQ5OTI2NGUzZjEwZjk3ODk3MTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBA1UOZmFjZWJvb2tfc3RhdGVxA1Ugc0xnWUNwZUpzYThTRHUyUk9KOFhuamFKT3FK\ndDYyVW5xBFUJZnJpZW5kX2lkcQVYAQAAADFVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QUJACEzCWEoY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-05-09 00:33:51'),
	('8721c260becadcaff2e712470c720192','YTBmNjlmNDJmZDkxN2UxYjFjYmEzN2NhMDc2NDVhOTU3NzQ4NTYxYTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBAVUOZmFjZWJvb2tfc3RhdGVxA1UgR2J3ZVBSM3BsMWdyQ3FYNlptRVcwNlAwcXpl\nYWFlUkVxBFUSX2F1dGhfdXNlcl9iYWNrZW5kcQVVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vi\nb29rLkZhY2Vib29rQmFja2VuZHEGVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxB1gI\nAAAAZmFjZWJvb2txCFUPX3Nlc3Npb25fZXhwaXJ5cQljZGF0ZXRpbWUKZGF0ZXRpbWUKcQpVCgfd\nBQMONicOAHFjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxCylScQyGUnENdS4=\n','2013-05-03 14:54:39'),
	('879de69ec1fc56154f4df00915ab6c45','MDMyZDIzZmQxYmRmNjZiM2JkMTNmYzI2OTM0NDZiY2QxZWEyYzAyYTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBCVUOZmFjZWJvb2tfc3RhdGVxA1UgajIzV1RnWUN1QTM5ZndZbFdUYUNEaE0zNFdO\ncDJSQjdxBFUSX2F1dGhfdXNlcl9iYWNrZW5kcQVVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vi\nb29rLkZhY2Vib29rQmFja2VuZHEGVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxB1gI\nAAAAZmFjZWJvb2txCFUPX3Nlc3Npb25fZXhwaXJ5cQljZGF0ZXRpbWUKZGF0ZXRpbWUKcQpVCgfd\nBQYSDQYBpgdjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxCylScQyGUnENdS4=\n','2013-05-06 18:13:06'),
	('8c463c67c58fde42e0acc8d3b4d4b02e','MGE0MDczYzUxODAwZTc3YWIxODA2NzAxMmE1OGU5NWRmNTM4ODI0MTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBAlUOZmFjZWJvb2tfc3RhdGVxA1UgZTh2TDlwOUVmdUVGZnN3eng5R2gybUxXNGg0\nZFFxRFFxBFUSX2F1dGhfdXNlcl9iYWNrZW5kcQVVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vi\nb29rLkZhY2Vib29rQmFja2VuZHEGVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxB1gI\nAAAAZmFjZWJvb2txCFUPX3Nlc3Npb25fZXhwaXJ5cQljZGF0ZXRpbWUKZGF0ZXRpbWUKcQpVCgfd\nBQYLDg4MGzljZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxCylScQyGUnENdS4=\n','2013-05-06 11:14:14'),
	('8f8927250241aeceea8cf2d26d08017d','YzYxNjMxYWU3ZDEyZDZiODZmYTRkZThiZTg1NmVlNWVkYzFmY2Y1MzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIGVrMW1mMzZ6NktUeWxvU2ZtWU5CdXk4SE9wRTM4TFg5cQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQZFCskBiUJY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQFVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-25 20:43:36'),
	('99032790903b7aaa7ad02df9e88aee38','NzkyNWM4OWRmOWYzNWJhOWFlYzg3ZWY4MzhmMzc1YjA4MjRiNTExNDqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIG41ZmlMY3NpNE9wR3RTVnhyZUdYVHZNOG5MQlhwanhicQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQWDCYbA3qpY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQhVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMVQhmYWNlYm9va3ENdS4=\n','2013-04-22 12:38:27'),
	('9958b54fe341075039cdd70136290c5d','NWZkMzI5NjU3NDM3ZThhYzI5ZjM0MWRkMWZhNDFhZTBhYTcwMjI5MTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBAVUOZmFjZWJvb2tfc3RhdGVxA1UgeGVxV21NMjhGTW50UHpqcnAzVEJxVmZ2WWxZ\nUW9lM29xBFUJZnJpZW5kX2lkcQVYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QQZFCshBpp6Y2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-04-25 20:43:33'),
	('9ad536b71b753e7387dd445fed06bb1e','MDFhZjk2ZDljMGRmMjdlYzE0OTk1OGMwZTU1YjQ3MDBlOWMyYjZhOTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIGlRSHZGWnRlMUJTeGVCYWRLcHFxM2Q0ME1rNEl0UEdzcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUNCzUzDh0VY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQRVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-13 11:53:51'),
	('a275f5e2bbc6dc1c4a52ebe9703388e2','MzExNjUwZGJkZjAyYzM2ODY2M2VlOGIwMGUwZmUxOTExNDkwNmM3YjqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIHc4RVRCUmtJTFdzczN6cmZHWThDRnBOd1NoeWV4QnFqcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQUEi8IBrrMY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQRVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-20 18:47:08'),
	('aae6b86b67ed957bf1511dd958d0fca7','M2Y1YmFhYTlmZWM1NjM5NDkwYTQ4MThhZmVkZmZjMWRmNDQ3YjVlNTqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIDFzVFVhbmFRTVFjZ0lNZU1DV21RSGdabmhOSGlQWVlscQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUGEg0HDtOjY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQlVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-06 18:13:07'),
	('acb83243763ce643a3cb7d88968ab3bb','NDAwOWI4ZTI0YzU2ODEyZWUxMjk0MjU5NDU1YWExZGQ2NWFhODIxNDqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIFY1cFYyQ2ludkUxMHZRSXpKckIwNk1YdGFvRkxvOWRScQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQZAC47DcZ/Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQpVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-25 00:46:59'),
	('d39df611b12b99a7ff65196f19ae33df','YzZkYTA3ZDE3NTg1MjQ0MGU3ZjE5N2VkNTc3NzRhMmI5ODRlNTJmYTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBAVUOZmFjZWJvb2tfc3RhdGVxA1UgMUhSQzJPRWMzRXhlMDdGaW9EeEFjbVgwMkYw\nOFpBT0VxBFUJZnJpZW5kX2lkcQVYAQAAADJVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QQXDRg1CN5kY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-04-23 13:24:53'),
	('dac95c9d3644f2436cdf8b013952a17a','MzA1NTNmYzA4NjEwZDJkODBiZTRjYmE1MDBiMjFiNTY4NWYzZTNlYTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBA1UOZmFjZWJvb2tfc3RhdGVxA1UgNFdMNk5weDVxTkVmY0xiMDdrMzd6aU5GY2Q2\ncWtodGdxBFUJZnJpZW5kX2lkcQVYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QUDER8bAXGpY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-05-03 17:31:27'),
	('dfe1371ee800b3b048a33004f850112d','Y2NmMjhkN2FkNTAyNDM4MTEzNjIxNDU5YzRkMzNkZTlkY2I1NzM1NjqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIHA0R3hxVG9iY3VVRnFoOEFLUDkxTHg0WXRPZnFlRHpScQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUDCigrDsbTY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQJVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-03 10:40:43'),
	('e057a1dbd5cc2aa52418f81dfd8e9219','ZjU0NGYyOGY0YzIzMjUyZTJjN2U1ZDM2NjJlMzYyOWI2MTg4YWIwYzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIGVNc0VHTlhFRFhOOU1ETXpnVUl2T0tKa1VvVWM2UlB2cQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUKFTgwC4uFY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQRVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-10 21:56:48'),
	('e304030e12d2ac345f0120bfe92dc50e','ZDU4MzMxMzgyZmM1Y2RiNzc1MjJlYTRmOWRhNDEwMmU3ODBkNjlhZDqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBA1UOZmFjZWJvb2tfc3RhdGVxA1UgREtyRmJDbE9oc1gyYkRQRlJmdHQxU3hUUUVr\nMWNMUnRxBFUJZnJpZW5kX2lkcQVYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QUDER8bC2RBY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-05-03 17:31:27'),
	('e79ed52a579616e84023a7a92670f3ad','NjJmMTMzODk5ZjFjZWY0OTMxNmUzNjI3MDc4YTU4MDEyOTM0OTAwNzqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBBFUOZmFjZWJvb2tfc3RhdGVxA1UgWllpSEQwVHJZWU1zemtWMmpIYkVvMThVSXlw\nejIyZmFxBFUSX2F1dGhfdXNlcl9iYWNrZW5kcQVVLXNvY2lhbF9hdXRoLmJhY2tlbmRzLmZhY2Vi\nb29rLkZhY2Vib29rQmFja2VuZHEGVR5zb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmRxB1gI\nAAAAZmFjZWJvb2txCFUPX3Nlc3Npb25fZXhwaXJ5cQljZGF0ZXRpbWUKZGF0ZXRpbWUKcQpVCgfd\nBQQHECgOWptjZGphbmdvLnV0aWxzLnRpbWV6b25lClVUQwpxCylScQyGUnENdS4=\n','2013-05-04 07:16:40'),
	('e9a8d049d25aa0d000a90b62de0b12f5','ODJjYWM5ODAwYWRmZTk5NjZmM2Q3MTNjM2EzNzliOWQyMjE3YWEwYTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBA1UOZmFjZWJvb2tfc3RhdGVxA1UgRzlQczBielpIclNuM2ZsUEo3VnRoaWVPdm9u\nRUJjZVJxBFUJZnJpZW5kX2lkcQVYAQAAADRVEl9hdXRoX3VzZXJfYmFja2VuZHEGVS1zb2NpYWxf\nYXV0aC5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va0JhY2tlbmRxB1Uec29jaWFsX2F1dGhfbGFz\ndF9sb2dpbl9iYWNrZW5kcQhYCAAAAGZhY2Vib29rcQlVD19zZXNzaW9uX2V4cGlyeXEKY2RhdGV0\naW1lCmRhdGV0aW1lCnELVQoH3QUGEDYxDCBMY2RqYW5nby51dGlscy50aW1lem9uZQpVVEMKcQwp\nUnENhlJxDnUu\n','2013-05-06 16:54:49'),
	('f395234dc7703dcbbad21a393796da5a','ODdiOGNmYmZlMTVhMmViYjQxMDgyNTI2MWI4NzEyYzZmNzhmYzJkNzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIDFpV1VLVW96VFRuQWJvcTdvMlZQVktPck1qSkpFMW8ycQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QUGEg0IAe0wY2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQlVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-05-06 18:13:08'),
	('fa8d264851235e4ef631df2999b69a17','NTM2MWUwMzQ0YzUxNzgyNWI4MmEyNzg3Yzc2ZjQxN2RlYTZmMjMwYzqAAn1xAShVDmZhY2Vib29r\nX3N0YXRlcQJVIFhHa0RVREpuSUVvcGhxQ1gwMlUzOGdZTjgwNUoyZ1ExcQNVD19zZXNzaW9uX2V4\ncGlyeXEEY2RhdGV0aW1lCmRhdGV0aW1lCnEFVQoH3QQXDRg1DpA6Y2RqYW5nby51dGlscy50aW1l\nem9uZQpVVEMKcQYpUnEHhlJxCFUSX2F1dGhfdXNlcl9iYWNrZW5kcQlVLXNvY2lhbF9hdXRoLmJh\nY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rQmFja2VuZHEKVQ1fYXV0aF91c2VyX2lkcQuKAQFVHnNv\nY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZHEMWAgAAABmYWNlYm9va3ENdS4=\n','2013-04-23 13:24:53');

/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_site
# ------------------------------------------------------------

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;

INSERT INTO `django_site` (`id`, `domain`, `name`)
VALUES
	(1,'example.com','example.com');

/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table social_auth_association
# ------------------------------------------------------------



# Dump of table social_auth_nonce
# ------------------------------------------------------------



# Dump of table social_auth_usersocialauth
# ------------------------------------------------------------

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;

INSERT INTO `social_auth_usersocialauth` (`id`, `user_id`, `provider`, `uid`, `extra_data`)
VALUES
	(1,1,'facebook','100000467382825','{\"access_token\": \"AAAGl12e8jmoBAHo1ZAJe5M6ETyfnrcOfOXCIt1DOZAceceACekqLkxcoV9C5E3US2R3xiQdTRyJDs2q1OSeBc5DljYByafVWw1asv8zgZDZD\", \"expires\": \"5121331\", \"id\": \"100000467382825\"}'),
	(2,2,'facebook','100000431944095','{\"access_token\": \"AAAGl12e8jmoBAKThXP22Nu2p3qxajD5o0PObxMb4AZCYkRyVnF2DP7PEIUchypal6EkCTVVthZAgSnfmGsh0iFmemKEHUzkJhXiWRrkgZDZD\", \"expires\": \"5183999\", \"id\": \"100000431944095\"}'),
	(3,3,'facebook','1488867719','{\"access_token\": \"AAAGl12e8jmoBAHyeCXaC2ZBKpucZBacFZCvQzm06QGzDEYcSZBG5M7dVavbaqKTZAqHzda02gcVMm680vbWFN4ZAWQpTfwffxXF6URSLyIkgZDZD\", \"expires\": \"5183998\", \"id\": \"1488867719\"}'),
	(4,4,'facebook','100000284981757','{\"access_token\": \"AAAGl12e8jmoBAF695UKjXzeQQyWLt44wl3eJCZBcaCFa6faexF24htlXaFKlfsvIEwDZAWkVRIweN3eZBzShZCTLlDk365KKbDTZACDU0mQZDZD\", \"expires\": \"5183999\", \"id\": \"100000284981757\"}'),
	(5,5,'facebook','100003040397216','{\"access_token\": \"AAAGl12e8jmoBAIVLhAiSyJhb0X9yK5uljZCbSiR2ZA7ZAzkRYGjMJfiy3jjkgZAtacZCBzgCqmdSloG8EgOLG7tirdfQcZAM7mKAFzV2CHiQZDZD\", \"expires\": \"5183995\", \"id\": \"100003040397216\"}'),
	(6,6,'facebook','100001635932210','{\"access_token\": \"AAAGl12e8jmoBAMeISxOmjAuCO7xgHC74xAJSq5oJrZB2L9z1zHWYpbB5T4ZCvkHPZBnPvR21Y9ZCt2GuPKp1cVmZAI54Wn5SrSso2INwgFgZDZD\", \"expires\": \"5183998\", \"id\": \"100001635932210\"}'),
	(7,7,'facebook','100000061884540','{\"access_token\": \"AAAGl12e8jmoBAJgjBZBjC2fCWjqHNZBcVDmW8Y1ZB1NLdvlgVpD6l9osEhfl3k9GF2BdfoOQGSY3lWSvdrjvXbO8peN1YPzlxANOShqZAgZDZD\", \"expires\": \"5183999\", \"id\": \"100000061884540\"}'),
	(8,8,'facebook','100001412702235','{\"access_token\": \"AAAGl12e8jmoBAIuvIfpriP9PQ0ZBNzzAwlsZCwm4EOJsKiyPn3DwG3IuQpaUTCkgntn1ZAo0pTUM3zkg7LlMZAkTf2FBLHsg30RShfT2fwZDZD\", \"expires\": \"5183998\", \"id\": \"100001412702235\"}'),
	(9,9,'facebook','100002510910651','{\"access_token\": \"AAAGl12e8jmoBAF49SAr51L8dcyPXvM3WDaxXWZBZAsnqrOjV9EZBzOLOSuzEcFZAlIEMKLMOB5qx8e0foYgeV8AIoBcgieN70Cyd32WbkwZDZD\", \"expires\": \"4746466\", \"id\": \"100002510910651\"}'),
	(10,10,'facebook','100002323554962','{\"access_token\": \"AAAGl12e8jmoBAIM48bWnlC2UGUPEljFzIlWgqkNoiK39IlK1sgKYYyZCEUJHdCxMVSZCfHp1fM2vMHl5Vpos4M1OaTTHUqnMi9bwYOnQZDZD\", \"expires\": \"5128379\", \"id\": \"100002323554962\"}'),
	(11,11,'facebook','1183028056','{\"access_token\": \"AAAGl12e8jmoBAPn2KDB3g8Us9PWZB3LLATa4N2zPAABiWJOdRuXerTkEWIspfLSVD0jDCWcNnXYeLpxydoEJ15VFgMYwCeyc2k75tpAZDZD\", \"expires\": \"5183999\", \"id\": \"1183028056\"}'),
	(12,12,'facebook','100000635717443','{\"access_token\": \"AAAGl12e8jmoBAF86sovQyMWj7n2cqo0wU5QokGdZC6aYesWM1VSxjluiVbHx3XVchPYIZC9ycpLQPefMyVR0e5shU10aDZBhsF5SHTGIAZDZD\", \"expires\": \"5183999\", \"id\": \"100000635717443\"}'),
	(13,13,'facebook','100005421398990','{\"access_token\": \"AAAGl12e8jmoBAAu6jtVC1U6nTOMIXIGpc7LcailowZAgEUWjBd66uIYcy3EhIREZBa0k7I3MebLsQJLZAbpe3qlVEs687HHWk1aEtAhsgZDZD\", \"expires\": \"5183998\", \"id\": \"100005421398990\"}');

/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table south_migrationhistory
# ------------------------------------------------------------

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;

INSERT INTO `south_migrationhistory` (`id`, `app_name`, `migration`, `applied`)
VALUES
	(1,'deptor','0001_initial','2013-03-14 11:50:28');

/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
