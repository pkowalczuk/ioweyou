from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import User
from social_auth.models import UserSocialAuth
from entry.models import Entry
from user.managers import UserProfileManager


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    photo = models.CharField(max_length=50)
    bank_account = models.CharField(max_length=100)

    objects = UserProfileManager()

    def __unicode__(self):
        return self.get_fullname()

    def get_fullname(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)

    def get_income(self):
        income = Entry.objects.all().extra(
            where=["lender_id='%s'"],
            params=[self.id]
        ).aggregate(Sum('value'))

        return income['value__sum']

    def get_outcome(self):
        outcome = Entry.objects.all().extra(
            where=["debtor_id='%s'"],
            params=[self.id]
        ).aggregate(Sum('value'))

        return outcome['value__sum']

    def get_income_accepted(self):
        income = Entry.objects.all().extra(
            where=["lender_id='%s' AND accepted_at IS NOT NULL"],
            params=[self.id]
        ).aggregate(Sum('value'))

        return income['value__sum']

    def get_outcome_accepted(self):
        outcome = Entry.objects.all().extra(
            where=["debtor_id='%s' AND accepted_at IS NOT NULL"],
            params=[self.id]
        ).aggregate(Sum('value'))

        return outcome['value__sum']

    def get_income_unaccepted(self):
        income = Entry.objects.all().extra(
            where=["lender_id='%s' AND accepted_at IS NULL"],
            params=[self.id]
        ).aggregate(Sum('value'))

        return income['value__sum']

    def get_outcome_unaccepted(self):
        outcome = Entry.objects.all().extra(
            where=["debtor_id='%s' AND accepted_at IS NULL"], 
            params=[self.id]
        ).aggregate(Sum('value'))

        return outcome['value__sum']

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
User.socialauth = property(lambda u: UserSocialAuth.objects.get_or_create(user=u)[0])


class Friendship(models.Model):
    creator = models.ForeignKey(User, related_name="friendship_creator_set")
    friend = models.ForeignKey(User, related_name="friend_set")
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def name(self):
        return self.creator.last_name + str(self.creator.id) + '_' + self.friend.last_name + str(self.friend.id)


class UserClient(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.name