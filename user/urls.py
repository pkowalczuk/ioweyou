from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^logout/', views.logout_view, name='logout'),
)