from django.forms import ModelForm
from user.models import User
from django import forms

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email','password' )
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Username', 'class': 'span4'}),
            'first_name': forms.TextInput(attrs={'placeholder': 'First name', 'class': 'span4'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Last name', 'class': 'span4'}),
            'email': forms.TextInput(attrs={'placeholder': 'Email', 'class': 'span4'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'span4'}),
        }

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user