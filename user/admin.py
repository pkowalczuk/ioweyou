from user.models import UserProfile, Friendship
from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site
from django.contrib import admin


class FriendshipAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'creator', 'friend', 'created_at')

admin.site.register(UserProfile)
admin.site.register(Friendship, FriendshipAdmin)
admin.site.unregister(User)
admin.site.unregister(Site)
admin.site.unregister(Group)