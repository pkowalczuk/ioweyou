from django.db import models
from django.db.models import Q


class UserProfileManager(models.Manager):

    def get_friends(self, user):
        return self.get_query_set().filter(
            Q(user__friend_set__creator=user, user__is_superuser=0) |
            Q(user__friendship_creator_set__friend=user, user__is_superuser=0)
        ).distinct()
