from models import Friendship
from social_auth.models import UserSocialAuth
import urllib2
import json as simplejson
from django.db.models import Q


def get_friends(backend, details, response, user=None, is_new=False, *args, **kwargs):

    if not user is None and is_new:
        opener = urllib2.build_opener()
        access_token = user.socialauth.extra_data['access_token']
        url = 'https://graph.facebook.com/' + user.socialauth.uid + '?fields=friends&limit=9999&access_token=' + access_token
        response_data = opener.open(url)
        data = simplejson.load(response_data)

        ids = []
        for friend in data['friends']['data']:
            ids.append(friend['id'])

        persons = UserSocialAuth.objects.filter(uid__in=ids)

        for person in persons:
            try:
                try:
                    Friendship.objects.get(Q(creator_id=user.id, friend_id=person.user_id) | Q(creator_id=person.user_id, friend_id=user.id))
		except Friendship.DoesNotExist:
		    relationship = Friendship(creator_id=user.id, friend_id=person.user_id)
                    relationship.save()
            except UserSocialAuth.DoesNotExist:
                pass
