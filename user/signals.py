from django.db.models.signals import post_save
from django.contrib.auth.models import User
from social_auth.models import UserSocialAuth
from models import UserProfile


def profile_create(sender, **kw):
    user = kw["instance"]

    if kw["created"]:
        profile = UserProfile()
        profile.user = user
        profile.save()


post_save.connect(profile_create, sender=User)
