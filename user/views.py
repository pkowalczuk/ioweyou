from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.shortcuts import render
from django.contrib import auth
from django.template import RequestContext
from user.models import UserProfile
from user.forms import UserForm


def login_view(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = auth.authenticate(username=username, password=password)
	if user is not None and user.is_active:
		auth.login(request, user)
		return HttpResponseRedirect("/")
	else:
		# Show an error page
		return HttpResponseRedirect("/")

def logout_view(request):
	auth.logout(request)
	return HttpResponseRedirect("/")

def register_view(request):
	if request.method == 'POST':
		registration_form = account_create(request)
		if registration_form.is_valid():
			return HttpResponseRedirect("/")
	else:
		registration_form = UserForm()

	context = RequestContext(request, {})
	context.update(csrf(request))

	return render(request, "user/register.html",{'registration_form': registration_form})

def account_create(request):
	form = UserForm(request.POST)

	if form.is_valid():
		user = form.save(commit=False)
		user.is_staff = False
		user.is_superuser = False
		user.is_active = True
		user.save()

		profile = UserProfile()
		profile.user = user
		profile.save()

		#messages.info(request, "Thanks for registering. You are now logged in.")
		user = auth.authenticate(username=request.POST['username'], password=request.POST['password'])
		auth.login(request, user)

	return form