from django.http import HttpResponseRedirect
from django.contrib import messages

from comment.forms import CommentForm

def add_process(request, form=CommentForm):
    form = form(request.POST)

    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Comment was added correctly.')
        return HttpResponseRedirect("/")
    else:
        messages.add_message(request, messages.ERROR, 'Form is not valid.')
        return HttpResponseRedirect("/")