from django.db import models
import ioweyou.settings as settings


class Comment(models.Model):
    class Meta:
        abstract = True

    content = models.TextField('Content', blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='comment')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return 'comment_' + str(self.created_at)