from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^announcement/$',  'announcement.views.archive_view', name='announcement_view'),
    url(r'^announcement/confirm/(?P<announcement_id>\d+)/$', 'announcement.views.confirm_process', name='announcement_confirm_process'),
)