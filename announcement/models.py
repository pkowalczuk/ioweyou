from django.db import models
from announcement.managers import AnnouncementManager
import ioweyou.settings as settings


class Announcement(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField('Content')
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, through='AnnouncementConfirmation')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    objects = AnnouncementManager()

    def __unicode__(self):
        return self.title


class AnnouncementConfirmation(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    announcement = models.ForeignKey(Announcement)
    created_at = models.DateTimeField(auto_now_add = True, editable=False)