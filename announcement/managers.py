from django.db import models


class AnnouncementManager(models.Manager):

    def get_query_set(self):
        return AnnouncementQuerySet(self.model)

    def get_confirmed(self, user):
        return self.get_query_set().exclude(announcementconfirmation__user=user.id)

    def one_or_none(self):
        try:
            announcement = self.get_query_set()[0]
        except IndexError:
            announcement = None

        return announcement


class AnnouncementQuerySet(models.query.QuerySet):
    
    def one_or_none(self):
        try:
            result = self[0]
        except IndexError:
            result = None

        return result