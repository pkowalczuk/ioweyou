from announcement.models import Announcement, AnnouncementConfirmation
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from datetime import datetime


def archive_view(request):
    announcements = Announcement.objects.all().order_by('-created_at')
    return render(request, 'announcement/archive.html', {'announcements': announcements})


def confirm_process(request, announcement_id):
    announcement = get_object_or_404(Announcement, pk=announcement_id)

    confirm = AnnouncementConfirmation(user=request.user, announcement=announcement, created_at=datetime.now)
    confirm.save()
    try:
        announcement = Announcement.objects.get_confirmed(request.user).order_by('-created_at')[0]
    except IndexError:
        announcement = None

    return render(request, 'ioweyou/partials/announcement.html', {'announcement': announcement})