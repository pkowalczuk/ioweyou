from django.contrib import admin
import announcement.models as models


class AnnouncementAdmin(admin.ModelAdmin):

    list_display = ('id', 'title', 'content', 'created_at',)

admin.site.register(models.Announcement, AnnouncementAdmin)
admin.site.register(models.AnnouncementConfirmation)